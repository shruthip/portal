Application.run(function($rootScope, $window, $location, $timeout) {
    "use strict";

    // Initialise google analytics
    $window.ga = $window.ga || function() {
        (ga.q = ga.q || []).push(arguments)
    };
    $window.ga.l = +new Date;

    $window.ga('create', 'UA-81739249-2', 'auto');

    $window.ga('send', 'pageview');

    // Track pageview on route change
    $rootScope.$on('$routeChangeSuccess', function() {
        $window.ga('set', 'page', $location.url());
        $window.ga('send', 'pageview');
    });

    /* perform any action on the variables within this block(on-page-load) */
    $rootScope.onAppVariablesReady = function() {
        /*
         * variables can be accessed through '$rootScope.Variables' property here
         * e.g. $rootScope.Variables.staticVariable1.getData()
         */
    };

    $rootScope.UserDataonBeforeUpdate = function(variable, inputData) {
        if (inputData) {
            inputData.name = {
                matchMode: 'isnotnull'
            }
        }
    };


    /* perform any action on session timeout here, e.g clearing some data, etc */
    $rootScope.onSessionTimeout = function() {
        /*
         * NOTE:
         * On re-login after session timeout:
         * if the same user logs in(through login dialog), app will retain its state
         * if a different user logs in, app will be reloaded and user is redirected to respective landing page configured in Security.
         */
    };

    $rootScope.RunUnderwritingonError = function(variable, data) {
        $rootScope.Variables.ErrorRunUnderwriting.setMessage(data);
        $rootScope.Variables.ErrorRunUnderwriting.invoke();
    };


    $rootScope.ProcessRedemptiononError = function(variable, data) {
        $rootScope.Variables.ErrorProcessRedemption.setMessage(data);
        $rootScope.Variables.ErrorProcessRedemption.invoke();
    };


    /* Offline Code - Starts Here */
    $rootScope.offlineToggleTag = [];
    var isOfflineToggleTagRequestInProgress = false;
    $rootScope.offlineAddNote = [];
    var isOfflineAddNoteRequestInProgress = false;

    $rootScope.checkforOnlineToPushChanges = function() {
        Offline.check();
        if (Offline.state === "up") {
            pushOfflineChanges();
        } else {
            $timeout($rootScope.checkforOnlineToPushChanges, 1000);
        }
    };

    var pushOfflineChanges = function() {
        if ($rootScope.offlineToggleTag.length > 0 && !isOfflineToggleTagRequestInProgress) {
            isOfflineToggleTagRequestInProgress = true;
            $rootScope.Variables.svToggleTagOffline.setInput("property_id", $rootScope.offlineToggleTag[0].property_id);
            $rootScope.Variables.svToggleTagOffline.setInput("note_type", $rootScope.offlineToggleTag[0].note_type);
            $rootScope.Variables.svToggleTagOffline.invoke();
        }

        if ($rootScope.offlineAddNote.length > 0 && !isOfflineAddNoteRequestInProgress) {
            isOfflineAddNoteRequestInProgress = true;
            $rootScope.Variables.svAddNoteOffline.setInput("property_id", $rootScope.offlineAddNote[0].property_id);
            $rootScope.Variables.svAddNoteOffline.setInput("note", $rootScope.offlineAddNote[0].note);
            $rootScope.Variables.svAddNoteOffline.invoke();
        }
    };

    $rootScope.svToggleTagOfflineonSuccess = function(variable, data) {
        isOfflineToggleTagRequestInProgress = false;
        $rootScope.offlineToggleTag.shift();
        $rootScope.Variables.stOfflineCount.dataSet.dataValue = $rootScope.Variables.stOfflineCount.dataSet.dataValue - 1;
        if ($rootScope.offlineToggleTag.length > 0) {
            $rootScope.checkforOnlineToPushChanges();
        }
    };

    $rootScope.svToggleTagOfflineonError = function(variable, data) {
        isOfflineToggleTagRequestInProgress = false;
        $rootScope.checkforOnlineToPushChanges();
    };

    // Add Note
    $rootScope.svAddNoteOfflineonSuccess = function(variable, data) {
        isOfflineAddNoteRequestInProgress = false;
        $rootScope.$emit('updated-notes');
        $rootScope.offlineAddNote.shift();
        $rootScope.Variables.stOfflineCount.dataSet.dataValue = $rootScope.Variables.stOfflineCount.dataSet.dataValue - 1;
        if ($rootScope.Variables.stOfflineCount.dataSet.dataValue > 0) {
            $rootScope.checkforOnlineToPushChanges();
        }
    };

    $rootScope.svAddNoteOfflineonError = function(variable, data) {
        isOfflineAddNoteRequestInProgress = false;
        $rootScope.checkforOnlineToPushChanges();
    };
    /* Offline Code - Ends Here */


    // $rootScope.CompleteSaleonError = function(variable, data) {
    //     $rootScope.Variables.ErrorCompleteSale.setMessage(data);
    //     $rootScope.Variables.ErrorCompleteSale.invoke();
    // };


    // $rootScope.isNumber = function(evt) {
    //     evt = (evt) ? evt : window.event;
    //     var charCode = (evt.which) ? evt.which : evt.keyCode;
    //     if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    //         return false;
    //     }
    //     return true;
    // };
}).directive('numbersOnly', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };

});

//Route Param Fix Reference - https://github.com/angular/angular.js/issues/1699#issuecomment-96126887
Application.service('routeParams', function(
    $route,
    $location
) {
    var R = {};

    /**
     * @ngdoc method
     * @name routeParams#replace
     *
     * @description
     * Replace route params, including path params, without reloading controller.
     *
     * Causes `$route` service to update the current URL, replacing
     * current route parameters with those specified in `params`.
     *
     * Provided property names that match the route's path segment
     * definitions will be interpolated into the location's path, while
     * remaining properties will be treated as query params.
     *
     * If `options.reload` is truthy, the current controller is reloaded.
     *
     * If `options.history` is truthy, the browser history is updated.
     * If `options.history` is undefined, the browser history is only updated
     * when changing value of any existing `$route` parameter,
     * ie. not when adding a previously undefined `$route` parameter.
     *
     * @param {!Object<string, string>} params mapping of URL parameter names to values
     * @param {!Object<string, boolean>} options `history` and `reload` options.
     *
     * @returns {Object} self
     */
    R.replace = function(params, options) {
        var key, value, updateRequired;
        options = options || {};

        // Convert params to strings, and check if they differ from current route params.
        // If `options.history` is undefined, and passed params update any current route params, then set it to true.
        var currentParams = $route.current.params;

        if (!$route.current.$$route) {
            // Can't change location.
            return;
        }
        var currentPath = $route.current.$$route.originalPath;
        var history = options.history;

        for (key in params) {
            // jshint eqnull: true
            value = params[key] = (params[key] == null ? undefined : params[key] + '');

            if (value !== currentParams[key]) {
                updateRequired = true;

                if (history === undefined && key in currentParams) {
                    history = true;
                }
            }
        }

        if (updateRequired) {
            // If `options.reload` is falsey, then set current route `reloadOnSearch` to false,
            // and make passed path params equal with current route path params,
            // so that `$route` treats the change as update only, and does not broadcast `$routeChangeStart` event.
            //
            // See https://github.com/angular/angular.js/issues/1699#issuecomment-45048054
            // and https://github.com/angular/angular.js/tree/v1.3.x/src/ngRoute/route.js#L484
            // and https://github.com/angular/angular.js/tree/v1.3.x/src/ngRoute/route.js#L539
            if (!options.reload) {
                // Set current route `reloadOnSearch` to false.
                $route.current.$$route.reloadOnSearch = false;

                // Add any passed path params to current route path params, and convert them to strings.
                // Path params are detected by searching for respective name group `:key[*?]` in current route path.
                for (key in params) {
                    if (currentPath.search(':\\b' + key + '\\b') !== -1) {
                        $route.current.pathParams[key] = params[key];
                    }
                }

                // Add any current route path params to passed params, if not set there already.
                for (key in $route.current.pathParams) {
                    if (!(key in params)) {
                        params[key] = $route.current.pathParams[key];
                    }
                }

                // TODO: push state if `options.history` is truthy.
            }
            // If `options.history` is falsey, and controller is reloaded,
            // then make `$location` replace current history state, instead of pushing a new one.
            else if (!history) {
                $location.replace();
            }

            $route.updateParams(params);

            // Update current route params, so the change is reflected immediatelly,
            // and nearby replace() call work correctly.
            for (key in params) {
                $route.current.params[key] = params[key];
            }
        }

        return R;
    };

    return R;



    $rootScope.PlatformNoteDataonSuccess = function(variable, data) {
        console.log("Onsuccess");
        console.log(data);
        console.log(variable);
        debugger
    };


    $rootScope.PlatformNoteDataonResult = function(variable, data) {
        console.log("Onresult");
        console.log(data);
        console.log(variable);
        debugger
    };

});