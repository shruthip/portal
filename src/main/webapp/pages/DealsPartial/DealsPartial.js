Application.$controller("DealsPartialPageController", ["$scope", "$compile", function($scope, $compile) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        // $scope.Widgets.accordionpane_deals.$element.find('.panel-heading').click();
        $scope.Widgets.accordionpane_deals.$element.find('.panel-heading').each(function(index) {
            var $header = WM.element(this),
                $button = '<button ng-click="onAccordionButtonClick($event, ' + index + ')" class="btn-default btn app-button" type="button"><span class="btn-caption">Add Deal</span></button>',
                $compiledTl = $compile($button)($scope);
            $header.find('.panel-actions').prepend($compiledTl);

        });
    };

    $scope.onAccordionButtonClick = function($event, index) {
        $event.stopPropagation();
        editFlag = false;
        $scope.Variables.SelectedContact.dataSet = {};
        $scope.Widgets.dialog_deal.open();
    };




}]);




Application.$controller("dialog_dealController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.dialog_dealOpened = function($event, $isolateScope) {
            $scope.Variables.DealPartial.dataSet.dataValue = $scope.partialPage;
            if (!editFlag) {
                $scope.Widgets.dealtable.selecteditem = {};
            } else {
                $scope.Widgets.dialog_deal.title = "Deal Detail (Deal ID " + $scope.Widgets.dealtable.selecteditem.id + ")"

            }
        };

        $scope.DealLiveForm1Beforeservicecall = function($event, $operation, $data) {
            if ($data.closingDate)
                $data.closingDate = new moment($data.closingDate).utc().format('YYYY-MM-DD HH:mm:ss');
        };


        $scope.DealLiveForm1Error = function($event, $operation, $data) {
            $scope.Variables.ErrorDeal.invoke();
        };

    }
]);



var editFlag = false;

Application.$controller("DealTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("dealtableController", ["$scope", "DialogService ",
    function($scope, DialogService) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.updaterowAction = function($event, $rowData) {
            editFlag = true;
            DialogService.open('dialog_deal', $scope, {
                'partialPage': "AddDeal",
            });
        };


        $scope.addNewRowAction = function($event) {
            editFlag = false;
            DialogService.open('dialog_deal', $scope, {
                'partialPage': "EditDeal",
            });
        };

    }
]);



Application.$controller("DealLiveForm1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("dialog2Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("dialog_dealsController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("AddDealsTable1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);