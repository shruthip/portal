Application.$controller("TakePhotoPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    var blobName = "";
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        blobName = $scope.Variables.svReadPropertyPhoto.dataSet.value;
    };


    $scope.AzureUploadonSuccess = function(variable, data) {

        if (data) {
            var date = new Date();
            var time = date.getTime();
            $scope.Variables.AzureLive.setInput("propertyId", $scope.$root.Widgets.SearchResultLiveList.selecteditem.property_id);
            $scope.Variables.AzureLive.setInput("link", data);
            $scope.Variables.AzureLive.setInput("user", $scope.$root.Variables.loggedInUser.dataSet.name);
            $scope.Variables.AzureLive.setInput("createdTimestamp", time);
            $scope.Variables.AzureLive.insertRecord();
            console.log(data)
            $scope.$parent.Widgets.TakePhotoDialog.closable = true;
            //$scope.$root.Variables.svPortalPropertyData.invoke();
        }
    };

    $scope.AzureLiveonSuccess = function(variable, data) {
        $scope.$root.$emit("update-photo-count");
    };



    $scope.fileupload1Select = function($event, $isolateScope) {
        $scope.$parent.Widgets.TakePhotoDialog.closable = false;
        $scope.Variables.svFileServiceUploadFile.setInput("files", $event[0]);
        $scope.Variables.svFileServiceUploadFile.invoke();
    };


    $scope.svFileServiceUploadFileonSuccess = function(variable, data) {
        $scope.Variables.AzureUpload.setInput("name", data[0].fileName);
        $scope.Variables.AzureUpload.setInput("blobName", blobName);

        $scope.Variables.AzureUpload.invoke();

    };


    $scope.svFileServiceUploadFileonError = function(variable, data) {
        $scope.$parent.Widgets.TakePhotoDialog.closable = true;
    };


    $scope.AzureUploadonError = function(variable, data) {
        $scope.$parent.Widgets.TakePhotoDialog.closable = true;
    };

}]);