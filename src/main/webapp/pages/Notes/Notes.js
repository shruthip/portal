Application.$controller("NotesPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */

        if ($scope.activePageName != "InspectMobile" && $scope.activePageName != "inspect") {
            $scope.Widgets.All_Tags_Container_Layout.show = true;
            $scope.Widgets.TagsContainer.content = "Tags";
        }

        if ($scope.activePageName === "service") {
            $scope.Variables.svPortalAuctionNotes.maxResults = "4";
            $scope.Widgets.NotesContent.height = "170px"
        }
    };

    $scope.$root.$on('updated-notes', function() {
        $scope.Variables.svPortalAuctionNotes.invoke();
    });

    $scope.NotesSwitchChange = function($event, $isolateScope, newVal, oldVal) {
        var flag;
        if (newVal == "All") {
            flag = 1;
        } else if (newVal == "Open") {
            flag = 2;
        } else /* if (newVal == "Archive") */ {
            flag = 3;
        }

        $scope.Variables.svPortalAuctionNotes.setInput("flag", flag);
        $scope.Variables.svPortalAuctionNotes.update();
    };

}]);

Application.$controller("ConfirmArchiveDialogController", ["$scope",
	function($scope) {
		"use strict";
		$scope.ctrlScope = $scope;
	}
]);