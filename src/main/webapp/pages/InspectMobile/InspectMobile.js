Application.$controller("InspectMobilePageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        $(window).resize(function() {
            $scope.Widgets.SearchContainer.height = $(window).height() - 100;
        });

        $(window).resize();
    };

    $scope.ButtonSlideClick = function($event, $isolateScope) {
        $('.Mobile-left').addClass("mobile-left-onclick");
        $('.Mobile-right').removeClass("mobile-right-onclick");
    };

    $scope.ButtonSlideRightClick = function($event, $isolateScope) {
        $('.Mobile-left').removeClass("mobile-left-onclick");
        $('.Mobile-right').addClass("mobile-right-onclick");
    };
}]);