Application.$controller("DealsPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };




    $scope.buttonClick = function($event, $isolateScope) {
        editFlag = false;
        $scope.Widgets.dialog_deal.open();
    };






    $scope.select_statusChange = function($event, $isolateScope, newVal, oldVal) {
        $scope.Variables.PlatformDealData.setFilter("statusId", $isolateScope.datavalue);
        $scope.Variables.PlatformDealData.update();
    };


    $scope.AddressSelect = function($event, $isolateScope, selectedValue) {
        $scope.Variables.PlatformDealData.setFilter("assetId", $isolateScope.datavalue);
        $scope.Variables.PlatformDealData.update();
    };

}]);

var editFlag = false;


Application.$controller("dialog_dealController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.dialog_dealOpened = function($event, $isolateScope) {
            if (!editFlag) {
                $scope.Widgets.DealTable.selecteditem = {};
            } else {
                $scope.Widgets.dialog_deal.title = "Deal Detail (Deal ID " + $scope.Widgets.DealTable.selecteditem.id + ")"
            }
        };


        $scope.DealLiveFormBeforeservicecall = function($event, $operation, $data) {
            if ($data.closingDate) {
                $data.closingDate = new moment($data.closingDate).utc().format('YYYY-MM-DD HH:mm:ss');
            }

        };


        $scope.DealLiveFormError = function($event, $operation, $data) {
            $scope.Variables.ErrorDeal.invoke();
        };

    }
]);







Application.$controller("DealLiveFormController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);




Application.$controller("DealTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;


        $scope.updaterowAction = function($event, $rowData) {
            editFlag = true;
            $scope.Widgets.dialog_deal.title = "Deal Details (Deal ID" + $rowData.id + ")"
            $scope.Widgets.dialog_deal.open();
        };

    }
]);