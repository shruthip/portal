Application.$controller("CalanderEventPageController", ["$scope", "DialogService", function($scope, DialogService) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */

    };



    $scope.NoteDataonBeforeUpdate = function(variable, inputData) {
        if (inputData) {
            inputData.doneTimestamp = {
                matchMode: 'isnotnull'
            }
        }
    };


    $scope.eventtask_btnClick = function($event, $isolateScope) {
        $scope.Widgets.container_wiki.show = false;
        $scope.Widgets.container_iframe.show = true;
    };


    $scope.wiki_btnClick = function($event, $isolateScope) {
        $scope.Widgets.container_wiki.show = true;
        $scope.Widgets.container_iframe.show = false;
    };





    $scope.AddEvent_TaskPanelClick = function($event, $isolateScope) {
        $scope.Variables.GlobalTaskData.dataSet = {};
        DialogService.open('task_dialog', undefined, {
            'partialPage': "AddTaskPage"
        });

        // DialogService.showDialog('task_dialog', {
        //     'partialPage': "AddTaskPage"
        // });
    };

}]);


Application.$controller("NoteTableController", ["$scope", "DialogService",
    function($scope, DialogService) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.updaterowAction = function($event, $rowData) {
            wmSpinner.show("", "spinnner-id_3", "", "");
            $scope.Variables.GlobalTaskData.dataSet = $rowData;
            $scope.Variables.DefaultProperty.setFilter("id", $rowData.property.asset.id);
            $scope.Variables.DefaultProperty.update({}, function(data) {
                DialogService.open('task_dialog', $scope, {
                    'partialPage': "UserPanel",
                });
                wmSpinner.hide("spinnner-id_3");
            })
        };

    }
]);