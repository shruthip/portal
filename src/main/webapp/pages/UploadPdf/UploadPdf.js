Application.$controller("UploadPdfPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        //$scope.Variables.svPdfTemplatePerName.invoke();
    };


    $scope.fileupload1Success = function($event, $isolateScope) {
        var fileResponse = JSON.parse($event.target.response);
        console.log($event.target.response);
        console.log(fileResponse[0].fileName);
        $scope.Variables.svAzureUpload.setInput("name", fileResponse[0].fileName);
        $scope.Variables.svAzureUpload.setInput("blobName", $scope.Variables.svReadPropertyPdf.dataSet.value);
        $scope.Variables.svAzureUpload.update();

    };


    $scope.svAzureUploadonSuccess = function(variable, data) {
        if (data) {
            var date = new Date();
            var time = date.getTime();
            if ($scope.activePageName === "PropertyDetails") {
                $scope.Variables.lvPdf.setInput("assetId", $scope.pageParams.assetID);
                $scope.Variables.lvPdf.setInput("createdTimestamp", time);
                $scope.Variables.lvPdf.setInput("name", $scope.Widgets.textFileName.datavalue);
                $scope.Variables.lvPdf.insertRecord();
            } else {
                $scope.Variables.lvPdf.setInput("certificateId", $scope.$root.Widgets.SearchResultLiveList.selecteditem.certificate_id);
                $scope.Variables.lvPdf.setInput("createdTimestamp", time);
                $scope.Variables.lvPdf.setInput("name", $scope.Widgets.textFileName.datavalue);
                $scope.Variables.lvPdf.insertRecord();
            }
        }
        //$scope.$parent.Widgets.UploadPdfDialog.closable = true;

    };


    $scope.lvPdfonSuccess = function(variable, data) {
        $scope.$root.$broadcast("myEvent");
        $scope.$parent.Widgets.UploadPdfDialog.close();
    };

}]);