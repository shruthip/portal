Application.$controller("TitlePageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        // if ($scope.activePageName === "Main") {
        //     $scope.Widgets.title_header_plain.show = "false";
        //     $scope.Widgets.layoutgrid_mobile.show = "false"
        // } else if ($scope.activePageName === "inspect") {
        //     $scope.Widgets.title_header_blue_bg.show = "false";
        // }
        // if ($scope.activePageName === "InspectMobile") {
        //     $scope.Widgets.layoutgrid1.show = "false";
        //     $scope.Widgets.layoutgrid_mobile.show = "true"
        // } else if ($scope.activePageName != "InspectMobile") {
        //     $scope.Widgets.layoutgrid_mobile.show = "false"
        // }

        if ($scope.activePageName === "Main") {
            $scope.Widgets.layoutgrid_mobile.show = "false";
            $scope.Widgets.title_header_plain.show = "false";
        } else if ($scope.activePageName === "inspect" || $scope.activePageName === "InspectMobile") {
            $scope.Widgets.title_header_blue_bg.show = "false";
        }
        if ($scope.activePageName === "InspectMobile") {
            $scope.Widgets.layoutgrid1.show = "false";
            $scope.Widgets.layoutgrid_mobile.show = "true"
        }
        if ($scope.activePageName != "InspectMobile") {
            $scope.Widgets.layoutgrid_mobile.show = "false"
        }
    };

}]);