Application.$controller("headermobilePageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        // if ($scope.activePageName === "Main") {
        //     $scope.Widgets.AppName.caption = "Newline Portal - Buy";
        // } else if ($scope.activePageName === "service") {
        //     $scope.Widgets.AppName.caption = "Newline Portal - Service";
        // } else if ($scope.activePageName === "inspect") {
        //     $scope.Widgets.AppName.caption = "Newline Portal - Inspect";
        // } else if ($scope.activePageName === "InspectMobile") {
        //     $scope.Widgets.AppName.caption = "Newline Portal - Inspect";
        // } else if ($scope.activePageName === "Issues") {
        //     $scope.Widgets.AppName.caption = "Newline Portal - Admin";
        // } else if ($scope.activePageName === "AddEditBuyer") {
        //     $scope.Widgets.AppName.caption = "Newline Portal - Admin";
        // } else if ($scope.activePageName === "AddEditBuyerGroup") {
        //     $scope.Widgets.AppName.caption = "Newline Portal - Admin";
        // } else if ($scope.activePageName === "AddEditCounty") {
        //     $scope.Widgets.AppName.caption = "Newline Portal - Admin";
        // } else if ($scope.activePageName === "AddEditGrade") {
        //     $scope.Widgets.AppName.caption = "Newline Portal - Admin";
        // } else if ($scope.activePageName === "AddEditStates") {
        //     $scope.Widgets.AppName.caption = "Newline Portal - Admin";
        // } else if ($scope.activePageName === "AddEditTownship") {
        //     $scope.Widgets.AppName.caption = "Newline Portal - Admin";
        // } else if ($scope.activePageName === "TemplateForPdf") {
        //     $scope.Widgets.AppName.caption = "Newline Portal - Admin";
        // } else if ($scope.activePageName === "GeneratePdf") {
        //     $scope.Widgets.AppName.caption = "Newline Portal - Admin";
        // } else if ($scope.activePageName === "ListTemplatePdf") {
        //     $scope.Widgets.AppName.caption = "Newline Portal - Admin";
        // }
    };

}]);