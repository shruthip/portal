Application.$controller("MainPageController", ["$scope", "$timeout", "routeParams", function($scope, $timeout, routeParams) {
    "use strict";

    var fetch;
    var pageNumber;
    var isForwardNavigation = true;
    var isNewSearch = true;
    var uniqueSearchId;

    // Workaround for Google Stree View not getting loaded when hidden
    var fixGoogleStreetView = true;

    var startPropertyChangeWatch = function() {

        if ($scope.Widgets.SearchResultLiveList && $scope.Widgets.SearchResultLiveList.selecteditem) {

            $scope.$watch(function() {
                return $scope.Widgets.SearchResultLiveList.selecteditem;
            }, function(newVal, oldVal) {
                if (newVal && newVal.constructor.name === "Object") {
                    routeParams.replace({
                        "search": newVal.property_id
                    }, {
                        "reload": false,
                        "history": true
                    });
                } else {
                    routeParams.replace({
                        "search": ""
                    }, {
                        "reload": false,
                        "history": true
                    });
                }
            }, true);
        } else {
            $timeout(startPropertyChangeWatch, 100);
        }
    };

    /* perform any action on widgets within this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */

        // Fetch => Page Size, pageNumber => Current Page Number
        fetch = 100; //$scope.Variables.qryPortalBuyFilter.maxResults;


        // Disable Underwriting On Start

        $scope.svQryPortalAuctionDataonSuccess({}, {
            content: []
        });

        //Resize Search Result
        $(window).resize(function() {
            // $scope.Widgets.SearchResultLiveList.height = ($(window).height() - 160);
            $scope.Widgets.AllContentRightTile.height = ($(window).height() - 60);
            $scope.Widgets.LeftPanel.height = ($(window).height() - 60);
        });

        $(window).resize();

        // Search if the Page has search Parameter
        if ($scope.pageParams.search) {
            $scope.SearchBtnClick({}, {});
        }

        startPropertyChangeWatch();
    };

    $scope.UnderwritingStatusBtnClick = function(status) {
        $scope.Variables.UpdateUnderwriting.setInput("status_type", status);
        $scope.Variables.UpdateUnderwriting.update();
        $scope.UnderwritingStatusChange(status);
    };


    $scope.anchor2Click = function($event, $isolateScope) {
        $scope.Widgets.anchor2.show = false;
        $scope.Widgets.anchor3.show = true;
        $('div[name="gridcolumn1"]').show();
        $('div[name="gridcolumn6"]').css('width', '83.33%');
    };


    $scope.anchor3Click = function($event, $isolateScope) {
        $scope.Widgets.anchor2.show = true;
        $scope.Widgets.anchor3.show = false;
        $('div[name="gridcolumn1"]').hide();
        $('div[name="gridcolumn6"]').css('width', '100%');
    };
    $scope.SearchResultLiveListClick = function($event, $isolateScope) {
        // Find the current result index and update the Record Count UI
        var index = findCurrentIndex();
        var currentIndex = ((pageNumber - 1) * fetch) + index;
        $scope.Widgets.CurrentRecordIndex.caption = currentIndex;
        checkNavigationControls(currentIndex);
    };

    $scope.UnderwritingStatusChange = function(status) {
        $scope.removeClass("PendingBtn", "active-underwriting");
        $scope.removeClass("DeclineBtn", "active-underwriting");
        $scope.removeClass("ApproveBtn", "active-underwriting");

        if (status === 'pending') {
            $scope.Widgets.PendingBtn.class += " active-underwriting";
        } else if (status === 'declined') {
            $scope.Widgets.DeclineBtn.class += " active-underwriting";
        } else if (status === 'approved') {
            $scope.Widgets.ApproveBtn.class += " active-underwriting";
        }
    };

    $scope.removeClass = function(widgetName, className) {
        if ($scope.Widgets[widgetName].class) {
            $scope.Widgets[widgetName].class = $scope.Widgets[widgetName].class.replace(className, "");
        }
    };

    $scope.ClearBtnClick = function($event, $isolateScope) {
        $scope.Widgets.Search.datavalue = "";
        $scope.Widgets.State.datavalue = "";
        $scope.Widgets.County.datavalue = "";
        $scope.Widgets.Township.datavalue = "";
        $scope.Widgets.TaxYear.datavalue = "";
        $scope.Widgets.PropertyClass.datavalue = "";
        $scope.Widgets.UnderwritingStatus.datavalue = "";
        $scope.Widgets.AuctionDay.datavalue = [];
        $scope.Widgets.Buyer.datavalue = "";
        $scope.Widgets.Grade.datavalue = [];
        $scope.Widgets.TagOperator.datavalue = "=";
        $scope.Widgets.Tag.datavalue = "";
        $scope.Widgets.SaleAmountOperator.datavalue = "<=";
        $scope.Widgets.SaleAmount.datavalue = "";
        $scope.Widgets.PriorYearsDueOperator.datavalue = "<=";
        $scope.Widgets.PriorYearsDue.datavalue = "";
        $scope.Widgets.FilterPaid.datavalue = 1;
        resetPaging();

    };

    function resetPaging() {
        //pageNumber => Current Page Number
        pageNumber = 1;
    }

    resetPaging();


    $scope.toggleFilter = function() {
        $scope.Widgets.FilterLayoutGrid.show = !$scope.Widgets.FilterLayoutGrid.show;
    };

    // Navigate Recrods - If flag is 1 then show Next Result, If flag is -1 then show Previous Result
    $scope.navigateRecords = function(flag) {
        // Find current selected Live List Index
        var index = findCurrentIndex();
        // Find current selected index in Total Search Result
        var currentIndexInTotal = ((pageNumber - 1) * fetch) + index;
        if (flag === 1) { // On Next Result
            if (currentIndexInTotal < $scope.Widgets.TotalRecordsIndex.caption) {
                if (index < fetch) {
                    $scope.Widgets.SearchResultLiveList.deselectItem($scope.Widgets.SearchResultLiveList.dataset[index - 1]);
                    $scope.Widgets.SearchResultLiveList.selectItem($scope.Widgets.SearchResultLiveList.dataset[index]);
                    checkNavigationControls(currentIndexInTotal + 1);
                } else {
                    $scope.NextPageClick({}, {});
                }
                $scope.Widgets.CurrentRecordIndex.caption = currentIndexInTotal + 1;
            } else {
                alert("You have reached the end of Search Result.");
            }
        } else if (flag === -1) { // On Previous Result
            if (currentIndexInTotal > 1) {
                if (index > 1) {
                    $scope.Widgets.SearchResultLiveList.deselectItem($scope.Widgets.SearchResultLiveList.dataset[index - 1]);
                    $scope.Widgets.SearchResultLiveList.selectItem($scope.Widgets.SearchResultLiveList.dataset[index - 2]);
                    checkNavigationControls(currentIndexInTotal - 1);
                } else {
                    $scope.PreviousPageClick({}, {});
                }
                $scope.Widgets.CurrentRecordIndex.caption = currentIndexInTotal - 1;
            } else {
                alert("You have reached the start of Search Result.");
            }
        }
    };

    function findCurrentIndex() {
        var index = 0;
        var sourceObj = $scope.Widgets.SearchResultLiveList.dataset;
        var selectedObj = $scope.Widgets.SearchResultLiveList.selecteditem;
        if (sourceObj && sourceObj.length > 0) {
            for (var i = 0; i < sourceObj.length; i++) {
                if (sourceObj[i].property_id === selectedObj.property_id) {
                    index = i + 1;
                    break;
                }
            }
        }
        return index;
    }

    function checkNavigationControls(index) {
        if (index <= 1) {
            $scope.Widgets.PreviousRecord.disabled = true;
        } else {
            $scope.Widgets.PreviousRecord.disabled = false;
        }
        if (index == $scope.Widgets.TotalRecordsIndex.caption) {
            $scope.Widgets.NextRecord.disabled = true;
        } else {
            $scope.Widgets.NextRecord.disabled = false;
        }

        if (pageNumber == 1) {
            $scope.Widgets.PreviousPage.disabled = true;
        } else {
            $scope.Widgets.PreviousPage.disabled = false;
        }

        if ((pageNumber * fetch) >= $scope.Widgets.TotalRecordsIndex.caption) {
            $scope.Widgets.NextPage.disabled = true;
        } else {
            $scope.Widgets.NextPage.disabled = false;
        }

        $timeout(setPageTitle);
    }

    var setPageTitle = function() {
        if ($scope.Widgets.SearchResultLiveList.selecteditem && $scope.Widgets.SearchResultLiveList.selecteditem.address_line1) {
            $scope.pageTitle = "Newline Portal - Buy - " + $scope.Widgets.SearchResultLiveList.selecteditem.address_line1 + " - " + $scope.Widgets.SearchResultLiveList.selecteditem.pin;
        } else {
            $scope.pageTitle = "Newline Portal - Buy";
        }
    };

    $scope.UpdateBidRateonSuccess = function(variable, data) {
        updateStatsBin();
    };

    function updateStatsBin() {
        $scope.Widgets.StatsContainer.Variables.svPortalAuctionDataFromPropertyID.invoke();
    }

    $scope.UpdateUnderwritingonSuccess = function(variable, data) {
        updateStatsBin();
    };

    $scope.qryPortalBuyFilteronSuccess = function(variable, data) {
        debugger;
        // On no Search Result
        if (data && data.length == 0) {
            // Disable Underwriting
            $scope.svQryPortalAuctionDataonSuccess({}, {
                content: []
            });
            $scope.Widgets.CurrentRecordIndex.caption = 0;
            checkNavigationControls(0);
        } else if (data && data.length > 0) {
            var indexToSet = 0;
            if (isForwardNavigation) {
                var currentIndex = ((pageNumber - 1) * fetch) + 1;
                $scope.Widgets.CurrentRecordIndex.caption = currentIndex;
                checkNavigationControls(currentIndex);
            } else {
                // If Search Result Count is less than Result Page Size then select last Search Result
                if (data.length < fetch) {
                    indexToSet = data.length - 1;
                } else {
                    indexToSet = fetch - 1;
                }
                var currentIndex1 = pageNumber * fetch;
                $scope.Widgets.CurrentRecordIndex.caption = currentIndex1;
                checkNavigationControls(currentIndex1);
                isForwardNavigation = true;
            }
            $timeout(function() {
                // Clear the existing selection
                $scope.Widgets.SearchResultLiveList.selecteditem = undefined;
                $scope.Widgets.SearchResultLiveList.selectItem($scope.Widgets.SearchResultLiveList.dataset[indexToSet]);

                setPageTitle();

                // Workaround for Google Stree View not getting loaded when hidden
                if (fixGoogleStreetView) {
                    $scope.$root.$emit("fix-google-street-view");
                    fixGoogleStreetView = false;
                }
            });
        }
    };

    $scope.qryPortalBuyFilteronBeforeUpdate = function(variable, data) {

        if (isNewSearch) {
            data.isNewSearch = true;
            data.previousUniqueSearchId = uniqueSearchId || "";
            uniqueSearchId = Date.now();
            isNewSearch = false;
        } else {
            data.isNewSearch = false;
            data.previousUniqueSearchId = "";
        }

        data.pageNumber = pageNumber;
        data.uniqueSearchId = uniqueSearchId;
    };

    $scope.SearchBtnClick = function($event, $isolateScope) {
        resetPaging();
        isNewSearch = true;
        $scope.Variables.qryPortalBuyFilter.invoke();
        // $scope.Widgets.FilterLayoutGrid.show = false;
    };

    var disableNavigation = function() {
        $scope.Widgets.PreviousRecord.disabled = true;
        $scope.Widgets.NextRecord.disabled = true;
        $scope.Widgets.PreviousPage.disabled = true;
        $scope.Widgets.NextPage.disabled = true;
    };

    $scope.PreviousRecordClick = function($event, $isolateScope) {
        disableNavigation();
        isForwardNavigation = false;
        $scope.navigateRecords(-1);
    };


    $scope.NextRecordClick = function($event, $isolateScope) {
        disableNavigation();
        isForwardNavigation = true;
        $scope.navigateRecords(1);
    };

    $scope.PreviousPageClick = function($event, $isolateScope) {
        disableNavigation();
        pageNumber = pageNumber - 1;
        $scope.Variables.qryPortalBuyFilter.invoke();
    };

    $scope.NextPageClick = function($event, $isolateScope) {
        disableNavigation();
        pageNumber = pageNumber + 1;
        $scope.Variables.qryPortalBuyFilter.invoke();
    };

    $scope.SearchKeypress = function($event, $isolateScope) {
        if ($event.which === 13) {
            $scope.SearchBtnClick({}, {});
        }
    };


    $scope.svQryPortalAuctionDataonSuccess = function(variable, data) {
        if (data.content.length === 0 || !data.content[0].underwriting_id) {
            $scope.UnderwritingStatusChange("");
            $scope.Widgets.BidRate.datavalue = "";
            $scope.Widgets.DeclineBtn.disabled = true;
            $scope.Widgets.PendingBtn.disabled = true;
            $scope.Widgets.ApproveBtn.disabled = true;
            $scope.Widgets.BidRate.disabled = true;
        } else {
            $scope.UnderwritingStatusChange(data.content[0].current_stat);
            $scope.Widgets.BidRate.datavalue = "" + data.content[0].current_bid;
            $scope.Widgets.DeclineBtn.disabled = false;
            $scope.Widgets.PendingBtn.disabled = false;
            $scope.Widgets.ApproveBtn.disabled = false;
            $scope.Widgets.BidRate.disabled = false;
        }
    };

    $scope.svQryCountyonSuccess = function(variable, data) {
        $scope.Widgets.County.datavalue = '';
        $scope.Widgets.Township.datavalue = '';
    };


    $scope.btnsearchClick = function($event, $isolateScope) {
        // ($scope.Widgets.FilterLayoutGrid.show = (!$scope.Widgets.FilterLayoutGrid.show) === true) ? false: true;
        // debugger;
        // $scope.Widgets.FilterLayoutGrid.show = !$scope.Widgets.FilterLayoutGrid.show
        if ($scope.Widgets.FilterLayoutGrid) {
            $scope.Widgets.FilterLayoutGrid.show = false;
        }
    };


    $scope.SearchFocus = function($event, $isolateScope) {
        // $scope.Widgets.FilterLayoutGrid.show = !$scope.Widgets.FilterLayoutGrid.show
        // debugger;
        if (!$scope.Widgets.FilterLayoutGrid == false) {
            $scope.Widgets.FilterLayoutGrid.show = true;
        }
    };


    $scope.MainSectionLabelClick = function($event, $isolateScope) {
        if ($scope.Widgets.FilterLayoutGrid) {
            $scope.Widgets.FilterLayoutGrid.show = false;
        }
    };


    $scope.AllContentRightTileClick = function($event, $isolateScope) {
        if ($scope.Widgets.FilterLayoutGrid) {
            $scope.Widgets.FilterLayoutGrid.show = false;
        }
    };





    $scope.svAuctionDayonSuccess = function(variable, data) {
        // Handled null Auction Day
        for (var i = 0; i < data.content.length; i++) {
            if (!data.content[i].auction_day) {
                data.content[i].auction_day = "null";
            }
        }
    };

}]);


Application.$controller("SearchResultLiveListController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);