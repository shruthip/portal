Application.$controller("AccrualsPageController", ["$scope", "DialogService", function($scope, DialogService) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };


    $scope.addAccrualClick = function($event, $isolateScope) {
        $scope.Variables.AccrualData.clearData();
        $scope.Variables.AccrualData.dataSet.mode = "Add";
        $scope.Variables.AccrualData.dataSet.certificate_id = $scope.$parent.Widgets.SearchResultLiveList.selecteditem.certificate_id;
        DialogService.open("dialogAddEditAccrual", $scope, {
            "selectedAccrual": null
        });
    };


    $scope.containerEditAccrualClick = function($event, $isolateScope, item, currentItemWidgets) {
        $scope.Variables.AccrualData.dataSet = item;
        $scope.Variables.AccrualData.dataSet.mode = "Edit";
        DialogService.open("dialogAddEditAccrual", $scope, {
            "selectedAccrual": item
        });
    };


    $scope.svAddAccrualonError = function(variable, data) {
        $scope.Variables.ErrorSQL.setMessage(data);
        $scope.Variables.ErrorSQL.invoke();
    };


    $scope.svUpdateAccrualonError = function(variable, data) {
        $scope.Variables.ErrorSQL.setMessage(data);
        $scope.Variables.ErrorSQL.invoke();
    };

}]);


Application.$controller("gridAccrualsController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("dialogAddEditAccrualController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;


        $scope.formAccrualSubmit = function($event, $isolateScope, $formData) {

            var selectedAccrualType = $.grep($scope.Variables.svAccrualTypes.dataSet.content, function(e) {
                return e.id == $scope.Widgets.selectAccrualType.datavalue;
            });

            var category = selectedAccrualType.length > 0 ? selectedAccrualType[0].disbursement_category : null;

            //invoke add or update
            if ($scope.Variables.AccrualData.dataSet.mode == "Add") {
                $scope.Variables.svAddAccrual.setInput("disbursement_category", category);
                $scope.Variables.svAddAccrual.invoke();
            } else if ($scope.Variables.AccrualData.dataSet.mode == "Edit") {
                $scope.Variables.svUpdateAccrual.setInput("disbursement_category", category);
                $scope.Variables.svUpdateAccrual.invoke();
            }
        };


        $scope.dialogAddEditAccrualOpened = function($event, $isolateScope) {
            $scope.Widgets.formAccrual.resetForm();
            if ($scope.Variables.AccrualData.dataSet.mode == "Edit") {
                if ($scope.Variables.AccrualData.dataSet.disbursement_id != undefined && $scope.Variables.AccrualData.dataSet.disbursement_id != null)
                    $scope.Widgets.textDisbursementID.datavalue = $scope.Variables.AccrualData.dataSet.disbursement_id;
                if ($scope.Variables.AccrualData.dataSet.penalty_period != undefined && $scope.Variables.AccrualData.dataSet.penalty_period != null)
                    $scope.Widgets.textPP.datavalue = $scope.Variables.AccrualData.dataSet.penalty_period;
                if ($scope.Variables.AccrualData.dataSet.revenue_amount != undefined && $scope.Variables.AccrualData.dataSet.revenue_amount != null)
                    $scope.Widgets.textRevenue.datavalue = $scope.Variables.AccrualData.dataSet.revenue_amount;
                if ($scope.Variables.AccrualData.dataSet.principal_amount != undefined && $scope.Variables.AccrualData.dataSet.principal_amount != null)
                    $scope.Widgets.textPrincipal.datavalue = $scope.Variables.AccrualData.dataSet.principal_amount;
            }
        };

    }
]);