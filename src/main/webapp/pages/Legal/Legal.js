Application.$controller("LegalPageController", ["$scope", "DialogService", "$timeout", function($scope, DialogService, $timeout) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

    $scope.svUpdateCaseonError = function(variable, data) {
        $scope.Variables.ErrorSQL.setMessage(data);
        $scope.Variables.ErrorSQL.invoke();
    };


    $scope.UpdateCasebtnClick = function($event, $isolateScope) {
        DialogService.open("UpdateCaseDialog", $scope, {
            "certificate_id": $scope.$parent.Widgets.SearchResultLiveList.selecteditem.certificate_id
        });
    };

    $scope.buttonMinusLegalClick = function(buttonId) {

        if (buttonId == 'minus_btnLegal') {
            document.getElementById("minus_btnLegal").style.display = 'none'; //step 2 :additional feature hide button
            document.getElementById("plus_btnLegal").style.display = ''; //step 3:additional feature show button
            $scope.Widgets.layoutgridLegal.show = "false";

        } else {
            document.getElementById("plus_btnLegal").style.display = 'none'; //step 2 :additional feature hide button
            document.getElementById("minus_btnLegal").style.display = ''; //step 3:additional feature show button
            $scope.Widgets.layoutgridLegal.show = "true";
        }
    };


    // $scope.svUpdateExpirationonSuccess = function(variable, data) {
    //     // $scope.Widgets.container17.Widgets.labelExpiration.caption = $scope.Widgets.dateExpiration.datavalue;
    //     $scope.Widgets.container17.Variables.svCertificateDataByPropertyID.invoke();
    //     if ($scope.Variables.svQryLegalDetails.dataSet.numberOfElements > 0) {
    //         $scope.Variables.svUpdateCase.invoke();
    //     } else {
    //         $scope.Variables.SuccessUpdateCase.invoke();
    //         DialogService.close("UpdateCaseDialog");
    //     }
    // };

    // $scope.svUpdateExpirationonError = function(variable, data) {
    //     $scope.Variables.ErrorSQL.setMessage(data);
    //     $scope.Variables.ErrorSQL.invoke();
    // };


    $scope.button3Click = function($event, $isolateScope) {
        DialogService.open("UpdateCaseDialog", $scope, {
            "certificate_id": $scope.$parent.Widgets.SearchResultLiveList.selecteditem.certificate_id
        });
    };


    $scope.svQryCloseMatteronSuccess = function(variable, data) {
        $scope.Widgets.legalGeneratePdfContainer.content = "";
        $scope.Widgets.legalGeneratePdfContainer.content = "GeneratePdf";
    };


    $scope.button4Click = function($event, $isolateScope) {
        //$scope.Widgets.spinnerCloseCase.show = true;
        $scope.$root.Widgets.spCloseCase.show = true;
    };


    $scope.svProcUpdateCaseonSuccess = function(variable, data) {
        if ($scope.$parent.Widgets.tabpane1.Variables) {
            $scope.$parent.Widgets.tabpane1.Variables.svQryDisbursement.invoke();
        }
        if ($scope.$parent.Widgets.tabpane2.Variables) {
            $scope.$parent.Widgets.tabpane2.Variables.svQryAccruals.invoke();
        }
        if ($scope.$parent.Widgets.tabpane3.Variables) {
            $scope.$parent.Widgets.tabpane3.Variables.svQryPayments.invoke();
        }
        $timeout(function() {
            $scope.Variables.svQryLegalDetails.invoke();
        }, 500);
    };


    $scope.svProcUpdateCaseonError = function(variable, data) {
        $scope.Variables.ErrorSQL.setMessage(data);
        $scope.Variables.ErrorSQL.invoke();
    };


    $scope.svQryCloseMatteronError = function(variable, data) {
        $scope.Variables.ErrorSQL.setMessage(data);
        $scope.Variables.ErrorSQL.invoke();
        $scope.$parent.Widgets.spCloseCase.show = false;
        $scope.Widgets.confirmdialog1.close()
    };

}]);


Application.$controller("UpdateCaseDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
        $scope.certificate_id;

        $scope.formCaseDetailsSubmit = function($event, $isolateScope, $formData) {

            var matterId = $scope.Widgets.labelMatterID.caption;
            if (matterId !== '' && matterId !== undefined) {
                console.log($scope.Variables.svUpdateCaseCertificate);
                $scope.Variables.svUpdateCase.invoke();
            } else {
                $scope.Variables.svProcUpdateCase.invoke();
            }
        };


        $scope.UpdateCaseDialogOpened = function($event, $isolateScope) {
            $scope.Widgets.formCaseDetails.resetForm();
        };

    }
]);

Application.$controller("dialog2Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("confirmdialog1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.confirmdialog1Ok = function($event, $isolateScope) {
            $scope.$root.Widgets.spCloseCase.show = true;
        };

    }
]);