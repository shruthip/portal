Application.$controller("servicePageController", ["$scope", "$timeout", "DialogService", "routeParams", function($scope, $timeout, DialogService, routeParams) {
    "use strict";

    var fetch;
    var pageNumber;
    var isForwardNavigation = true;
    var isNewSearch = true;
    var uniqueSearchId;

    // Workaround for Google Stree View not getting loaded when hidden
    var fixGoogleStreetView = true;

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        // Fetch => Page Size, pageNumber => Current Page Number
        fetch = $scope.Variables.qryPortalServiceFilter.maxResults;

        //Resize Search Result
        $(window).resize(function() {
            // $scope.Widgets.SearchResultLiveList.height = ($(window).height() - 160);
            $scope.Widgets.AllContentRightTile.height = ($(window).height() - 60);
            $scope.Widgets.LeftPanel.height = ($(window).height() - 60);
        });

        $(window).resize();

        // Search if the Page has search Parameter
        if ($scope.pageParams.search) {
            $scope.SearchBtnClick({}, {});
        }

        startCertificateChangeWatch();
    };

    var startCertificateChangeWatch = function() {

        if ($scope.Widgets.SearchResultLiveList && $scope.Widgets.SearchResultLiveList.selecteditem) {

            $scope.$watch(function() {
                return $scope.Widgets.SearchResultLiveList.selecteditem;
            }, function(newVal, oldVal) {
                if (newVal && newVal.constructor.name === "Object") {
                    routeParams.replace({
                        "search": newVal.certificate_id
                    }, {
                        "reload": false,
                        "history": true
                    });
                } else {
                    routeParams.replace({
                        "search": ""
                    }, {
                        "reload": false,
                        "history": true
                    });
                }
            }, true);
        } else {
            $timeout(startCertificateChangeWatch, 100);
        }
    };

    $scope.SearchResultLiveListClick = function($event, $isolateScope) {
        // Find the current result index and update the Record Count UI
        var index = findCurrentIndex();
        var currentIndex = ((pageNumber - 1) * fetch) + index;
        $scope.Widgets.CurrentRecordIndex.caption = currentIndex;
        checkNavigationControls(currentIndex);
    };

    $scope.ClearBtnClick = function($event, $isolateScope) {
        $scope.Widgets.Search.datavalue = "";
        $scope.Widgets.State.datavalue = "";
        $scope.Widgets.County.datavalue = "";
        $scope.Widgets.TaxYear.datavalue = "";
        $scope.Widgets.CertificateStatus.datavalue = [];
        $scope.Widgets.ExpirationDateOperator.datavalue = "<=";
        $scope.Widgets.ExpirationDate.datavalue = "";
        $scope.Widgets.TagOperator.datavalue = "=";
        $scope.Widgets.Tag.datavalue = "";
        resetPaging();
        // $scope.Widgets.FilterLayoutGrid.show = false;

    };

    $scope.anchor2Click = function($event, $isolateScope) {
        $scope.Widgets.anchor2.show = false;
        $scope.Widgets.anchor3.show = true;
        $('div[name="gridcolumn5"]').show();
        $('div[name="gridcolumn6"]').css('width', '83.33%');
    };


    $scope.anchor3Click = function($event, $isolateScope) {
        $scope.Widgets.anchor2.show = true;
        $scope.Widgets.anchor3.show = false;
        $('div[name="gridcolumn5"]').hide();
        $('div[name="gridcolumn6"]').css('width', '100%');
    };

    function resetPaging() {
        // pageNumber => Current Page Number
        pageNumber = 1;
    }

    resetPaging();

    $scope.toggleFilter = function() {
        $scope.Widgets.FilterLayoutGrid.show = !$scope.Widgets.FilterLayoutGrid.show;
    };

    // Navigate Recrods - If flag is 1 then show Next Result, If flag is -1 then show Previous Result
    $scope.navigateRecords = function(flag) {
        // Find current selected Live List Index
        var index = findCurrentIndex();
        // Find current selected index in Total Search Result
        var currentIndexInTotal = ((pageNumber - 1) * fetch) + index;
        if (flag === 1) { // On Next Result
            if (currentIndexInTotal < $scope.Widgets.TotalRecordsIndex.caption) {
                if (index < fetch) {
                    $scope.Widgets.SearchResultLiveList.deselectItem($scope.Widgets.SearchResultLiveList.dataset[index - 1]);
                    $scope.Widgets.SearchResultLiveList.selectItem($scope.Widgets.SearchResultLiveList.dataset[index]);
                    checkNavigationControls(currentIndexInTotal + 1);
                } else {
                    $scope.NextPageClick({}, {});
                }
                $scope.Widgets.CurrentRecordIndex.caption = currentIndexInTotal + 1;
            } else {
                alert("You have reached the end of Search Result.");
            }
        } else if (flag === -1) { // On Previous Result
            if (currentIndexInTotal > 1) {
                if (index > 1) {
                    $scope.Widgets.SearchResultLiveList.deselectItem($scope.Widgets.SearchResultLiveList.dataset[index - 1]);
                    $scope.Widgets.SearchResultLiveList.selectItem($scope.Widgets.SearchResultLiveList.dataset[index - 2]);
                    checkNavigationControls(currentIndexInTotal - 1);
                } else {
                    $scope.PreviousPageClick({}, {});
                }
                $scope.Widgets.CurrentRecordIndex.caption = currentIndexInTotal - 1;
            } else {
                alert("You have reached the start of Search Result.");
            }
        }
    };

    function findCurrentIndex() {
        var index = 0;
        var sourceObj = $scope.Widgets.SearchResultLiveList.dataset;
        var selectedObj = $scope.Widgets.SearchResultLiveList.selecteditem;
        if (sourceObj && sourceObj.length > 0) {
            for (var i = 0; i < sourceObj.length; i++) {
                if (sourceObj[i].property_id === selectedObj.property_id) {
                    index = i + 1;
                    break;
                }
            }
        }
        return index;
    }

    function checkNavigationControls(index) {
        if (index <= 1) {
            $scope.Widgets.PreviousRecord.disabled = true;
        } else {
            $scope.Widgets.PreviousRecord.disabled = false;
        }
        if (index == $scope.Widgets.TotalRecordsIndex.caption) {
            $scope.Widgets.NextRecord.disabled = true;
        } else {
            $scope.Widgets.NextRecord.disabled = false;
        }

        if (pageNumber == 1) {
            $scope.Widgets.PreviousPage.disabled = true;
        } else {
            $scope.Widgets.PreviousPage.disabled = false;
        }

        if ((pageNumber * fetch) >= $scope.Widgets.TotalRecordsIndex.caption) {
            $scope.Widgets.NextPage.disabled = true;
        } else {
            $scope.Widgets.NextPage.disabled = false;
        }

        $timeout(setPageTitle);
    }

    var setPageTitle = function() {
        if ($scope.Widgets.SearchResultLiveList.selecteditem && $scope.Widgets.SearchResultLiveList.selecteditem.address_line1) {
            $scope.pageTitle = "Newline Portal - Service - " + $scope.Widgets.SearchResultLiveList.selecteditem.address_line1 + " - " + $scope.Widgets.SearchResultLiveList.selecteditem.certificate_number;
        } else {
            $scope.pageTitle = "Newline Portal - Service";
        }
    };

    $scope.picture1Mouseenter = function($event, $isolateScope) {
        $("filterwhite").hide();
        document.getElementById("filterwhite").style.display = "none"
        document.getElementById("filterblue").style.display = "block"
    };

    $scope.picture1Mouseleave = function($event, $isolateScope) {
        document.getElementById("filterwhite").style.display = "block"
        document.getElementById("filterblue").style.display = "none"
    };

    //--- check query service page
    $scope.qryPortalServiceFilteronSuccess = function(variable, data) {
        // On no Search Result
        if (data && data.length == 0) {
            $scope.Widgets.CurrentRecordIndex.caption = 0;
            checkNavigationControls(0);
        } else if (data && data.length > 0) {
            var indexToSet = 0;
            if (isForwardNavigation) {
                var currentIndex = ((pageNumber - 1) * fetch) + 1;
                $scope.Widgets.CurrentRecordIndex.caption = currentIndex;
                checkNavigationControls(currentIndex);
            } else {
                // If Search Result Count is less than Result Page Size then select last Search Result
                if (data.length < fetch) {
                    indexToSet = data.length - 1;
                } else {
                    indexToSet = fetch - 1;
                }
                var currentIndex1 = pageNumber * fetch;
                $scope.Widgets.CurrentRecordIndex.caption = currentIndex1;
                checkNavigationControls(currentIndex1);
                isForwardNavigation = true;
            }
            $timeout(function() {
                // Clear the existing selection
                $scope.Widgets.SearchResultLiveList.selecteditem = undefined;
                $scope.Widgets.SearchResultLiveList.selectItem($scope.Widgets.SearchResultLiveList.dataset[indexToSet]);
                setPageTitle();

                // Workaround for Google Stree View not getting loaded when hidden
                if (fixGoogleStreetView) {
                    $scope.$root.$emit("fix-google-street-view");
                    fixGoogleStreetView = false;
                }
            });
        }
    };

    $scope.qryPortalServiceFilteronBeforeUpdate = function(variable, data) {
        if (isNewSearch) {
            data.isNewSearch = true;
            data.previousUniqueSearchId = uniqueSearchId || "";
            uniqueSearchId = Date.now();
            isNewSearch = false;
        } else {
            data.isNewSearch = false;
            data.previousUniqueSearchId = "";
        }

        data.pageNumber = pageNumber;
        data.uniqueSearchId = uniqueSearchId;
    };

    $scope.SearchBtnClick = function($event, $isolateScope) {
        resetPaging();
        isNewSearch = true;
        $scope.Variables.qryPortalServiceFilter.invoke();
        // $scope.Widgets.FilterLayoutGrid.show = false;
    };

    var disableNavigation = function() {
        $scope.Widgets.PreviousRecord.disabled = true;
        $scope.Widgets.NextRecord.disabled = true;
        $scope.Widgets.PreviousPage.disabled = true;
        $scope.Widgets.NextPage.disabled = true;
    };

    $scope.PreviousRecordClick = function($event, $isolateScope) {
        disableNavigation();
        isForwardNavigation = false;
        $scope.navigateRecords(-1);
    };

    $scope.NextRecordClick = function($event, $isolateScope) {
        disableNavigation();
        isForwardNavigation = true;
        $scope.navigateRecords(1);
    };

    $scope.PreviousPageClick = function($event, $isolateScope) {
        disableNavigation();
        pageNumber = pageNumber - 1;
        $scope.Variables.qryPortalServiceFilter.invoke();
    };

    $scope.NextPageClick = function($event, $isolateScope) {
        disableNavigation();
        pageNumber = pageNumber + 1;
        $scope.Variables.qryPortalServiceFilter.invoke();
    };

    $scope.SearchKeypress = function($event, $isolateScope) {
        if ($event.which === 13) {
            $scope.SearchBtnClick({}, {});
        }
    };

    $scope.svQryCountyonSuccess = function(variable, data) {
        $scope.Widgets.County.datavalue = '';
    };

    $scope.button12Click = function($event, $isolateScope) {
        DialogService.open("dialogAddTransaction", $scope);
    };


    $scope.svAddDisbursementonError = function(variable, data) {
        $scope.Variables.ErrorSQL.setMessage(data);
        $scope.Variables.ErrorSQL.invoke();
    };


    $scope.svAddAccrualTransactiononError = function(variable, data) {
        $scope.Variables.ErrorSQL.setMessage(data);
        $scope.Variables.ErrorSQL.invoke();
    };


    $scope.svAddPaymentTransactiononError = function(variable, data) {
        $scope.Variables.ErrorSQL.setMessage(data);
        $scope.Variables.ErrorSQL.invoke();
    };


    $scope.svAddDisbursementTransactiononSuccess = function(variable, data) {
        if ($scope.Widgets.tabpane1.Variables.svQryDisbursement) {
            $scope.Widgets.tabpane1.Variables.svQryDisbursement.invoke();
        }
    };


    $scope.svAddAccrualTransactiononSuccess = function(variable, data) {
        if ($scope.Widgets.tabpane2.Variables) {
            $scope.Widgets.tabpane2.Variables.svQryAccruals.invoke();
        }
    };


    $scope.svAddPaymentTransactiononSuccess = function(variable, data) {
        if ($scope.Widgets.tabpane3.Variables) {
            $scope.Widgets.tabpane3.Variables.svQryPayments.invoke();
        }
    };


    $scope.btnsearchClick = function($event, $isolateScope) {
        // $scope.Widgets.FilterLayoutGrid.show = !$scope.Widgets.FilterLayoutGrid.show;

        if ($scope.Widgets.FilterLayoutGrid) {
            $scope.Widgets.FilterLayoutGrid.show = false;
        }
    };


    $scope.SearchFocus = function($event, $isolateScope) {
        if (!$scope.Widgets.FilterLayoutGrid == false) {
            $scope.Widgets.FilterLayoutGrid.show = true;
        }
    };


    $scope.MainSectionLabelClick = function($event, $isolateScope) {
        if ($scope.Widgets.FilterLayoutGrid) {
            $scope.Widgets.FilterLayoutGrid.show = false;
        }
    };


    $scope.AllContentRightTileClick = function($event, $isolateScope) {
        if ($scope.Widgets.FilterLayoutGrid) {
            $scope.Widgets.FilterLayoutGrid.show = false;
        }
    };

}]);

Application.$controller("SearchResultLiveListController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("dialogAddTransactionController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.dialogAddTransactionOpened = function($event, $isolateScope) {
            $scope.Widgets.formDisbursement.resetFrom();
            $scope.Widgets.formAccural.resetFrom();
            $scope.Widgets.formPayment.resetFrom();
        };

    }
]);

Application.$controller("dialog2Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);