Application.$controller("TaxAndHistoryDetailsPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

    $scope.svQryUpdateAuctiononError = function(variable, data) {
        $scope.Variables.ncAuctionFailure.setMessage("Error while updating Auction.  Please check the values. <br>" + data);
        $scope.Variables.ncAuctionFailure.invoke();
    };


    $scope.svQryUpdateAuctionResultonError = function(variable, data) {
        $scope.Variables.ncAuctionResultFailure.setMessage("Error while updating Auction Result.  Please check the values. <br>" + data);
        $scope.Variables.ncAuctionResultFailure.invoke();
    };


    $scope.svUpdateAuctiononError = function(variable, data) {
        $scope.Variables.ncAuctionFailure.setMessage("Error while updating Auction.  Please check the values. <br>" + data);
        $scope.Variables.ncAuctionFailure.invoke();

    };


    $scope.AuctionsGridRowupdate = function($event, $isolateScope, $rowData) {
        //If Auction Results record is not there then don't set Input data
        if ($rowData.showRecord == "true") {
            $scope.Variables.svUpdateAuction.setInput("status", $rowData.status);
            $scope.Variables.svUpdateAuction.setInput("disposition_date", $rowData.dispositionDate);
            $scope.Variables.svUpdateAuction.setInput("disposition_research_date", $rowData.dispositionResearchDate);
        }
        $scope.Variables.svUpdateAuction.setInput("auction_id", $rowData.auctionId);
        $scope.Variables.svUpdateAuction.setInput("sale_amount", $rowData.saleAmount);
        $scope.Variables.svUpdateAuction.setInput("prior_years_due", $rowData.priorYearsDue);

        $scope.Variables.svUpdateAuction.invoke();
    };


    $scope.AuctionsGridBeforeformrender = function($event, $isolateScope, $rowData, $operation) {
        if ($rowData.showRecord == "true") {
            $isolateScope.columns.status.readonly = false;
            $isolateScope.columns.dispositionDate.readonly = false;
            $isolateScope.columns.dispositionResearchDate.readonly = false;
        } else {
            $isolateScope.columns.status.readonly = true;
            $isolateScope.columns.dispositionDate.readonly = true;
            $isolateScope.columns.dispositionResearchDate.readonly = true;
        }
    };

}]);



Application.$controller("TaxHistoryResearchGridController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("AuctionsGridController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("AssessmentsGridController", ["$scope",
	function($scope) {
		"use strict";
		$scope.ctrlScope = $scope;
	}
]);

Application.$controller("AssessmentLiveFormController", ["$scope",
	function($scope) {
		"use strict";
		$scope.ctrlScope = $scope;
	}
]);