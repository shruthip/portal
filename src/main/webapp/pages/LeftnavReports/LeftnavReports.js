Application.$controller("LeftnavReportsPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on the variables within this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         */
    };

    /* perform any action on widgets within this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        $scope.TablesAnchorGroupClick();
    };

    $scope.TablesAnchorGroupClick = function() {

        $scope.Widgets.Buyer.show = true;
        $scope.Widgets.BuyerGroup.show = true;
        $scope.Widgets.County.show = true;
        $scope.Widgets.Grade.show = true;
        $scope.Widgets.States.show = true;
        $scope.Widgets.Township.show = true;
        $scope.Widgets.Interest.show = true;
        $scope.Widgets.PartyAgent.show = true;
        $scope.Widgets.Inspection.show = true;
        $scope.Widgets.User.show = true;
        $scope.Widgets.Group.show = true;

        $scope.Widgets.TemplateItem1.show = false;
        $scope.Widgets.TemplateItem2.show = false;
        $scope.Widgets.TemplateItem3.show = false;
    };

    $scope.TemplatesAnchorGroupClick = function() {

        $scope.Widgets.TemplateItem1.show = true;
        $scope.Widgets.TemplateItem2.show = true;
        $scope.Widgets.TemplateItem3.show = true;

        $scope.Widgets.Buyer.show = false;
        $scope.Widgets.BuyerGroup.show = false;
        $scope.Widgets.County.show = false;
        $scope.Widgets.Grade.show = false;
        $scope.Widgets.States.show = false;
        $scope.Widgets.Township.show = false;
        $scope.Widgets.Interest.show = false;
        $scope.Widgets.PartyAgent.show = false;
        $scope.Widgets.Inspection.show = false;
        $scope.Widgets.User.show = false;
        $scope.Widgets.Group.show = false;

    };

    var previousActiveAnchorGroup = "TablesAnchorGroup";
    var previousActiveAnchor = "anchor2";
    $scope.AnchorClick = function(activeAnchorGroup, activeAnchor) {
        $scope.Widgets[previousActiveAnchorGroup].class = "";
        $scope.Widgets[previousActiveAnchor].class = "submenu";
        $scope.Widgets[activeAnchorGroup].class = "active";
        $scope.Widgets[activeAnchor].class = "submenu active";
        previousActiveAnchorGroup = activeAnchorGroup;
        previousActiveAnchor = activeAnchor;
    };

}]);