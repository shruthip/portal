Application.$controller("TaxAndHistoryPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        if ($scope.activePageName === "Main") {
            $scope.Widgets.tax_sales_header_plain.show = "false";
        } else if ($scope.activePageName === "inspect" || $scope.activePageName === "InspectMobile") {
            $scope.Widgets.tax_sales_header_blue_bg.show = "false";
        }
        if ($scope.activePageName === "InspectMobile") {
            $scope.Widgets.layoutgrid1.show = "false";
            $scope.Widgets.layoutgrid_mobile.show = "true"
        }
        if ($scope.activePageName === "inspect") {
            $scope.Widgets.layoutgrid1.show = "false";
            $scope.Widgets.layoutgrid_mobile.show = "true"
        }
        if ($scope.activePageName != "InspectMobile" && $scope.activePageName != "inspect") {
            $scope.Widgets.layoutgrid_mobile.show = "false"
        }
    };

}]);


Application.$controller("TaxAndHistoryDetailsDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.TaxAndHistoryDetailsDialogClose = function($event, $isolateScope) {
            $scope.$parent.$parent.$parent.Widgets.panel2.Variables.svPortalAuctionHistoryFromPropertyID.setInput("property_id", $scope.$parent.$parent.Widgets.SearchResultLiveList.selecteditem.property_id);
            $scope.$parent.$parent.$parent.Widgets.panel2.Variables.svPortalAuctionHistoryFromPropertyID.invoke();
        };

    }
]);