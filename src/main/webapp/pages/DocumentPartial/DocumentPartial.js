Application.$controller("DocumentPartialPageController", ["$scope", "$compile", function($scope, $compile) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        $scope.Widgets.accordionpane.$element.find('.panel-heading').click();
        $scope.Widgets.accordionpane.$element.find('.panel-heading').each(function(index) {
            var $header = WM.element(this),
                $button = '<button ng-click="onAccordionButtonClick($event, ' + index + ')" class="btn-default btn app-button" type="button"><span class="btn-caption">Add Doc</span></button>',
                $compiledTl = $compile($button)($scope);
            $header.find('.panel-actions').prepend($compiledTl);

        });

    };

    $scope.onAccordionButtonClick = function($event, index) {
        $event.stopPropagation();
        $scope.Widgets.UploadPdfDialog.open();

    };

    $scope.getEscapedUrl = function(item) {
        var tempUrl = "";
        if (item) {
            tempUrl = location.pathname + "services/pdfJava/DownloadFromAzureStorage?url=" + escape(item.link);
        }
        return tempUrl;
    };
}]);

Application.$controller("UploadPdfDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("deleteBlobFromAzureController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);