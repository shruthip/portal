Application.$controller("TopnavtoolsPageController", ["$scope", "DialogService", function($scope, DialogService) {
    "use strict";

    /* perform any action on the variables within this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         */
    };

    /* perform any action on widgets within this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        $(".panel-heading").hide();
        $scope.Widgets.accordionpane1.collapse();
        $scope.Widgets.accordionpane3.collapse();
    };

    $scope.button4Click = function($event, $isolateScope) {
        $scope.Widgets.accordionpane3.collapse();
        $scope.Widgets.accordionpane1.togglePane();
    };


    $scope.button6Click = function($event, $isolateScope) {
        $scope.Widgets.accordionpane1.collapse();
        $scope.Widgets.accordionpane3.togglePane();
    };
    $scope.selectToolsChange = function($event, $isolateScope, newVal, oldVal) {
        if (newVal == "RunUnderwriting") {
            $scope.Variables.RunUnderwriting.invoke();
        } else if (newVal == "CompleteSale") {
            DialogService.open("CompleteSaleDialog", $scope, {});
        } else if (newVal == "ProcessRedemptions") {
            $scope.Variables.ProcessRedemption.invoke();
            // $scope.Variables.SettleRedemption.invoke();
        } else if (newVal == "PaySearchFee") {
            DialogService.open("PaySearchDialog", $scope, {});
        } else if (newVal == "PayTakeNotice") {
            DialogService.open("PayTakeNoticeDialog", $scope, {});
        } else if (newVal == "PayPublication") {
            DialogService.open("PayPublicationDialog", $scope, {});
        }
    };

    $scope.NameClick = function($event, $isolateScope, item, currentItemWidgets) {
        // debugger;
        if (item.dataValue == "RunUnderwriting") {
            $scope.Variables.RunUnderwriting.invoke();
        } else if (item.dataValue == "CompleteSale") {
            DialogService.open("CompleteSaleDialog", $scope, {});
        } else if (item.dataValue == "ProcessRedemptions") {
            $scope.Variables.ProcessRedemption.invoke();
            // $scope.Variables.SettleRedemption.invoke();
        } else if (item.dataValue == "PaySearchFee") {
            DialogService.open("PaySearchDialog", $scope, {});
        } else if (item.dataValue == "PayTakeNotice") {
            DialogService.open("PayTakeNoticeDialog", $scope, {});
        } else if (item.dataValue == "PayPublication") {
            DialogService.open("PayPublicationDialog", $scope, {});
        }

    };

    $scope.NameMouseenter = function($event, $isolateScope, item, currentItemWidgets) {
        // if (item.dataValue == "RunUnderwriting") {
        //     $(item.dataValue).mouseover(function() {
        //         $("label").css("background-color", "yellow");
        //     });
        //     $("label").mouseout(function() {
        //         $("label").css("background-color", "lightgray");
        //     });
        // }
    };

    $scope.CompleteSaleonError = function(variable, data) {
        $scope.Variables.ErrorCompleteSale.setMessage(data);
        $scope.Variables.ErrorCompleteSale.invoke();
    };


    $scope.PaySearchFeeonError = function(variable, data) {
        $scope.Variables.ErrorCompleteSale.setMessage(data);
        $scope.Variables.ErrorCompleteSale.invoke();
    };


    $scope.PayTakeNoticeonError = function(variable, data) {
        $scope.Variables.ErrorCompleteSale.setMessage(data);
        $scope.Variables.ErrorCompleteSale.invoke();
    };


    $scope.PayPublicationonError = function(variable, data) {
        $scope.Variables.ErrorCompleteSale.setMessage(data);
        $scope.Variables.ErrorCompleteSale.invoke();
    };
    $scope.Reports_btnClick = function($event, $isolateScope) {
        window.open("https://www.periscopedata.com", "_blank");
    };

    $scope.Import_btnClick = function($event, $isolateScope) {
        window.open("https://app.skyvia.com", "_blank");
    };




}]);

Application.$controller("CompleteSaleDialogController", ["$scope", "DialogService",
    function($scope, DialogService) {
        "use strict";
        $scope.ctrlScope = $scope;
        $scope.showSearch = false;
        $scope.certificatesFound = 0;

        // $scope.containerSearchSaleClick = function($event, $isolateScope) {
        //     $scope.showSearch = true;
        // };


        $scope.formCompleteSaleSubmit = function($event, $isolateScope, $formData) {
            // $scope.showSearch = true;
            // $scope.Widgets.textCertificatesFound.datavalue = "0 Certificates Found";
            if ($scope.Widgets.textVolumeFromSale.datavalue)
                $scope.Variables.CompleteSale.setInput("volume_floor", $scope.Widgets.textVolumeFromSale.datavalue);
            else
            //$scope.Variables.CompleteSale.setInput("volume_floor", undefined);
                $scope.Variables.CompleteSale.setInput("volume_floor", 0);
            if ($scope.Widgets.textVolumeToSale.datavalue)
                $scope.Variables.CompleteSale.setInput("volume_ceiling", $scope.Widgets.textVolumeToSale.datavalue);
            else
            //$scope.Variables.CompleteSale.setInput("volume_ceiling", undefined);
                $scope.Variables.CompleteSale.setInput("volume_ceiling", 1000);
            $scope.Variables.CompleteSale.setInput("preview", 1);

            $scope.Variables.CompleteSale.invoke({}, function(data) {
                if (data.length > 0) {
                    var currentData = data[0];
                    $scope.certificatesFound = currentData["count_records"];
                    $scope.Widgets.textCertificatesFound.datavalue = $scope.certificatesFound + " Certificates Found";
                    $scope.showSearch = true;
                }
            });
        };


        // $scope.CompleteSaleDialogClose = function($event, $isolateScope) {
        //     // $scope.Widgets.formCompleteSale.resetForm();
        //     $scope.Variables.CompleteSale.setInput("volume_floor", null);
        //     $scope.Variables.CompleteSale.setInput("volume_ceiling", null);
        // };

        $scope.buttonUpdateSaleClick = function($event, $isolateScope) {
            if ($scope.Widgets.textVolumeFromSale.datavalue)
                $scope.Variables.CompleteSale.setInput("volume_floor", $scope.Widgets.textVolumeFromSale.datavalue);
            else
            //$scope.Variables.CompleteSale.setInput("volume_floor", undefined);
                $scope.Variables.CompleteSale.setInput("volume_floor", 0);
            if ($scope.Widgets.textVolumeToSale.datavalue)
                $scope.Variables.CompleteSale.setInput("volume_ceiling", $scope.Widgets.textVolumeToSale.datavalue);
            else
            //$scope.Variables.CompleteSale.setInput("volume_ceiling", undefined);
                $scope.Variables.CompleteSale.setInput("volume_ceiling", 1000);
            $scope.Variables.CompleteSale.setInput("preview", 0);
            $scope.Variables.CompleteSale.invoke({}, function() {
                $scope.Variables.SuccessCompleteSale.invoke();
                DialogService.close("CompleteSaleDialog");
            });
        };

    }
]);

Application.$controller("PaySearchDialogController", ["$scope", "DialogService",
    function($scope, DialogService) {
        "use strict";
        $scope.ctrlScope = $scope;
        $scope.showSearch = false;
        $scope.certificatesFound = 0;

        $scope.formPaySearchSubmit = function($event, $isolateScope, $formData) {
            // $scope.showSearch = true;
            // $scope.Widgets.textCertificatesFoundFee.datavalue = "0 Certificates Found";
            if ($scope.Widgets.textVolumeFromFee.datavalue)
                $scope.Variables.PaySearchFee.setInput("volume_floor", $scope.Widgets.textVolumeFromFee.datavalue);
            else
            //$scope.Variables.PaySearchFee.setInput("volume_floor", undefined);
                $scope.Variables.PaySearchFee.setInput("volume_floor", 0);
            if ($scope.Widgets.textVolumeToFee.datavalue)
                $scope.Variables.PaySearchFee.setInput("volume_ceiling", $scope.Widgets.textVolumeToFee.datavalue);
            else
            //$scope.Variables.PaySearchFee.setInput("volume_ceiling", undefined);
                $scope.Variables.PaySearchFee.setInput("volume_ceiling", 1000);
            $scope.Variables.PaySearchFee.setInput("reference", $scope.Widgets.textReferenceFee.datavalue);
            $scope.Variables.PaySearchFee.setInput("preview", 1);
            $scope.Variables.PaySearchFee.invoke({}, function(data) {
                if (data.length > 0) {
                    var currentData = data[0];
                    $scope.certificatesFound = currentData["count_records"];
                    $scope.Widgets.textCertificatesFoundFee.datavalue = $scope.certificatesFound + " Certificates Found";
                    $scope.showSearch = true;
                }
            });
        };


        $scope.buttonPriorSearchClick = function($event, $isolateScope) {
            // $scope.Variables.PaySearchFee.setInput("volume_floor", $scope.Widgets.textVolumeFromFee.datavalue);
            // $scope.Variables.PaySearchFee.setInput("volume_ceiling", $scope.Widgets.textVolumeToFee.datavalue);
            if ($scope.Widgets.textVolumeFromFee.datavalue)
                $scope.Variables.PaySearchFee.setInput("volume_floor", $scope.Widgets.textVolumeFromFee.datavalue);
            else
            //$scope.Variables.PaySearchFee.setInput("volume_floor", undefined);
                $scope.Variables.PaySearchFee.setInput("volume_floor", 0);
            if ($scope.Widgets.textVolumeToFee.datavalue)
                $scope.Variables.PaySearchFee.setInput("volume_ceiling", $scope.Widgets.textVolumeToFee.datavalue);
            else
            //$scope.Variables.PaySearchFee.setInput("volume_ceiling", undefined);
                $scope.Variables.PaySearchFee.setInput("volume_ceiling", 1000);
            $scope.Variables.PaySearchFee.setInput("reference", $scope.Widgets.textReferenceFee.datavalue);
            $scope.Variables.PaySearchFee.setInput("preview", 0);
            $scope.Variables.PaySearchFee.invoke({}, function() {
                $scope.Variables.SuccessPayFee.invoke();
                DialogService.close("PaySearchDialog");
            });
        };

    }
]);

Application.$controller("PayTakeNoticeDialogController", ["$scope", "DialogService",
    function($scope, DialogService) {
        "use strict";
        $scope.ctrlScope = $scope;
        $scope.showSearch = false;
        $scope.certificatesFound = 0;

        $scope.formPayTakeNoticeSubmit = function($event, $isolateScope, $formData) {
            // $scope.Widgets.textCertificatesFoundNotice.datavalue = "0 Certificates Found";
            if ($scope.Widgets.textVolumeFromNotice.datavalue)
                $scope.Variables.PayTakeNotice.setInput("volume_floor", $scope.Widgets.textVolumeFromNotice.datavalue);
            else
            //$scope.Variables.PayTakeNotice.setInput("volume_floor", undefined);
                $scope.Variables.PayTakeNotice.setInput("volume_floor", 0);
            if ($scope.Widgets.textVolumeToNotice.datavalue)
                $scope.Variables.PayTakeNotice.setInput("volume_ceiling", $scope.Widgets.textVolumeToNotice.datavalue);
            else
            //$scope.Variables.PayTakeNotice.setInput("volume_ceiling", undefined);
                $scope.Variables.PayTakeNotice.setInput("volume_ceiling", 1000);
            $scope.Variables.PayTakeNotice.setInput("reference", $scope.Widgets.textReferenceNotice.datavalue);
            $scope.Variables.PayTakeNotice.setInput("preview", 1);
            $scope.Variables.PayTakeNotice.invoke({}, function(data) {
                if (data.length > 0) {
                    var currentData = data[0];
                    $scope.certificatesFound = currentData["count_records"];
                    $scope.Widgets.textCertificatesFoundNotice.datavalue = $scope.certificatesFound + " Certificates Found";
                    $scope.showSearch = true;
                }
            });
        };


        $scope.buttonPayNoticeClick = function($event, $isolateScope) {
            if ($scope.Widgets.textVolumeFromNotice.datavalue)
                $scope.Variables.PayTakeNotice.setInput("volume_floor", $scope.Widgets.textVolumeFromNotice.datavalue);
            else
            //$scope.Variables.PayTakeNotice.setInput("volume_floor", undefined);
                $scope.Variables.PayTakeNotice.setInput("volume_floor", 0);
            if ($scope.Widgets.textVolumeToNotice.datavalue)
                $scope.Variables.PayTakeNotice.setInput("volume_ceiling", $scope.Widgets.textVolumeToNotice.datavalue);
            else
            //$scope.Variables.PayTakeNotice.setInput("volume_ceiling", undefined);
                $scope.Variables.PayTakeNotice.setInput("volume_ceiling", 1000);
            $scope.Variables.PayTakeNotice.setInput("reference", $scope.Widgets.textReferenceNotice.datavalue);
            $scope.Variables.PayTakeNotice.setInput("preview", 0);
            $scope.Variables.PayTakeNotice.invoke({}, function() {
                $scope.Variables.SuccessTakeNotice.invoke();
                DialogService.close("PayTakeNoticeDialog");
            });
        };

    }
]);

Application.$controller("PayPublicationDialogController", ["$scope", "DialogService",
    function($scope, DialogService) {
        "use strict";
        $scope.ctrlScope = $scope;
        $scope.showSearch = false;
        $scope.certificatesFound = 0;

        $scope.formPayPublicationSubmit = function($event, $isolateScope, $formData) {
            $scope.Variables.PayPublication.setInput("reference", $scope.Widgets.textReferencePublication.datavalue);
            $scope.Variables.PayPublication.setInput("preview", 1);
            $scope.Variables.PayPublication.invoke({}, function(data) {
                if (data.length > 0) {
                    var currentData = data[0];
                    $scope.certificatesFound = currentData["count_records"];
                    $scope.Widgets.textCertificatesFoundPublication.datavalue = $scope.certificatesFound + " Certificates Found";
                    $scope.showSearch = true;
                }
            });
            // $scope.Widgets.textCertificatesFoundPublication.datavalue = "0 Certificates Found";
        };


        $scope.buttonPayPublicationClick = function($event, $isolateScope) {
            $scope.Variables.PayPublication.setInput("reference", $scope.Widgets.textReferencePublication.datavalue);
            $scope.Variables.PayPublication.setInput("preview", 0);
            $scope.Variables.PayPublication.invoke({}, function() {
                $scope.Variables.SuccessPayPublication.invoke();
                DialogService.close("PayPublicationDialog");
            });
        };

    }
]);