Application.$controller("PropertyInfoHeaderPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        if ($scope.activePageName === "InspectMobile") {
            $scope.Widgets.PropertyInfoLabel2.margin = "0px 0px";
            $scope.Widgets.PropertyInfoLabel3.margin = "0px 0px";
            $scope.Widgets.PropertyInfoLabel4.margin = "0px 0px";
        }

    };


    $scope.svPortalPropertyDataonSuccess = function(variable, data) {
        // PropertyInfoLabel1
        var tempStr = "";
        if (data.content[0].pin) {
            tempStr += data.content[0].pin + " ";
        }

        if (data.content[0].property_id) {
            tempStr += "<div class='text-muted'>(Property ID: " + data.content[0].property_id + ")</div>";
        }
        $scope.Widgets.PropertyInfoLabel1.caption = tempStr;

        // PropertyInfoLabel2
        tempStr = "";
        if (data.content[0].property_address) {
            tempStr += data.content[0].property_address + "<br/>";
        }

        if (data.content[0].township_name) {
            tempStr += data.content[0].township_name + " - ";
        }

        if (data.content[0].county_name) {
            tempStr += data.content[0].county_name + " ";
        }

        if (data.content[0].volume) {
            tempStr += "<span class='text-muted'>(" + data.content[0].volume + ")</span>";
        }
        $scope.Widgets.PropertyInfoLabel2.caption = tempStr;


        // PropertyInfoLabel3
        tempStr = "";
        if (data.content[0].assessee_name) {
            tempStr += data.content[0].assessee_name + "<br />";
        }

        if (data.content[0].assessee_address_line1) {
            tempStr += data.content[0].assessee_address_line1 + "<br />";
        }

        if (data.content[0].assessee_address_city) {
            tempStr += data.content[0].assessee_address_city + " ";
        }

        if (data.content[0].assessee_address_state) {
            tempStr += data.content[0].assessee_address_state + " ";
        }

        if (data.content[0].assessee_address_zip) {
            tempStr += data.content[0].assessee_address_zip;
        }
        $scope.Widgets.PropertyInfoLabel3.caption = tempStr;

        // PropertyInfoLabel4
        tempStr = "";
        if (data.content[0].property_class) {
            tempStr += data.content[0].property_class + "<br />";
        }

        if (data.content[0].property_exterior) {
            tempStr += data.content[0].property_exterior;
        }

        if (data.content[0].property_type) {
            if (data.content[0].property_exterior) {
                tempStr += " - ";
            }
            tempStr += data.content[0].property_type;
        }

        var isLotBldgInfoShowable = true;
        if ($scope.activePageName === "InspectMobile") {
            isLotBldgInfoShowable = false;
        }

        if (isLotBldgInfoShowable && (tempStr.length > 0) && (data.content[0].property_exterior || data.content[0].property_type) && data.content[0].lot_bldg) {
            tempStr += "<br />" + data.content[0].lot_bldg;
        } else if (isLotBldgInfoShowable && data.content[0].lot_bldg) {
            tempStr += data.content[0].lot_bldg;
        }

        $scope.Widgets.PropertyInfoLabel4.caption = tempStr;

        if ($scope.activePageName === "inspect") {
            $(window).resize();
        }
    };


    $scope.UpdatePropertyInfoonError = function(variable, data) {
        $scope.Variables.PropertyInfoErrorNotification.setMessage("Error while saving Property Info.  Please check the fields. <br>" + data);
        $scope.Variables.PropertyInfoErrorNotification.invoke();
    };


    $scope.PropertyNextRecordBtnClick = function($event, $isolateScope) {
        if ($scope.activePageName === "inspect" || $scope.activePageName === "InspectMobile") {
            $scope.$parent.Widgets.SearchContainer.Widgets.NextRecord.onClick();
        } else {
            $scope.$parent.Widgets.NextRecord.onClick();
        }
    };

    $scope.isNextRecordBtnDiabled = function() {
        if ($scope.activePageName === "inspect" || $scope.activePageName === "InspectMobile") {
            if ($scope.$parent.Widgets.SearchContainer && $scope.$parent.Widgets.SearchContainer.Widgets && $scope.$parent.Widgets.SearchContainer.Widgets.NextRecord) {
                return $scope.$parent.Widgets.SearchContainer.Widgets.NextRecord.disabled;
            } else {
                return true;
            }
        } else {
            if ($scope.$parent.Widgets.NextRecord) {
                return $scope.$parent.Widgets.NextRecord.disabled;
            } else {
                return true;
            }
        }
    };


    $scope.PropertyPreviousRecordBtnClick = function($event, $isolateScope) {
        if ($scope.activePageName === "inspect" || $scope.activePageName === "InspectMobile") {
            $scope.$parent.Widgets.SearchContainer.Widgets.PreviousRecord.onClick();
        } else {
            $scope.$parent.Widgets.PreviousRecord.onClick();
        }
    };

    $scope.isPreviousRecordBtnDiabled = function() {
        if ($scope.activePageName === "inspect" || $scope.activePageName === "InspectMobile") {
            if ($scope.$parent.Widgets.SearchContainer && $scope.$parent.Widgets.SearchContainer.Widgets && $scope.$parent.Widgets.SearchContainer.Widgets.PreviousRecord) {
                return $scope.$parent.Widgets.SearchContainer.Widgets.PreviousRecord.disabled;
            } else {
                return true;
            }
        } else {
            if ($scope.$parent.Widgets.PreviousRecord) {
                return $scope.$parent.Widgets.PreviousRecord.disabled;
            } else {
                return true;
            }
        }
    };


    $scope.svPropertyonSuccess = function(variable, data) {
        //$scope.svPortalPropertyDataonSuccess = function(variable, data)
    };

}]);


Application.$controller("PropertyInfoDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.PropertyInfoDialogOpened = function($event, $isolateScope) {
            //$scope.Widgets.PropertyInfoForm.resetForm();
        };

    }
]);

Application.$controller("PropertyInfoLiveFormController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);