Application.$controller("PhotosAndLinksPageController", ["$scope", "$window", "DialogService", function($scope, $window, DialogService) {
    "use strict";


    $scope.$root.$on("update-photo-count", function() {
        $scope.Variables.lvPhoto.listRecords();
    });

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {

        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'deletePhotoId
         */
        if ($scope.activePageName === "service" && $scope.Widgets.carousel1) {
            $scope.Widgets.carousel1.height = "410px"
        } else if ($scope.activePageName === "PropertyDetails") {
            $scope.Widgets.AssessorPicture.height = "350px"
        }
        if ($scope.activePageName === "service" && $scope.Widgets.PhotoHeader && $scope.Widgets.PhotoContainer) {
            $scope.Widgets.PhotoHeader.padding = "1em 0em 0em 0em";
            $scope.Widgets.PhotoContainer.padding = "2.5em 0em 0em 0em";
        }
    };


    $scope.svPortalPropertyDataonSuccess = function(variable, data) {
        $scope.$root.$emit("get-google-street-view-or-map");
        var photoUrl = $scope.Variables.svPortalPropertyData.firstRecord.assessor_photo || "";
        if (photoUrl.indexOf("http:") === 0) {
            $scope.Variables.svAccessPhoto.invoke();
        } else { /* if Photo Url is null OR if (photoUrl.indexOf("https:") === 0) */
            $scope.Widgets.AssessorPicture.picturesource = photoUrl;
        }


    };

    $scope.StreetViewBtnClick = function($event, $isolateScope) {
        $scope.Widgets.AssessorPicture.show = false;
        $scope.Widgets.MapContainer.show = false;
        $scope.Widgets.StreetViewContainer.show = true;
        //$scope.Widgets.MapContainer.content = "";
        if ($scope.Widgets.StreetViewContainer.content) {
            $scope.$root.$emit("update-google-street-view");
        } else {
            $scope.Widgets.StreetViewContainer.content = "StreetView";
        }
    };


    $scope.AssessorPictureBtnClick = function($event, $isolateScope) {
        $scope.Widgets.StreetViewContainer.show = false;
        $scope.Widgets.MapContainer.show = false;
        $scope.Widgets.AssessorPicture.show = true;
        //$scope.Widgets.StreetViewContainer.content = "";
        //$scope.Widgets.MapContainer.content = "";

        if ($scope.Variables.lvPhoto.dataSet.data.length > 0) {
            $scope.Widgets.PhotoCoroselDialog.open();
        }
    };

    $scope.MapBtnClick = function($event, $isolateScope) {
        $scope.Widgets.AssessorPicture.show = false;
        $scope.Widgets.StreetViewContainer.show = false;
        $scope.Widgets.MapContainer.show = true;
        //$scope.Widgets.StreetViewContainer.content = "";
        $scope.Widgets.MapContainer.content = "Map";
    };


    $scope.SelectLinkChange = function($event, $isolateScope, newVal, oldVal) {
        if (newVal.gis) {
            $window.open(newVal.gis, "_blank");
        }

    };

    $scope.AssessorPictureBtnmainClick = function($event, $isolateScope) {
        $scope.Widgets.StreetViewContainer.show = false;
        $scope.Widgets.MapContainer.show = false;
        $scope.Widgets.AssessorPicture.show = true;
        //$scope.Widgets.StreetViewContainer.content = "";
        //$scope.Widgets.MapContainer.content = "";
    };


    $scope.lvPhotoonSuccess = function(variable, data) {
        if (data.length == 0 && $scope.Widgets.PhotoCoroselDialog.close) {
            $scope.Widgets.PhotoCoroselDialog.close();
        } else if (data.length > 0) {
            $scope.$root.$broadcast("refreshAfterDelete");
        }
    };


    $scope.svAccessPhotoonSuccess = function(variable, data) {
        if (data != "") {
            $scope.Widgets.AssessorPicture.picturesource = "data:image/png;base64," + data;
            // $('[name="AssessorPicture"]')[0].src = "data:image/png;base64," + data;
        } else {
            $scope.Widgets.AssessorPicture.picturesource = "";
        }
    };

}]);

Application.$controller("PhotoCoroselDialogController", ["$scope", "$compile",
    function($scope, $compile) {
        "use strict";
        $scope.ctrlScope = $scope;
        var previousIndex = 0;

        $scope.PhotoCoroselDialogOpened = function($event, $isolateScope) {
            var str = "";
            for (var i = 0; i < $scope.Variables.lvPhoto.dataSet.data.length; i++) {
                str += '<img src="' + $scope.Variables.lvPhoto.dataSet.data[i].link + '"  class="thumbnail-pic" title= "' + $scope.Variables.lvPhoto.dataSet.data[i].createdTimestamp + '"  ng-click="changePhoto(' + i + ')"/>';
            }
            str = '<div class="thumbnail-bin">' + str + '</div>';
            $scope.Widgets.html1.content = $compile(str)($scope);

            if ($scope.Variables.lvPhoto.dataSet.data.length > 0) {
                $scope.changePhoto(0);
            }
        };

        $scope.scrollThumbnail = function(direction) {
            var obj = document.getElementsByClassName("thumbnail-bin")[0];
            if (direction === 1 && obj.scrollLeft < (obj.scrollWidth - 100)) {
                obj.scrollLeft = obj.scrollLeft + 100;
            } else if (direction === -1 && obj.scrollLeft > 100) {
                obj.scrollLeft = obj.scrollLeft - 100;
            }
        }

        $scope.changePhoto = function(index) {
            if (index === 0) {
                $scope.Widgets.PreviousPhotoBtn.disabled = true;
            } else {
                $scope.Widgets.PreviousPhotoBtn.disabled = false;
            }

            if (index === ($scope.Variables.lvPhoto.dataSet.data.length - 1)) {
                $scope.Widgets.NextPhotoBtn.disabled = true;
            } else {
                $scope.Widgets.NextPhotoBtn.disabled = false;
            }

            $($(".thumbnail-pic")[previousIndex]).removeClass("selected-thumbnail");
            $($(".thumbnail-pic")[index]).addClass("selected-thumbnail");
            previousIndex = index;
            $scope.Widgets.PhotoPicture.imagesource = $($(".thumbnail-pic")[index]).attr("src");

            $scope.Widgets.timeStamp.caption = new Date($scope.Variables.lvPhoto.dataSet.data[index].createdTimestamp).toISOString().slice(0, 22);

            $scope.Variables.deletePhotoId.dataSet.dataValue = $scope.Variables.lvPhoto.dataSet.data[index].id;
            $scope.Variables.deletePhotoUrl.dataSet.dataValue = $scope.Variables.lvPhoto.dataSet.data[index].link;
        }


        $scope.PreviousPhotoBtnClick = function($event, $isolateScope) {
            $scope.changePhoto(previousIndex - 1);
            $(".selected-thumbnail")[0].scrollIntoView();
        };


        $scope.NextPhotoBtnClick = function($event, $isolateScope) {
            $scope.changePhoto(previousIndex + 1);
            $(".selected-thumbnail")[0].scrollIntoView();
        };

        $scope.$on("refreshAfterDelete", function(event, data) {
            $scope.PhotoCoroselDialogOpened(event, data);
        })

    }
]);

Application.$controller("confirmdialog1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("deletePhotoFromAzureController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);