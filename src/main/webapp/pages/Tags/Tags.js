Application.$controller("TagsPageController", ["$scope", "$interval", "$timeout", "$rootScope", function($scope, $interval, $timeout, $rootScope) {
    "use strict";
    var isFetchingOfflineDataInProgress = false;

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        if ($scope.activePageName === "service") {
            $scope.Widgets.layoutgrid1.show = false;
            $scope.Widgets.buttonminus.show = false;
        }
        if ($scope.activePageName === "Main" || $scope.activePageName === "inspect" || $scope.activePageName === "InspectMobile") {
            $scope.Widgets.buttonplus.show = false;
        }

        if ($scope.activePageName === "inspect" || $scope.activePageName === "InspectMobile") {
            $scope.$watch(function() {
                return $scope.Variables.stOfflineCount.dataSet.dataValue;
            }, function(newVal, oldVal) {
                if (newVal === 1 && oldVal === 0) {
                    $rootScope.checkforOnlineToPushChanges();
                }
            });

            //Since AddNotePopover requires preloading for Offline feature - Container is loaded with the AddNote Partial page and removed.
            $scope.Widgets.AddNoteContainerHidden.content = "AddNote";
        }

    };

    $scope.qryPortalBuyFilteronSuccess = function(variable, data) {
        if (data.content.length > 0) {
            $scope.UnderwritingStatusChange(data.content[0].status);
        } else {
            $scope.UnderwritingStatusChange("");
        }
    };

    function updateTag(button_name) {
        $scope.Variables.svToggleTag.setInput('note_type', button_name);
        $scope.Variables.svToggleTag.update();
        $scope.Variables.svGetPortalTagsForPropertyID.update();
    }

    function deriveButtonNameFromTagsKey(tag_key) {
        return "btn" + tag_key.replace(' ', '').replace('/', '')
    }

    $scope.svGetPortalTagsForPropertyIDonSuccess = function(variable, data) {
        var contentJSON = data.content[0];
        delete contentJSON['property_id'];
        for (var key in contentJSON) {
            var buttonName = deriveButtonNameFromTagsKey(key);
            var value = contentJSON[key];
            var buttonSelector = "#" + buttonName;
            if (value == 0) {
                console.info("seleted ", value, $(buttonSelector));
                $(buttonSelector).removeClass("btn-tag-selected");
                $(buttonSelector).addClass("btn-tag-unselected");
            } else {
                console.info("unseleted ", value, $(buttonSelector));
                $(buttonSelector).removeClass("btn-tag-unselected");
                $(buttonSelector).addClass("btn-tag-selected");
            }
        }
    };

    /* Offline - Code - Starts Here */

    var inspectSearchResultPageChange = function() {
        var propertyList = [];
        if ($scope.$parent && $scope.$parent.$parent && $scope.$parent.$parent.$parent && $scope.$parent.$parent.$parent.$parent && $scope.$parent.$parent.$parent.$parent.Widgets && $scope.$parent.$parent.$parent.$parent.Widgets.SearchResultLiveList && $scope.$parent.$parent.$parent.$parent.Widgets.SearchResultLiveList.dataset && $scope.$parent.$parent.$parent.$parent.Widgets.SearchResultLiveList.dataset.length > 0) {
            for (var i = 0; i < $scope.$parent.$parent.$parent.$parent.Widgets.SearchResultLiveList.dataset.length; i++) {
                propertyList.push($scope.$parent.$parent.$parent.$parent.Widgets.SearchResultLiveList.dataset[i].property_id);
            }
            $scope.Variables.svGetPortalTagsForPropertyIDList.setInput("propertyid", propertyList);
            $scope.Variables.svGetPortalTagsForPropertyIDList.invoke();
        } else {
            //inspectSearchResultPageChange();
        }
    };

    $scope.$root.$on("inspect-search-result-page-change", function() {
        inspectSearchResultPageChange();
    });

    $scope.getTagData = function(propertyId, shouldClone) {
        if ($scope.Variables.svGetPortalTagsForPropertyIDList.dataSet && $scope.Variables.svGetPortalTagsForPropertyIDList.dataSet.content && $scope.Variables.svGetPortalTagsForPropertyIDList.dataSet.content.length > 0) {
            for (var i = 0; i < $scope.Variables.svGetPortalTagsForPropertyIDList.dataSet.content.length; i++) {
                if ($scope.Variables.svGetPortalTagsForPropertyIDList.dataSet.content[i].property_id == propertyId) {
                    var obj = $scope.Variables.svGetPortalTagsForPropertyIDList.dataSet.content[i];
                    if (shouldClone) {
                        obj = JSON.parse(JSON.stringify(obj));
                    }
                    return {
                        content: [obj]
                    };
                }
            }
        }
        return {
            content: [{}]
        };
    };

    $scope.svGetPortalTagsForPropertyIDonBeforeUpdate = function(variable, inputData) {
        var continueRequest = true;
        if ($scope.activePageName === "inspect" || $scope.activePageName === "InspectMobile") {
            // In Inspect page, it will always sync offline
            continueRequest = false;
            if (isFetchingOfflineDataInProgress) {
                $timeout(function() {
                    $scope.svGetPortalTagsForPropertyIDonBeforeUpdate(variable, inputData);
                }, 100);
            } else {
                var data = $scope.getTagData($scope.$parent.$parent.$parent.$parent.Widgets.SearchResultLiveList.selecteditem.property_id, true);
                $scope.Variables.svGetPortalTagsForPropertyID.dataSet = data;
                $scope.svGetPortalTagsForPropertyIDonSuccess(variable, data);
            }
        }
        return continueRequest;
    };

    $scope.svToggleTagonBeforeUpdate = function(variable, inputData) {

        var continueRequest = true;
        if ($scope.activePageName === "inspect" || $scope.activePageName === "InspectMobile") {
            // In Inspect page, it will always sync offline
            continueRequest = false;
            var data = $scope.getTagData($scope.Variables.svToggleTag.dataBinding.property_id, false);
            if (data.content[0][$scope.Variables.svToggleTag.dataBinding.note_type] == 0) {
                data.content[0][$scope.Variables.svToggleTag.dataBinding.note_type] = 1;
            } else {
                data.content[0][$scope.Variables.svToggleTag.dataBinding.note_type] = 0;
            }
            $rootScope.offlineToggleTag.push(inputData);
            $scope.Variables.stOfflineCount.dataSet.dataValue = $scope.Variables.stOfflineCount.dataSet.dataValue + 1;

            var data = JSON.parse(JSON.stringify(data));
            $scope.Variables.svGetPortalTagsForPropertyID.dataSet = data;
            $scope.svGetPortalTagsForPropertyIDonSuccess(variable, data);
        }
        return continueRequest;
    };

    $scope.svGetPortalTagsForPropertyIDListonBeforeUpdate = function(variable, inputData) {
        var continueRequest = false;
        if ($scope.activePageName === "inspect" || $scope.activePageName === "InspectMobile") {
            // Will fetch all the result's Tags only on Inspect page
            continueRequest = true;
            isFetchingOfflineDataInProgress = true;
        }
        return continueRequest;
    };


    $scope.svGetPortalTagsForPropertyIDListonResult = function(variable, data) {
        isFetchingOfflineDataInProgress = false;
    };

    /* Offline - Code - Ends Here*/

    $scope.TagChange = function(tagValue) {
        updateTag(tagValue);
    };


    $scope.svToggleTagonSuccess = function(variable, data) {
        $scope.$root.$emit('updated-notes');
    };


    $scope.buttonminusClick = function(buttonId) {
        if (buttonId == 'minus_btn') {
            $scope.Widgets.buttonminus.show = false;
            $scope.Widgets.buttonplus.show = true;
            $scope.Widgets.layoutgrid1.show = false;

        } else {
            $scope.Widgets.buttonminus.show = true;
            $scope.Widgets.buttonplus.show = false;
            $scope.Widgets.layoutgrid1.show = true;
        }

    };


    // $scope.svToggleTagonResult = function(variable, data) {
    //     $scope.$root.$emit('updated-notes');
    // };

}]);

Application.$controller("TakePhotoDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

    }
]);

Application.$controller("UploadPdfDialog1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);