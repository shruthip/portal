Application.$controller("TaskPageController", ["$scope", "DialogService", function($scope, DialogService) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */


    };



    $scope.search_addressSelect = function($event, $isolateScope, selectedValue) {
        $scope.Variables.TaskData.setFilter("propertyId", $isolateScope.datavalue);
        $scope.Variables.TaskData.update();
    };

    $scope.select_typeChange = function($event, $isolateScope, newVal, oldVal) {
        if (newVal === null) {
            var noteTypeIds = [];
            _.forEach($scope.Variables.NoteTypeSearch.dataSet.data, function(obj) {
                noteTypeIds.push(obj.id);
            });
            $scope.Variables.TaskData.setFilter("noteTypeId", noteTypeIds);
        } else {
            $scope.Variables.TaskData.setFilter("noteTypeId", $isolateScope.datavalue);
        }
        $scope.Variables.TaskData.setFilter("noteTypeId", id);
        $scope.Variables.TaskData.update();
    };

    $scope.NoteTypeDataonSuccess = function(variable, data) {
        var id = [];
        _.each(data.content, function(obj) {
            id.push(obj.id);
        });
        $scope.Variables.TaskData.setFilter("noteTypeId", id);
        $scope.Variables.TaskData.update();
    };

    $scope.select_userChange = function($event, $isolateScope, newVal, oldVal) {
        var id = [];
        $scope.Variables.NoteTypeData.update({}, function(data) {
            _.each(data, function(obj) {
                id.push(obj.id);
            });
        })
        $scope.Variables.TaskData.setFilter("noteTypeId", id);
        $scope.Variables.TaskData.setFilter("userId", $isolateScope.datavalue);
        $scope.Variables.TaskData.update();
    };

    $scope.AddTaskClick = function($event, $isolateScope) {
        $scope.Variables.GlobalTaskData.dataSet = {};
        DialogService.open('task_dialog', $scope, {
            'partialPage': "AddTaskPage"
        });

    };

}]);


Application.$controller("dialog_TaskController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;



    }
]);

Application.$controller("NoteLiveFormController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("NoteTableController", ["$scope", "DialogService", "wmSpinner",
    function($scope, DialogService, wmSpinner) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.$on("updateTaskData", function(event, data) {
            $scope.Variables.NoteTypeData.update();
        })

        $scope.updaterowAction = function($event, $rowData) {
            wmSpinner.show("", "spinnner-id_2", "", "");
            $scope.Variables.GlobalTaskData.dataSet = $rowData;
            $scope.Variables.DefaultProperty.setFilter("id", $rowData.property.asset.id);
            $scope.Variables.DefaultProperty.update({}, function(data) {
                DialogService.open('task_dialog', $scope, {
                    'partialPage': "TaskPage",
                });
                wmSpinner.hide("spinnner-id_2");
            })
        };

    }
]);