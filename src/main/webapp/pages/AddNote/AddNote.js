Application.$controller("AddNotePageController", ["$scope", "$rootScope", function($scope, $rootScope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        //Since AddNotePopover requires preloading for Offline feature - In Tags Partial page, a Container is loaded with the AddNote Partial page and removed.
        if ($scope.$root.activePageName === "inspect" || $scope.$root.activePageName === "InspectMobile") {
            if ($scope.$parent.Widgets.AddNoteContainerHidden) {
                $scope.$parent.Widgets.AddNoteContainerHidden.content = "";
            }
        }

        $scope.Widgets.Note.focus();
    };

    $scope.svAddNoteonSuccess = function(variable, data) {
        $scope.$root.$emit('updated-notes');
        closeAddNotePopover();
    };

    var closeAddNotePopover = function() {
        if ($scope.$parent.$parent.$parent.Widgets.popover1) {
            $scope.$parent.$parent.$parent.Widgets.popover1._popoverOptions.isOpen = false;
        } else {
            $scope.$parent.$parent.$parent.Widgets.popover2._popoverOptions.isOpen = false;
        }
    };

    $scope.svAddNoteonBeforeUpdate = function(variable, inputData) {
        var continueRequest = true;
        if ($scope.$root.activePageName === "inspect" || $scope.$root.activePageName === "InspectMobile") {
            Offline.check();
            if (Offline.state === "down") {
                $rootScope.offlineAddNote.push({
                    property_id: $scope.$parent.$parent.$parent.$parent.$parent.$parent.Widgets.SearchResultLiveList.selecteditem.property_id,
                    note: $scope.Widgets.Note.datavalue
                });
                $scope.Variables.stOfflineCount.dataSet.dataValue = $scope.Variables.stOfflineCount.dataSet.dataValue + 1;
                closeAddNotePopover();
                continueRequest = false;
            }
        }
        return continueRequest;
    };

}]);