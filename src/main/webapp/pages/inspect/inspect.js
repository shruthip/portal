Application.$controller("inspectPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {

        // Change height of UI components on Window Resize 
        $(window).resize(function() {
            $scope.Widgets.SearchContainer.Widgets.SearchResultLiveList.height = ($(window).height() - 150);
            $("#gMapsDiv").height($(window).height() - $("[name=PropertyInfoHeaderContainer]").height() - 100);
            $scope.Widgets.gridcolumn99.height = $(window).height() - $("[name=PropertyInfoHeaderContainer]").height() - 100;
        });

        $(window).resize();
    };
    $scope.anchor2Click = function($event, $isolateScope) {
        $scope.Widgets.anchor2.show = false;
        $scope.Widgets.anchor3.show = true;
        $('div[name="LeftGridColumn"]').show();
        $('div[name="MainGridColumn"]').css('width', '83.33%');
    };


    $scope.anchor3Click = function($event, $isolateScope) {
        $scope.Widgets.anchor2.show = true;
        $scope.Widgets.anchor3.show = false;
        $('div[name="LeftGridColumn"]').hide();
        $('div[name="MainGridColumn"]').css('width', '100%');
    };

}]);