Application.$controller("DisbursementsPageController", ["$scope", "DialogService", function($scope, DialogService) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */

    };


    $scope.container1Click = function($event, $isolateScope) {

        $scope.Variables.DisbursementData.clearData();
        $scope.Variables.DisbursementData.dataSet.mode = "Add";
        $scope.Variables.DisbursementData.dataSet.certificate_id = $scope.$parent.Widgets.SearchResultLiveList.selecteditem.certificate_id;
        DialogService.open("DisbursementAddEditDialog", $scope, {
            "selectedDisbursement": null
        });
    };


    $scope.container2Click = function($event, $isolateScope, item, currentItemWidgets) {

        $scope.Variables.DisbursementData.dataSet = item;
        $scope.Variables.DisbursementData.dataSet.mode = "Edit";
        // $scope.Variables.DisbursementData.dataSet.certificate_id = certificateID;
        DialogService.open("DisbursementAddEditDialog", $scope, {
            "selectedDisbursement": item
        });
    };


    $scope.svAddDisbursementonError = function(variable, data) {
        $scope.Variables.ErrorSQL.setMessage(data);
        $scope.Variables.ErrorSQL.invoke();
    };


    $scope.svUpdateDisbursementonError = function(variable, data) {
        $scope.Variables.ErrorSQL.setMessage(data);
        $scope.Variables.ErrorSQL.invoke();
    };

}]);


Application.$controller("DisbursementAddEditDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        // $scope.saveDisbursementBtnClick = function($event, $isolateScope) {

        //     //validate input values
        //     var type = $scope.Widgets.selectType.datavalue;
        //     var taxYear = $scope.Widgets.textTaxYear.datavalue;
        //     var installment = $scope.Widgets.selectInstallment.datavalue;
        //     var paymentDate = $scope.Widgets.date2.datavalue;
        //     var reference = $scope.Widgets.textReference.datavalue;
        //     var amount = $scope.Widgets.textAmount.datavalue;
        //     var category = null;

        //     var certificateID = $scope.Variables.DisbursementData.dataSet.certificate_id; //$scope.$parent.Widgets.SearchResultLiveList.selecteditem.certificate_id; //$scope.Widgets.livelist1.selecteditem.certificate_id
        //     var selectedDisbursementType = $.grep($scope.Variables.DisbursementTypes.dataSet.content, function(e) {
        //         return e.type == type;
        //     });

        //     category = selectedDisbursementType.length > 0 ? selectedDisbursementType[0].category : null;


        //     var isValid = true;
        //     var error = "Please enter valid ";
        //     if (type == undefined || type == null || type.length == 0) {
        //         isValid = false;
        //         error = error + "Type,";
        //     }
        //     if (taxYear == undefined || taxYear == null || taxYear == 0) {
        //         // isValid = false;
        //         // error = error + "Tax Year,";
        //     }

        //     if (paymentDate == undefined || paymentDate == null || paymentDate.length == 0) {
        //         isValid = false;
        //         error = error + "Payment Date,";
        //     }
        //     if (installment == undefined || installment == null) {
        //         // isValid = false;
        //         // error = error + "Installment,";
        //     }
        //     // installment = null;
        //     if (reference == undefined || reference == null || reference.length == 0) {
        //         // isValid = false;
        //         // error = error + "Reference,";
        //         // // reference = null;
        //     }
        //     if (amount == undefined || amount == null) {
        //         // isValid = false;
        //         // error = error + "Amount,";
        //     }
        //     // amount = 0;

        //     if (!isValid) {
        //         error = error.replace(/,\s*$/, "");
        //         $scope.Variables.ErrorValidation.setMessage(error);
        //         $scope.Variables.ErrorValidation.notify();
        //         // return;
        //     }

        //     //invoke add or update
        //     if ($scope.Variables.DisbursementData.dataSet.mode == "Add") {
        //         $scope.Variables.svAddDisbursement.setInput("selected_category", category);
        //         $scope.Variables.svAddDisbursement.invoke();
        //     } else if ($scope.Variables.DisbursementData.dataSet.mode == "Edit") {
        //         $scope.Variables.svUpdateDisbursement.setInput("selected_category", category);
        //         $scope.Variables.svUpdateDisbursement.invoke({}, function() {
        //             // $scope.Variables.svQryDisbursement.invoke({}, function() {
        //             //     alert("update");
        //             // });
        //         });
        //     }
        // };

        $scope.formDisbursementSubmit = function() {
            var selectedDisbursementType = $.grep($scope.Variables.DisbursementTypes.dataSet.content, function(e) {
                return e.type == $scope.Widgets.selectType.datavalue;
            });

            var category = selectedDisbursementType.length > 0 ? selectedDisbursementType[0].category : null;

            //invoke add or update
            if ($scope.Variables.DisbursementData.dataSet.mode == "Add") {
                $scope.Variables.svAddDisbursement.setInput("selected_category", category);
                $scope.Variables.svAddDisbursement.invoke();
            } else if ($scope.Variables.DisbursementData.dataSet.mode == "Edit") {
                $scope.Variables.svUpdateDisbursement.setInput("selected_category", category);
                $scope.Variables.svUpdateDisbursement.invoke({}, function() {
                    // $scope.Variables.svQryDisbursement.invoke({}, function() {
                    //     alert("update");
                    // });
                });
            }
        };


        $scope.DisbursementAddEditDialogOpened = function($event, $isolateScope) {
            $scope.Widgets.formDisbursement.resetForm();
            if ($scope.Variables.DisbursementData.dataSet.mode == "Edit" && $scope.Variables.DisbursementData.dataSet.tax_year_paid != undefined && $scope.Variables.DisbursementData.dataSet.tax_year_paid != null && !isNaN($scope.Variables.DisbursementData.dataSet.tax_year_paid))
                $scope.Widgets.textTaxYear.datavalue = parseInt($scope.Variables.DisbursementData.dataSet.tax_year_paid);
            if ($scope.Variables.DisbursementData.dataSet.mode == "Edit" && $scope.Variables.DisbursementData.dataSet.amount != undefined && $scope.Variables.DisbursementData.dataSet.amount != null)
                $scope.Widgets.textAmount.datavalue = $scope.Variables.DisbursementData.dataSet.amount;
        };

    }
]);





Application.$controller("DisbursementData.paymentsController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);