Application.$controller("QBAuthPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };


    $scope.isQBAuthenticatedonSuccess = function(variable, data) {
        if (data === false) {
            $scope.Variables.getQBAuthUrl.update({}, function(data) {
                window.location = data;
            });
        } else {
            $scope.Variables.getQBCompanyInfo.update({}, function(data) {
                console.log("CompanyInfo:");
                console.log(data);
            });
            $scope.Variables.getBalanceSheetReport.update({}, function(data) {
                console.log("BalanceSheet Report");
                console.log(data);
            });
        }
    };

}]);

Application.$controller("getCompanyInfoTable1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);