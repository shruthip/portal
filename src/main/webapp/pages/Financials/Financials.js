Application.$controller("FinancialsPageController", ["$scope", "$compile", function($scope, $compile) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */


        /*$scope.Widgets.financialpanel.$element.find('.panel-heading').each(function(index) {
            var $header = WM.element(this),
                $select = '<wm-select name="reoCustomer_select" width="250" class="pull-right" datafield="id" dataset="bind:Variables.REOCustomersData.dataSet" displayfield="displayName" on-change="reoCustomer_selectChange($event, $scope, newVal, oldVal)" placeholder="Select QBO Customer" datavalue="bind:$parent.$parent.Variables.AssetData.dataSet.data[0].externalAssetId"></wm-select>',
                $compiledTl = $compile($select)($scope);
            $header.find('.panel-actions').prepend($compiledTl);
        });*/
    };


    $scope.getReportsonSuccess = function(variable, data) {
        console.log("financial reports data:");
        console.log(data);
        $scope.prepareBalanceSheetData(data.balancesheet);
        $scope.prepareProfitAndLossData(data.profitandloss);
    };

    $scope.prepareProfitAndLossData = function(data) {
        try {
            var incomeSection = _.find(data.rows.row, {
                "group": "Income"
            });

            var reoNetGainSection;
            _.forEach(incomeSection.rows.row, function(o) {
                if (o.type === 'SECTION' && o.header !== null && o.header.colData[0].value === '4100 REO Net Gain on Sale') {
                    reoNetGainSection = o;
                    return;
                }
            });
            var reoNetGainArray = [];
            /*reoNetGainArray.push({
                "key": "Income"
            });*/
            reoNetGainArray = _.concat(reoNetGainArray, $scope.prepareDataArray(reoNetGainSection));
            reoNetGainArray.push({
                "key": "Total Income",
                "value": reoNetGainSection.summary.colData[1].value
            });
            $scope.Variables.ProfitAndLossData.dataSet = reoNetGainArray;
        } catch (err) {
            console.log("ERROR", err);
            $scope.Variables.ProfitAndLossData.dataSet = []
        }
    }

    $scope.prepareBalanceSheetData = function(data) {
        try {
            var totalAssets = _.find(data.rows.row, {
                "group": "TotalAssets"
            });
            var currentAssets = _.find(totalAssets.rows.row, {
                "group": "CurrentAssets"
            });
            var otherCurrentAssets = _.find(currentAssets.rows.row, {
                "group": "OtherCurrentAssets"
            });
            var otherCurrentAssetsSummary = otherCurrentAssets.summary;
            var otherCurrentAssetsData = otherCurrentAssets.rows.row;

            var reoAssetsSection;
            _.forEach(otherCurrentAssetsData, function(o) {
                if (o.type === 'SECTION' && o.header !== null && o.header.colData[0].value === '1300 REO') {
                    reoAssetsSection = o;
                    return;
                }
            });

            var balanceSheetDataArray = [];
            /*balanceSheetDataArray.push({
                "key": "Balance Sheet"
            });
            balanceSheetDataArray.push({
                "key": "Assets"
            });*/
            balanceSheetDataArray = _.concat(balanceSheetDataArray, $scope.prepareDataArray(reoAssetsSection));
            balanceSheetDataArray.push({
                "key": "Total Assets",
                "value": reoAssetsSection.summary.colData[1].value
            });
            $scope.Variables.BalanceSheetData.dataSet = balanceSheetDataArray;
        } catch (err) {
            console.log("ERROR", err);
            $scope.Variables.BalanceSheetData.dataSet = [];
        }
    }

    $scope.prepareDataArray = function(data) {
        var dataArray = [];
        _.forEach(data.rows.row, function(o) {
            var keyData = o.colData[0];
            var valueData = o.colData[1];
            var obj = {};
            obj.id = keyData.id;
            //obj.key = keyData.value;
            obj.key = keyData.value.substring(keyData.value.indexOf(" ") + 1);
            obj.value = valueData.value;
            dataArray.push(obj);
        });
        return dataArray;
    }

    $scope.getREOCustomersonSuccess = function(variable, data) {
        $scope.Variables.REOCustomersData.dataSet = data.entities;
    };

    $scope.reoCustomer_selectChange = function($event, $isolateScope, newVal, oldVal) {
        $scope.Variables.getReports.setInput({
            "qbCustomerId": newVal
        });
        $scope.Variables.getReports.update();
        $scope.Variables.UpdateExternalAssetId.setInput({
            'asset_id': parseInt($scope.$parent.$parent.pageParams.AssetID),
            'external_asset_id': newVal !== null ? parseInt(newVal) : 0
        });
        $scope.Variables.UpdateExternalAssetId.update();
    };

    $scope.UpdateExternalAssetIdonSuccess = function(variable, data) {
        $scope.Variables.qbCustomerNotification.notify();
    };

}]);

Application.$controller("BalanceSheetDataTable1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("table2Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);