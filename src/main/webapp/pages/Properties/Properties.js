Application.$controller("PropertiesPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };






    $scope.GetListOfAssetsByBatchonSuccess = function(variable, data) {
        /*var arr = [];
        _.forEach(data.content, function(obj) {
            arr.push(obj.id);
        });
        if (arr.length == 0) {
            $scope.Variables.PlatformAssetData.setFilter('id', null);
            $scope.Variables.PlatformAssetData.update();
        } else {
            $scope.Variables.PlatformAssetData.setFilter('id', arr);
            $scope.Variables.PlatformAssetData.update();
        }*/
    };


    $scope.GetAssetByCourtActiononSuccess = function(variable, data) {
        /*var arr = [];
        _.forEach(data.content, function(obj) {
            arr.push(obj.id);
        });
        if (arr.length == 0) {
            $scope.Variables.PlatformAssetData.setFilter('id', null);
            $scope.Variables.PlatformAssetData.update();
        } else {
            $scope.Variables.PlatformAssetData.setFilter('id', arr);
            $scope.Variables.PlatformAssetData.update();
        }*/
    };


    $scope.GetPropertyIdByStateonSuccess = function(variable, data) {
        var arr = [];
        _.forEach(data.content, function(obj) {
            arr.push(obj.id);
        });
        if (arr.length === 0) {
            $scope.Variables.PlatformAssetData.setFilter('id', null);
            $scope.Variables.PlatformAssetData.update();
        } else {
            $scope.Variables.PlatformAssetData.setFilter('id', arr);
            $scope.Variables.PlatformAssetData.update();
        }
    };

    $scope.select_stateChange = function($event, $isolateScope, newVal, oldVal) {
        if (newVal === null) {
            $scope.Variables.PlatformAssetData.setFilter('id', null);
            $scope.Variables.PlatformAssetData.update();
        } else {
            $scope.Variables.GetPropertyIdByState.update();
        }
    };

    $scope.select_BatchChange = function($event, $isolateScope, newVal, oldVal) {
        $scope.Variables.PlatformAssetData.setFilter('batch', newVal);
        $scope.Variables.PlatformAssetData.update();
    };

    $scope.select_statusChange = function($event, $isolateScope, newVal, oldVal) {
        $scope.Variables.PlatformAssetData.setFilter('statusId', newVal);
        $scope.Variables.PlatformAssetData.update();
    };

    $scope.select_actionChange = function($event, $isolateScope, newVal, oldVal) {
        $scope.Variables.PlatformAssetData.setFilter('nextCourtActionId', $isolateScope.datavalue);
        $scope.Variables.PlatformAssetData.update();
    };


    $scope.search_addressSelect = function($event, $isolateScope, selectedValue) {
        $scope.Variables.PlatformAssetData.setFilter('id', $isolateScope.datavalue);
        $scope.Variables.PlatformAssetData.update();
    };

}]);




Application.$controller("dialog_assetController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("AssetLiveFormController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("AssetDataTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.updaterowAction = function($event, $rowData) {
            $scope.Variables.goToPage_PropertyDetails.setData({
                "PropertyId": $rowData.propertyId,
                "AssetId": $rowData.id,
            });
            $scope.Variables.goToPage_PropertyDetails.invoke();
        };

    }
]);