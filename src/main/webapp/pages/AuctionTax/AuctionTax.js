Application.$controller("AuctionTaxPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        if ($scope.activePageName === "Main") {
            $scope.Widgets.Taxes_header.show = "false";
        } else if ($scope.activePageName === "inspect") {
            $scope.Widgets.Taxes_header.show = "true";
        }

    };


    $scope.svUpdateAvailableSubonError = function(variable, data) {
        $scope.Variables.ErrorUpdateAvailableSub.setMessage(data);
        $scope.Variables.ErrorUpdateAvailableSub.invoke();
    };

}]);

Application.$controller("UpdateSubDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.UpdateSubDialogOpened = function($event, $isolateScope) {
            $scope.Widgets.AuctionTaxSub.resetForm();
        };

    }
]);