Application.$controller("StreetViewPageController", ["$scope", function($scope) {
    "use strict";

    var panorama;
    var geocoder;
    var directionsService;
    var sv;
    var myLatLng;

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        // Constants & Variables
        var GOOGLE_MAPS_API_KEY;
        GOOGLE_MAPS_API_KEY = $scope.Variables.svReadPropertyGoogleMapsApiKey.dataSet.value;
        /* if (propertyObj.result.status === "success") {
             GOOGLE_MAPS_API_KEY = propertyObj.result.content["google.maps.api.key"];
         } else {
             //"Error while getting Key.  Retry. - propertyObj.result.message;
             return;
         }*/

        // Load Google Stree View script
        function loadScripts() {
            // Check whether Google Map API is already loaded or not
            if (typeof google === 'object' && typeof google.maps === 'object') {
                initialize();
            } else {
                var googleStreetViewJS = "https://maps.googleapis.com/maps/api/js?key=" + GOOGLE_MAPS_API_KEY;
                $.getScript(googleStreetViewJS, function() {
                    initialize();
                });
            }
        }

        function initialize() {
            sv = new google.maps.StreetViewService();
            geocoder = new google.maps.Geocoder();
            directionsService = new google.maps.DirectionsService();
            panorama = new google.maps.StreetViewPanorama(document.getElementById("street-view"), {
                imageDateControl: true
            });
            getGoogleStreetView();
        }

        loadScripts();
    };

    function geocodeAddress(address) {
        geocoder.geocode({
            'address': address
        }, function(results, status) {
            if (status === 'OK') {
                // Street View - Heading Code - Using DirectionsService - Starts Here
                myLatLng = results[0].geometry.location;
                // find a Streetview location on the road
                var request = {
                    origin: address,
                    destination: address,
                    travelMode: google.maps.DirectionsTravelMode.DRIVING
                };
                directionsService.route(request, directionsCallback);
                // Street View - Heading Code - Using DirectionsService - Ends Here
            } else {
                $scope.Variables.ncGeocodeFailure.setMessage('Geocode was not successful for the following reason: ' + status);
                $scope.Variables.ncGeocodeFailure.invoke();
            }
        });
    }

    // Street View - Heading Code - Using DirectionsService - Starts Here
    function processSVData(data, status) {
        if (status == google.maps.StreetViewStatus.OK) {
            panorama.setPano(data.location.pano);

            var heading = google.maps.geometry.spherical.computeHeading(data.location.latLng, myLatLng);
            panorama.setPov({
                heading: heading,
                pitch: 0,
                zoom: 1
            });
            panorama.setVisible(true);

        } else {
            $scope.Variables.ncGeocodeFailure.setMessage("Street View data not found for this location.");
            $scope.Variables.ncGeocodeFailure.invoke();
        }
    }

    function directionsCallback(response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                var latlng = response.routes[0].legs[0].start_location;
                sv.getPanoramaByLocation(latlng, 50, processSVData);
            } else {
                $scope.Variables.ncGeocodeFailure.setMessage("Directions service not successfull for the following reason:" + status);
                $scope.Variables.ncGeocodeFailure.invoke();
            }
        }
        // Street View - Heading Code - Using DirectionsService - Ends Here

    function getGoogleStreetView() {
        // Update the Google Street View only when the container is visible
        if ($scope.$parent.Widgets.StreetViewContainer.show) {
            var address = "Eiffel Tower, Paris";
            if ($scope.$parent.Variables.svPortalPropertyData && $scope.$parent.Variables.svPortalPropertyData.dataSet.content && $scope.$parent.Variables.svPortalPropertyData.dataSet.content[0].property_address) {
                address = $scope.$parent.Variables.svPortalPropertyData.dataSet.content[0].property_address;
            }
            geocodeAddress(address);
        }
    }

    $scope.$root.$on("get-google-street-view-or-map", getGoogleStreetView);

    var previousPropertyId = "";
    $scope.$root.$on("update-google-street-view", function() {
        if ($scope.$parent.Variables.svPortalPropertyData && $scope.$parent.Variables.svPortalPropertyData.dataSet.content && $scope.$parent.Variables.svPortalPropertyData.dataSet.content[0].property_id) {
            var currentPropertyId = $scope.$parent.Variables.svPortalPropertyData.dataSet.content[0].property_id;
            if (previousPropertyId != currentPropertyId) {
                previousPropertyId = currentPropertyId;
                getGoogleStreetView();
            }
        }
    });

    // Workaround for Google Stree View not getting loaded when hidden
    $scope.$root.$on("fix-google-street-view", function() {
        google.maps.event.trigger(panorama, 'resize');
    });

}]);