Application.$controller("GeneratePdfPageController", ["$scope", "$q", "$timeout", function($scope, $q, $timeout) {
    "use strict";

    var isBackgroundPdfGenerationEnabled = false;
    var pdfFolderName = "";
    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get vaisue of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        $scope.Widgets.buttonminus.show = false;
        $scope.Widgets.resultgridpdf.show = false;
        if ($scope.activePageName === "service") {
            isBackgroundPdfGenerationEnabled = true;
            var templateSet = '';

            if ($scope.$parent.partialcontainername == 'legalGeneratePdfContainer') {
                templateSet = $scope.$parent.Variables.svCloseMatterTemplate.dataSet.content[0];
            } else {
                templateSet = $scope.$parent.Widgets.Template.datavalue;
            }

            $scope.Widgets.Template.datavalue = templateSet;
            $scope.Widgets.certificateId.datavalue = $scope.$parent.$parent.$parent.Widgets.SearchResultLiveList.selecteditem.certificate_id;
            $scope.Variables.PlatformExecuteProcPdfFilter.setInput("template_id", templateSet.id);
            $scope.Variables.PlatformExecuteProcPdfFilter.setInput("certificate_id", $scope.$parent.$parent.$parent.Widgets.SearchResultLiveList.selecteditem.certificate_id);

            $scope.Variables.PlatformExecuteProcPdfFilter.invoke();
        }
        pdfFolderName = $scope.Variables.svReadPropertyPdf.dataSet.value;
    };

    var batch = false;
    var batchCode = "";
    var batchFiles = [];

    var totalRecords = 0;
    var currentRecords = 0;
    var entityTable = {
        "&frasl;": "&#8260;",
        "&lsaquo;": "&#8249;",
        "&rsaquo;": "&#8250;",
        "&Prime;": "&#8243;",
        "&prime;": "&#8242;",
        "&bdquo;": "&#8222;",
        "&rdquo;": "&#8221;",
        "&ldquo;": "&#8220;",
        "&sbquo;": "&#8218;",
        "&rsquo;": "&#8217;",
        "&lsquo;": "&#8216;",
        "&mdash;": "&#8212;",
        "&ndash;": "&#8211;",
        "&nbsp;": "&#160;",
        "&shy;": "&#173;",
        "&amp;": "&#38;",
        "&quot;": "&#34;"
    };

    function batchPushToAzure(data) {
        $scope.Variables.svPushPdfToAzure.setInput("sourceFile", data);
        $scope.Variables.svPushPdfToAzure.setInput("azureFolder", pdfFolderName);
        $scope.Variables.svPushPdfToAzure.invoke({}, function(data) {
            if (batch) {
                $scope.Widgets.resultPdf.show = false;
                $scope.Widgets.resultPdfLabel.show = true;
            } else {
                $scope.Widgets.resultPdf.show = true;
                $scope.Widgets.resultPdfLabel.show = false;
            }

            var templateId = batchFiles[0].templateId;
            var certificate_id = ',';

            batchFiles.forEach(function(obj) {

                certificate_id += ',' + obj.certificateId;
                displayPdf({
                    url: data,
                    certificateId: obj.certificateId
                });
            });
            certificate_id += ',';

            $scope.Variables.svInsertPdf.setInput("tempate_id", templateId);
            $scope.Variables.svInsertPdf.setInput("url", data);
            $scope.Variables.svInsertPdf.setInput("certificate_ids", certificate_id);
            $scope.Variables.svInsertPdf.invoke();

            $scope.Variables.notifyPdfSuccess.invoke();
            $scope.Widgets.spinner1.show = false;

        });
    }

    function replaceJsonValue(parsedHtml, id, jsonRecord) {
        parsedHtml = $(parsedHtml);

        jsonRecord = JSON.parse(jsonRecord);
        /*        jsonRecord = {
                    "employees": [{
                            'empId': 1,
                            'lastname': 'Lin',
                            'firstname': 'Eric',
                            'jobTitle': 'Product Manager'
                        }
                    ]
                };
        */
        var elementToClone = parsedHtml.find("#" + id);

        for (var key in jsonRecord) {
            if (jsonRecord.hasOwnProperty(key)) {
                var listObject = jsonRecord[key];

                listObject.forEach(function(data) {

                    var newObj = elementToClone.clone().removeAttr('id')[0].outerHTML;

                    for (var column in data) {
                        newObj = newObj.replace(new RegExp('##' + column + '##', "ig"), data[column]);
                    }

                    $(newObj).insertBefore(elementToClone);
                });
            }
        }
        elementToClone.remove();

        var htmlContent = "";

        for (var element in parsedHtml) {
            if (parsedHtml.hasOwnProperty(element) && element != 'length' && parsedHtml[element].outerHTML != undefined) {
                htmlContent += parsedHtml[element].outerHTML;
                console.log(parsedHtml[element].outerHTML + "\n");
            }
        }

        return htmlContent;
    }


    $scope.GeneratePdfBtnClick = function($event, $isolateScope) {
        $scope.Variables.resultPdf.dataSet = [];
        $scope.Widgets.resultPdf.show = false;
        $scope.Widgets.spinner1.show = true;
        $scope.Widgets.resultPdfLabel.show = false;
        totalRecords = 0;
        currentRecords = 0;
        batchCode = "";
        batchFiles = [];
        var template = $scope.Widgets.Template.datavalue;
        var selectedRecord = $scope.Widgets.TemplateResultGrid.selecteditem;
        if (selectedRecord.constructor.name === "Object") {
            selectedRecord = [selectedRecord];
        }

        if (JSON.parse($scope.Widgets.chkSelectAll.datavalue)) {
            selectedRecord = $scope.Variables.PlatformExecuteProcPdfFilter.dataSet.content;
        }

        totalRecords = selectedRecord.length;

        var html_template = template.template;
        batch = JSON.parse($scope.Widgets.chkBatch.datavalue);
        if (html_template) {

            var temp = [];
            var html_batch_template = "";
            selectedRecord.forEach(function(record) {
                html_template = template.template;

                var parsedHtml = $(html_template);

                var listAllIds = [];

                parsedHtml.find('[id^="content_"]').each(function() {
                    listAllIds.push(this.id);
                });

                $.each(record, function(key, element) {
                    var keyword = "##" + key + "##";
                    if (listAllIds.includes(key)) {
                        html_template = replaceJsonValue(html_template, key, element);
                    } else {
                        html_template = html_template.replace(new RegExp(keyword, "ig"), element);
                    }

                });

                //html_template = html_template.replace(new RegExp("&nbsp;", "ig"), ' ');
                html_template = html_template.replace(/\<br\>/g, "<br/>");
                //html_template = html_template.replace(new RegExp("&rsquo;", "ig"), ' ');
                $.each(entityTable, function(key, element) {
                    html_template = html_template.replace(new RegExp(key, "ig"), element);
                });

                if (batch) {
                    if (!html_batch_template) {
                        html_batch_template = html_template;
                    } else {
                        html_batch_template += "<div class='page-break'></div>" + html_template;
                    }

                    batchFiles.push({
                        'templateId': template.id,
                        'certificateId': record.certificate_id
                    });
                } else {
                    var CssStyle = $scope.Widgets.Css.datavalue;

                    $scope.Variables.svGeneratePdf.setInput("template_id", template.id);
                    $scope.Variables.svGeneratePdf.setInput("fileName", record.certificate_id);
                    $scope.Variables.svGeneratePdf.setInput("JsonNode", {
                        "value": html_template.replace(null, ''),
                        "cssStyle": CssStyle
                    });
                    $scope.Variables.svGeneratePdf.setInput("batch", batch);
                    $scope.Variables.svGeneratePdf.setInput("azureFolder", pdfFolderName);
                    $scope.Variables.svGeneratePdf.invoke();
                }
            });

            if (batch) {
                var CssStyle = $scope.Widgets.Css.datavalue;

                $scope.Variables.svGeneratePdf.setInput("template_id", template.id);
                $scope.Variables.svGeneratePdf.setInput("fileName", new Date().getTime());
                $scope.Variables.svGeneratePdf.setInput("JsonNode", {
                    "value": html_batch_template.replace(null, ''),
                    "cssStyle": CssStyle
                });
                $scope.Variables.svGeneratePdf.setInput("batch", true);
                $scope.Variables.svGeneratePdf.setInput("azureFolder", pdfFolderName);
                $scope.Variables.svGeneratePdf.invoke();

            }

        } else {
            $scope.Variables.noTemplateAvailable.invoke();
        }

    };

    function displayPdf(objPdf) {
        objPdf.escapedUrl = location.pathname + "services/pdfJava/DownloadFromAzureStorage?url=" + escape(objPdf.url);
        $scope.Variables.resultPdf.dataSet.push(objPdf);
    }


    function insertAzureUrl(templateId, certificateId, createdTimestamp, link) {
        $scope.Variables.lvPDF.createRecord({
            row: {
                'templateId': templateId,
                'certificateId': certificateId,
                "createdTimestamp": createdTimestamp,
                "link": link
            }
        });
    }

    $scope.svGeneratePdfonSuccess = function(variable, data) {
        currentRecords++;

        if (!batch) {
            $scope.Widgets.resultPdf.show = true;
            $scope.Widgets.spinner1.show = false;
            $scope.Widgets.resultPdfLabel.show = false;
            insertAzureUrl(parseInt(data[0]), parseInt(data[1]), new Date().getTime(), data[2]);

            displayPdf({
                url: data[2],
                certificateId: data[1]
            });
            $scope.Variables.notifyPdfSuccess.invoke();
        } else {
            batchPushToAzure(data[2]);
        }
    };


    $scope.svMergePdfonSuccess = function(variable, data) {
        $scope.Variables.svPushPdfToAzure.setInput("sourceFile", data);
        $scope.Variables.svPushPdfToAzure.setInput("azureFolder", pdfFolderName);
        $scope.Variables.svPushPdfToAzure.invoke({}, function(data) {
            if (batch) {
                $scope.Widgets.resultPdf.show = false;
                $scope.Widgets.spinner1.show = true;
                $scope.Widgets.resultPdfLabel.show = true;
            } else {
                $scope.Widgets.resultPdf.show = true;
                $scope.Widgets.spinner1.show = false;
                $scope.Widgets.resultPdfLabel.show = false;
            }

            var templateId = batchFiles[0].templateId;
            var certificate_id = ',';
            batchFiles.forEach(function(obj) {

                certificate_id += ',' + obj.certificateId;
                displayPdf({
                    url: data,
                    certificateId: obj.certificateId
                });
            });
            certificate_id += ',';

            $scope.Variables.svInsertPdf.setInput("tempate_id", templateId);
            $scope.Variables.svInsertPdf.setInput("url", data);
            $scope.Variables.svInsertPdf.setInput("certificate_ids", certificate_id);
            $scope.Variables.svInsertPdf.invoke();

            $scope.Variables.notifyPdfSuccess.invoke();
        });

    };


    $scope.svGeneratePdfonError = function(variable, data) {
        currentRecords++;
    };

    $scope.svMergePdfonError = function(variable, data) {
        console.log(data);
    };


    $scope.SearchBtnClick = function($event, $isolateScope) {
        $scope.Variables.resultPdf.dataSet = [];
        $scope.Widgets.resultgridpdf.show = false;
        $scope.Widgets.resultPdf.show = false;
        $scope.Widgets.TemplateResultGrid.show = true;
        $scope.Widgets.buttonminus.show = false;
        $scope.Widgets.buttonplus.show = true;

    };


    $scope.PlatformExecuteProcPdfFilteronSuccess = function(variable, data) {
        $timeout(function() {
            if (isBackgroundPdfGenerationEnabled && $scope.Variables.PlatformExecuteProcPdfFilter.dataSet.content && $scope.Variables.PlatformExecuteProcPdfFilter.dataSet.content.length) {
                $scope.Widgets.TemplateResultGrid.selecteditem = $scope.Variables.PlatformExecuteProcPdfFilter.dataSet.content;
                $scope.GeneratePdfBtnClick({}, {});
            } else if (isBackgroundPdfGenerationEnabled) {
                $timeout(function() {
                    $scope.$parent.Variables.SuccessUpdateCloseMatter.invoke();
                    $scope.$parent.Variables.svQryLegalDetails.invoke();
                    $scope.$root.Widgets.spCloseCase.show = false;
                    $scope.$parent.Widgets.confirmdialog1.close();
                    $scope.$root.$broadcast("myEvent");

                });
            }
        }, 500);
    };


    $scope.buttonminusClick = function(buttonId) {
        if (buttonId == 'minus_btn_pdf') {
            $scope.Widgets.buttonminus.show = false;
            $scope.Widgets.buttonplus.show = true;
            $scope.Widgets.resultgridpdf.show = false;
        } else {
            $scope.Widgets.buttonminus.show = true;
            $scope.Widgets.buttonplus.show = false;
            $scope.Widgets.resultgridpdf.show = true;
        }
    };


    $scope.svInsertPdfonSuccess = function(variable, data) {
        if (isBackgroundPdfGenerationEnabled) {
            if ($scope.$parent.partialcontainername == 'legalGeneratePdfContainer') {
                $timeout(function() {
                    $scope.$parent.Variables.SuccessUpdateCloseMatter.invoke();
                    $scope.$parent.Variables.svQryLegalDetails.invoke();
                    $scope.$root.Widgets.spCloseCase.show = false;
                    $scope.$parent.Widgets.confirmdialog1.close();
                    $scope.$root.$broadcast("myEvent");
                });
            } else {
                $timeout(function() {
                    $scope.$root.$broadcast("myEvent");
                });
            }
        }
    };

}]);




Application.$controller("resultPdfController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("TemplateResultGridController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);