Application.$controller("CommonPageController", ['$scope',
    function($scope) {
        "use strict";

        /* perform any action on the variables within this block(on-page-load) */
        $scope.onPageVariablesReady = function() {
            /*
             * variables can be accessed through '$scope.Variables' property here
             * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
             * $scope.Variables.loggedInUser.getData()
             */
        };
        /* perform any action on widgets within this block */
        $scope.onPageReady = function() {
            /*
             * widgets can be accessed through '$scope.Widgets' property here
             * e.g. to get value of text widget named 'username' use following script
             * '$scope.Widgets.username.datavalue'
             */
        };
    }
]);

Application.$controller("CommonLoginDialogController", ["$scope", "DialogService", "$window",
    function($scope, DialogService, $window) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.CommonLoginDialogError = function($event, $isolateScope) {
            /*
             * Error message can be accessed from the property $isolateScope.loginMessage.caption
             */
        };

        $scope.CommonLoginDialogSuccess = function($event, $isolateScope) {
            /*
             * This success handler provides a redirectUrl which is the role based url set while configuring Security service for the project.
             * The redirectUrl can be accessed as $isolateScope.redirectUrl
             * To navigate to the url use '$window' service as:
             * $window.location = $isolateScope.redirectUrl
             */
            DialogService.hideDialog("CommonLoginDialog");
        };
    }
]);

Application.$controller("task_dialogController", ["$scope", "DialogService",
    function($scope, DialogService) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.NoteLiveFormsBeforeservicecall = function($event, $operation, $data) {
            $data.property = $data.property.property;
            $data.user = $data.userByUserId.email;
            if ($data.TaskCompleted) {
                $data.doneTimestamp = new moment().format('YYYY-MM-DD HH:mm:ss');
            } else {
                $data.doneTimestamp = null;
            }
            if ($data.dueTimestamp) {
                $data.dueTimestamp = new moment($data.dueTimestamp).utc().format('YYYY-MM-DD HH:mm:ss');
            }
        };

        $scope.NoteLiveFormsSuccess = function($event, $operation, $data) {
            if ($scope.$parent.activePageName === 'Task') {
                $scope.$root.$emit("updateTaskData");
                DialogService.hideDialog("task_dialog");
                //$scope.$parent.Widgets.NoteTable.refreshData();
            }
            if ($scope.$parent.activePageName === 'PropertyDetails') {
                $scope.$root.$emit("updateTaskData");
                DialogService.hideDialog("task_dialog");
                //$scope.$parent.Widgets.NoteTable.refreshData();
            }
            DialogService.hideDialog("task_dialog");
        };


        $scope.task_dialogOpened = function($event, $isolateScope) {
            $scope.Variables.TaskPartial.dataSet.partialPage = $scope.partialPage;
        };

    }
]);

Application.$controller("NoteLiveFormsController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);