Application.$controller("PaymentsPageController", ["$scope", "DialogService", function($scope, DialogService) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };


    $scope.containerEditPaymentClick = function($event, $isolateScope, item, currentItemWidgets) {
        $scope.Variables.PaymentData.dataSet = item;
        $scope.Variables.PaymentData.dataSet.mode = "Edit";
        DialogService.open("dialogAddEditPayment", $scope, {
            "selectedPayment": item
        });
    };


    $scope.addPaymentClick = function($event, $isolateScope) {
        $scope.Variables.PaymentData.clearData();
        $scope.Variables.PaymentData.dataSet.mode = "Add";
        $scope.Variables.PaymentData.dataSet.certificate_id = $scope.$parent.Widgets.SearchResultLiveList.selecteditem.certificate_id;
        DialogService.open("dialogAddEditPayment", $scope, {
            "selectedPayment": null
        });
    };


    $scope.svAddPaymentonError = function(variable, data) {
        $scope.Variables.ErrorSQL.setMessage(data);
        $scope.Variables.ErrorSQL.invoke();
    };


    $scope.svUpdatePaymentonError = function(variable, data) {
        $scope.Variables.ErrorSQL.setMessage(data);
        $scope.Variables.ErrorSQL.invoke();
    };

}]);

Application.$controller("dialogAddEditPaymentController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.dialogAddEditPaymentOpened = function($event, $isolateScope) {
            $scope.Widgets.formPayment.resetForm();
            if ($scope.Variables.PaymentData.dataSet.mode == "Edit") {
                if ($scope.Variables.PaymentData.dataSet.amount != undefined && $scope.Variables.PaymentData.dataSet.amount != null)
                    $scope.Widgets.textAmount.datavalue = $scope.Variables.PaymentData.dataSet.amount;
            }
            //console.log($scope.Variables.PaymentData.dataSet);
        };

        $scope.formPaymentSubmit = function($event, $isolateScope, $formData) {

            //invoke add or update
            if ($scope.Variables.PaymentData.dataSet.mode == "Add") {
                $scope.Variables.svAddPayment.invoke();
            } else if ($scope.Variables.PaymentData.dataSet.mode == "Edit") {
                $scope.Variables.svUpdatePayment.invoke();
            }
        };

    }
]);