Application.$controller("ToolsPopoverPageController", ["$scope", "DialogService", function($scope, DialogService) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

    $scope.NameClick = function($event, $isolateScope, item, currentItemWidgets) {
        if (item.dataValue == "RunUnderwriting") {
            $scope.$parent.$parent.$parent.Variables.RunUnderwriting.invoke();
        } else if (item.dataValue == "CompleteSale") {
            DialogService.open("CompleteSaleDialog", $scope.$parent.$parent.$parent, {});
        } else if (item.dataValue == "ProcessRedemptions") {
            $scope.$parent.$parent.$parent.Variables.ProcessRedemption.invoke();
            // $scope.Variables.SettleRedemption.invoke();
        } else if (item.dataValue == "PaySearchFee") {
            DialogService.open("PaySearchDialog", $scope.$parent.$parent.$parent, {});
        } else if (item.dataValue == "PayTakeNotice") {
            DialogService.open("PayTakeNoticeDialog", $scope.$parent.$parent.$parent, {});
        } else if (item.dataValue == "PayPublication") {
            DialogService.open("PayPublicationDialog", $scope.$parent.$parent.$parent, {});
        }

    };

}]);

Application.$controller("CompleteSaleDialogsController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.CompleteSaleDialogsOpened = function($event, $isolateScope) {
            debugger
            $scope.Variables.svQryStateFee.update({}, function(data) {
                debugger;
            })
        };

    }
]);

Application.$controller("PaySearchDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("PayTakeNoticeDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("PayPublicationDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);