Application.$controller("ContactsPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */

    };


    $scope.button_contactClick = function($event, $isolateScope) {
        editFlag = false;
        $scope.Widgets.dialog_contact.open();
    };






    $scope.select_typeChange = function($event, $isolateScope, newVal, oldVal) {
        $scope.Variables.PlatformContactData.setFilter("typeId", $isolateScope.datavalue);
        $scope.Variables.PlatformContactData.update();
    };





    $scope.search_contactsSelect = function($event, $isolateScope, selectedValue) {
        $scope.Variables.PlatformContactData.setFilter("id", $isolateScope.datavalue);
        $scope.Variables.PlatformContactData.update();
    };

}]);

var editFlag = false;


Application.$controller("dialog_contactController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;



        $scope.dialog_contactOpened = function($event, $isolateScope) {
            if (!editFlag) {
                $scope.Widgets.ContactTable.selecteditem = {};
                $scope.Widgets.container_deals.show = false;
                $scope.Widgets.anchor_email.show = false;
                $scope.Widgets.anchor_map.show = false;
                $scope.Widgets.anchor_phone.show = false;
            } else {
                $scope.Widgets.dialog_contact.title = "Contact Detail (Contact ID " + $scope.Widgets.ContactTable.selecteditem.id + ")";
            }
        };

    }
]);

Application.$controller("ContactLiveFormController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("ContactTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;




        $scope.updaterowAction = function($event, $rowData) {
            editFlag = true;
            $scope.Widgets.dialog_contact.open();
        };

    }
]);