Application.$controller("InspectSearchPageController", ["$scope", "$timeout", "routeParams", function($scope, $timeout, routeParams) {
    "use strict";

    // Workaround for Google Map not getting loaded when hidden
    var fixGoogleMap = true;

    // Workaround for Google Stree View not getting loaded when hidden
    var fixGoogleStreetView = true;

    var fetch;
    var pageNumber;
    var isForwardNavigation = true;

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        pageNumber = 1;
        fetch = $scope.Variables.svPlatformPortalInspectFilterDataByDropDownID.dataBinding.fetch;

    };

    var updateUrlParam = function(newVal) {
        if (newVal && newVal.constructor.name === "Object") {
            routeParams.replace({
                "search": newVal.drop_down
            }, {
                "reload": false,
                "history": true
            });
        } else {
            routeParams.replace({
                "search": ""
            }, {
                "reload": false,
                "history": true
            });
        }
    };

    var disableNavigation = function() {
        $scope.Widgets.PreviousRecord.disabled = true;
        $scope.Widgets.NextRecord.disabled = true;
        $scope.Widgets.PreviousPage.disabled = true;
        $scope.Widgets.NextPage.disabled = true;
    };

    // Paging
    $scope.PreviousPageClick = function($event, $isolateScope) {
        disableNavigation();
        pageNumber = pageNumber - 1;
        $scope.Variables.svPlatformPortalInspectFilterDataByDropDownID.invoke();
    };

    $scope.NextPageClick = function($event, $isolateScope) {
        disableNavigation();
        pageNumber = pageNumber + 1;
        $scope.Variables.svPlatformPortalInspectFilterDataByDropDownID.invoke();
    };

    $scope.PreviousRecordClick = function($event, $isolateScope) {
        disableNavigation();
        isForwardNavigation = false;
        $scope.navigateRecords(-1);
    };

    $scope.NextRecordClick = function($event, $isolateScope) {
        disableNavigation();
        isForwardNavigation = true;
        $scope.navigateRecords(1);
    };

    $scope.SearchResultLiveListClick = function($event, $isolateScope) {
        // Find the current result index and update the Record Count UI
        var index = findCurrentIndex();
        var currentIndex = ((pageNumber - 1) * fetch) + index;
        $scope.Widgets.CurrentRecordIndex.caption = currentIndex;
        checkNavigationControls(currentIndex);
    };

    // Navigate Recrods - If flag is 1 then show Next Result, If flag is -1 then show Previous Result
    $scope.navigateRecords = function(flag) {
        // Find current selected Live List Index
        var index = findCurrentIndex();
        // Find current selected index in Total Search Result
        var currentIndexInTotal = ((pageNumber - 1) * fetch) + index;

        var sourceObj = $scope.Widgets.SearchResultLiveList.dataset;

        if (flag === 1) { // On Next Result
            if (currentIndexInTotal < $scope.Widgets.TotalRecordsIndex.caption) {
                if (index < fetch) {
                    $scope.Widgets.SearchResultLiveList.deselectItem(sourceObj[index - 1]);
                    $scope.Widgets.SearchResultLiveList.selectItem(sourceObj[index]);
                    checkNavigationControls(currentIndexInTotal + 1);
                } else {
                    $scope.NextPageClick({}, {});
                }
                $scope.Widgets.CurrentRecordIndex.caption = currentIndexInTotal + 1;
            }
        } else if (flag === -1) { // On Previous Result
            if (currentIndexInTotal > 1) {
                if (index > 1) {
                    $scope.Widgets.SearchResultLiveList.deselectItem(sourceObj[index - 1]);
                    $scope.Widgets.SearchResultLiveList.selectItem(sourceObj[index - 2]);
                    checkNavigationControls(currentIndexInTotal - 1);
                } else {
                    $scope.PreviousPageClick({}, {});
                }
                $scope.Widgets.CurrentRecordIndex.caption = currentIndexInTotal - 1;
            }
        }
    };

    function findCurrentIndex() {
        var index = 0;
        var sourceObj = $scope.Widgets.SearchResultLiveList.dataset;
        var selectedObj = $scope.Widgets.SearchResultLiveList.selecteditem;
        if (sourceObj && sourceObj.length > 0) {
            for (var i = 0; i < sourceObj.length; i++) {
                if (sourceObj[i].property_id === selectedObj.property_id) {
                    index = i + 1;
                    break;
                }
            }
        }
        return index;
    }

    function checkNavigationControls(index) {
        if (index <= 1) {
            $scope.Widgets.PreviousRecord.disabled = true;
        } else {
            $scope.Widgets.PreviousRecord.disabled = false;
        }
        if (index == $scope.Widgets.TotalRecordsIndex.caption) {
            $scope.Widgets.NextRecord.disabled = true;
        } else {
            $scope.Widgets.NextRecord.disabled = false;
        }

        if (pageNumber == 1) {
            $scope.Widgets.PreviousPage.disabled = true;
        } else {
            $scope.Widgets.PreviousPage.disabled = false;
        }

        if ((pageNumber * fetch) >= $scope.Widgets.TotalRecordsIndex.caption) {
            $scope.Widgets.NextPage.disabled = true;
        } else {
            $scope.Widgets.NextPage.disabled = false;
        }
        $timeout(setPageTitle);
    }

    var setPageTitle = function() {
        if ($scope.Widgets.SearchResultLiveList.selecteditem && $scope.Widgets.SearchResultLiveList.selecteditem.address) {
            $scope.pageTitle = "Newline Portal - Inspect - " + $scope.Widgets.SearchResultLiveList.selecteditem.address + " - " + $scope.Widgets.SearchResultLiveList.selecteditem.pin;
        } else {
            $scope.pageTitle = "Newline Portal - Inspect";
        }
    };


    function resetPaging() {
        //pageNumber => Current Page Number
        pageNumber = 1;
    }

    resetPaging();

    $scope.svPlatformPortalInspectFilterDataByDropDownIDonBeforeUpdate = function(variable, data) {
        data.page_number = pageNumber;
    };

    $scope.svPlatformPortalInspectFilterDataByDropDownIDonSuccess = function(variable, data) {
        // On no Search Result
        if (data && data.length == 0) {
            $scope.Widgets.CurrentRecordIndex.caption = 0;
            checkNavigationControls(0);
        } else if (data && data.length > 0) {
            // Clear Map and render new Markers on Map
            $scope.$parent.$parent.googleMapData = data;
            $scope.$root.$emit("google-map-invoke-clear-and-render-markers");

            var indexToSet = 0;
            if (isForwardNavigation) {
                var currentIndex = ((pageNumber - 1) * fetch) + 1;
                $scope.Widgets.CurrentRecordIndex.caption = currentIndex;
                checkNavigationControls(currentIndex);
            } else {
                // If Search Result Count is less than Result Page Size then select last Search Result
                if (data.length < fetch) {
                    indexToSet = data.length - 1;
                } else {
                    indexToSet = fetch - 1;
                }
                var currentIndex1 = pageNumber * fetch;
                $scope.Widgets.CurrentRecordIndex.caption = currentIndex1;
                checkNavigationControls(currentIndex1);
                isForwardNavigation = true;
            }
            $timeout(function() {
                // Clear the existing selection
                $scope.Widgets.SearchResultLiveList.selecteditem = undefined;
                $scope.Widgets.SearchResultLiveList.selectItem($scope.Widgets.SearchResultLiveList.dataset[indexToSet]);

                // Create Reference in Parent page for other Widgets to access
                $scope.$parent.$parent.Widgets.SearchResultLiveList = $scope.Widgets.SearchResultLiveList;
                setPageTitle();

                // Workaround for Google Map not getting loaded when hidden
                $timeout(function() {
                    if (fixGoogleMap) {
                        $scope.$root.$emit("fix-google-map");
                        fixGoogleMap = false;
                    }
                });

                // Workaround for Google Stree View not getting loaded when hidden
                if (fixGoogleStreetView) {
                    $scope.$root.$emit("fix-google-street-view");
                    fixGoogleStreetView = false;
                }

                /* Offline Code - Starts Here */
                if ($scope.activePageName === "inspect" || $scope.activePageName === "InspectMobile") {
                    if (data && data.length > 0) {
                        $scope.emitSearchResultChange();
                    }
                }
                /* Offline Code - Ends Here */
            });
        }
    };

    $scope.emitSearchResultChange = function() {
        if ($scope.Widgets.SearchResultLiveList.dataset.length > 0) {
            $scope.$root.$emit("inspect-search-result-page-change");
        } else {
            $timeout(function() {
                emitSearchResultChange();
            }, 1000);
        }
    }

    $scope.selectDropDownChange = function($event, $isolateScope, newVal, oldVal) {
        updateUrlParam(newVal);
        resetPaging();
    };


    $scope.RefreshBtnClick = function($event, $isolateScope) {
        resetPaging();
        $scope.Variables.svPlatformPortalInspectFilterDataByDropDownID.invoke();
    };


    $scope.svDropDownIDsonSuccess = function(variable, data) {
        if ($scope.$parent.$parent.pageParams.search) {
            $timeout(function() {
                $scope.Widgets.selectDropDown.datavalue = {
                    "drop_down": $scope.$parent.$parent.pageParams.search
                };
                $scope.$parent.$parent.pageParams.search = "";
            });
        }
    };

}]);


Application.$controller("SearchResultLiveListController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);