Application.$controller("AdminTasksPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        // Change height of UI components on Window Resize 
        $(window).resize(function() {

            $scope.Widgets.SearchContainer.height = $(window).height() - 60;
            $scope.Widgets.SearchContainer.width = $("[name=LeftGridColumn]").width();
            $scope.Widgets.WM_RUN_WINDOW.height = $(window).height() - 60;
        });

        $(window).resize();
        // alert
    };

}]);