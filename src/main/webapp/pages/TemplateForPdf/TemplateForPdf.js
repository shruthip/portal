Application.$controller("TemplateForPdfPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

    $scope.liveform1Beforeservicecall = function($event, $operation, $data) {
        $data.template = $scope.Widgets.textEditor.editorvalue;
    };

}]);

Application.$controller("grid1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("liveform1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.textEditorLoad = function($isolateScope) {
            $scope.Widgets.textEditor.editorvalue = $scope.Widgets.template.datavalue;
            $scope.Widgets.template.show = false;
        };

    }
]);