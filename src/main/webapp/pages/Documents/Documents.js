Application.$controller("DocumentsPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        $scope.Widgets.buttonMinusParties.show = false;
        // $scope.Widgets.DocumentLiveList.show = false;
    };
    $scope.$on("myEvent", function() {
        $scope.Variables.PlatformPdfData.update();
    });
    $scope.buttonMinusDocumentsClick = function(buttonId) {

        if (buttonId == 'minus_btndocument') {
            $scope.Widgets.buttonMinusParties.show = false;
            $scope.Widgets.buttonPlusParties.show = true;
            $scope.Widgets.DocumentLiveList.show = false;

        } else {
            $scope.Widgets.buttonMinusParties.show = true;
            $scope.Widgets.buttonPlusParties.show = false;
            $scope.Widgets.DocumentLiveList.show = true;

        }
    }

    $scope.getEscapedUrl = function(item) {
        var tempUrl = "";
        if (item) {
            tempUrl = location.pathname + "services/pdfJava/DownloadFromAzureStorage?url=" + escape(item.link);
        }
        return tempUrl;
    };

    $scope.GeneratePdfBtnClick = function($event, $isolateScope) {
        $scope.Widgets.GeneratePdfContainer.content = "GeneratePdf";
    };



}]);

Application.$controller("UploadPdfDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("deleteBlobFromAzureController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);