Application.$controller("MapPageController", ["$scope", "$timeout", function($scope, $timeout) {
    "use strict";

    // Constants & Variables
    var GOOGLE_MAPS_API_KEY;
    var map;
    var GeoMarker;
    var markers = [];
    var infoWindow = null;
    var mapFeatures = [];
    var wText = 'Loading data from Parcel API...';
    var contentString = "Loading data from Parcel API....";
    var watchId;
    var geoErrorListner;

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */

        //Code to invoke Map in Photos and Links Bin
        if ($scope.activePageName != "inspect" && $scope.activePageName != "InspectMobile") {
            $("#gMapsDiv").height(412);
        }

        checkGoogleMapsApiKeyAvailability();

        // If Inspect page then set the Map to center on Property chnage
        if ($scope.activePageName === "inspect" || $scope.activePageName === "InspectMobile") {
            $scope.$watch(function() {
                try {
                    return $scope.$parent.Widgets.SearchContainer.Widgets.SearchResultLiveList.selecteditem.order;
                } catch (e) {
                    //Error
                    return undefined;
                }
            }, function() {
                try {
                    var coords = $scope.$parent.Widgets.SearchContainer.Widgets.SearchResultLiveList.selecteditem.lookup.split(",");
                    var latlng = new google.maps.LatLng(coords[0], coords[1]);
                    map.setCenter(latlng);
                } catch (e) {
                    //Error
                }
            });
        }
    };

    var checkGoogleMapsApiKeyAvailability = function() {
        if ($scope.Variables.svReadPropertyGoogleMapsApiKey.dataSet.value) {
            GOOGLE_MAPS_API_KEY = $scope.Variables.svReadPropertyGoogleMapsApiKey.dataSet.value;

            // Load the scripts and start maps rendering in UI
            loadScripts();
        } else {
            $timeout(checkGoogleMapsApiKeyAvailability, 500);
        }
    };

    /* Map Script - Starts Here */

    // Load Google Maps and ReportAllUsa scripts, in order.
    function loadScripts() {
        // Check whether Google Map API is already loaded or not
        if (typeof google === 'object' && typeof google.maps === 'object') {
            loadOtherScripts();
        } else {
            var googleMapsJS = "https://maps.googleapis.com/maps/api/js?key=" + GOOGLE_MAPS_API_KEY;
            // Starting load of: googleMapsJS
            $.getScript(googleMapsJS, function() {
                loadOtherScripts();
            });
        }
    }

    function loadOtherScripts() {
        // googleMapsJS Script loaded successfully
        var reportAllJS = "https://reportallusa.com/overlay/js.php?v=1.0&map=Google&client=DBGUwzBN1o";
        $.getScript(reportAllJS, function() {
            // ReportAll JS loaded successfully
            var mapLabelJS = "resources/js/markerwithlabel.js";
            $.getScript(mapLabelJS, function() {
                // Marker with Label for Google Maps V3 loaded successfully
                var geolocationJS = "resources/js/geolocation-marker.js";
                $.getScript(geolocationJS, function() {
                    // Geolocation JS loaded successfully.
                    initMap();
                })
            });
        });
    }

    // Methid to Initalize Google Maps API
    function initMap() {

        // the default location for the map
        var defaultLocation = {
            lat: 41.636258,
            lng: -87.596769
        };

        // setup Maps in UI
        map = new google.maps.Map(document.getElementById('gMapsDiv'), {
            center: defaultLocation,
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        // create a default marker and drop it in the center
        var defaultMarker = {
            map: map,
            animation: google.maps.Animation.DROP,
            position: defaultLocation
        }

        // create a Geomarker
        GeoMarker = new GeolocationMarker();
        GeoMarker.setCircleOptions({
            fillColor: '#808080'
        });

        google.maps.event.addListenerOnce(GeoMarker, 'position_changed', function() {
            map.setCenter(this.getPosition());
            map.fitBounds(this.getBounds());
        });

        // Handle click event on the map created above
        map.addListener('click', function(event) {
            showREPDataOnClick(map, event);
        })

        // Create geolocation button 
        addYourLocationButton(map, defaultMarker);

        // Initialize REP
        REP.Layer.Google.Initialize(map);

        //Code to invoke Map in Photos and Links Bin
        if ($scope.activePageName != "inspect" && $scope.activePageName != "InspectMobile") {
            //$("#gMapsDiv").height(412);
            getGoogleMap();
        }
    }

    function addYourLocationButton(map, marker) {
        var controlDiv = document.createElement('div');
        var firstChild = document.createElement('button');
        firstChild.style.backgroundColor = '#fff';
        firstChild.style.border = 'none';
        firstChild.style.outline = 'none';
        firstChild.style.width = '28px';
        firstChild.style.hvareight = '28px';
        firstChild.style.borderRadius = '2px';
        firstChild.style.boxShadow = '0 1px 4px rgba(0,0,0,0.3)';
        firstChild.style.cursor = 'pointer';
        firstChild.style.marginRight = '10px';
        firstChild.style.padding = '0';
        firstChild.title = 'Your Location';
        controlDiv.appendChild(firstChild);

        var secondChild = document.createElement('div');
        secondChild.style.margin = '5px';
        secondChild.style.width = '18px';
        secondChild.style.height = '18px';
        secondChild.style.backgroundImage = 'url(https://maps.gstatic.com/tactile/mylocation/mylocation-sprite-2x.png)';
        secondChild.style.backgroundSize = '180px 18px';
        secondChild.style.backgroundPosition = '0 0';
        secondChild.style.backgroundRepeat = 'no-repeat';
        firstChild.appendChild(secondChild);

        google.maps.event.addListener(map, 'center_changed', function() {
            secondChild.style['background-position'] = '0 0';
        });

        var imgX, animationInterval;

        function showPosition(position) {
            var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            map.setCenter(latlng);
            clearInterval(animationInterval);
            secondChild.style['background-position'] = '-144px 0';
        }

        function animateGeolocationIcon() {
            imgX = imgX === '-18' ? '0' : '-18';
            secondChild.style['background-position'] = imgX + 'px 0';
        }

        // Clicking on the geolocation button 
        firstChild.addEventListener('click', function() {
            //GeoMarker.setMap(null)
            // Animate GeoLocation Icon
            imgX = '0';
            animationInterval = setInterval(animateGeolocationIcon, 500);

            // Check whether browser supports Geolocation or not
            if (navigator.geolocation) {
                // Check whether user is trying it Switch On or Switch Off Geolocation 
                if (watchId) {
                    navigator.geolocation.clearWatch(watchId);
                    watchId = null;

                    if (geoErrorListner) {
                        google.maps.event.removeListener(geoErrorListner);
                        geoErrorListner = null;
                    }

                    // Stop Animation
                    clearInterval(animationInterval);
                    secondChild.style['background-position'] = '0 0';

                    // Geolocation marker clicked. Switched Off Geolocation.
                    GeoMarker.setMap(map);
                } else {
                    watchId = navigator.geolocation.watchPosition(showPosition);
                    geoErrorListner = google.maps.event.addListener(GeoMarker, 'geolocation_error', function(e) {
                        alert('There was an error obtaining your position. Message: ' + e.message);
                    });


                    // Stop Animation
                    clearInterval(animationInterval);
                    secondChild.style['background-position'] = '-144px 0';

                    // end set center correctly
                    //Geolocation marker clicked. Switched On Geolocation.
                    GeoMarker.setMap(map);
                }
            } else {
                // Stop Animation
                clearInterval(animationInterval);
                secondChild.style['background-position'] = '0 0';

                alert("Your Browser does not support Geolocation");
                // end set center correctly
                // Geolocation marker clicked. Your Browser does not support Geolocation.
                GeoMarker.setMap(map);
            }
        });

        controlDiv.index = 1;
        map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(controlDiv);
    }

    // When the user clicks on a grid, highlight it and show info window populated with data fetched from ReportAllUSA service
    function showREPDataOnClick(map, event) {
        // If Map zoom level is less than 15, return
        if (map.getZoom() < REP.Layer.Google.MIN_ZOOM) {
            // Zoom Level too low for REP
            return;
        }

        // Close any previous InfoWindow and hide any previous features.
        if (infoWindow !== null) {
            infoWindow.close();
        }
        infoWindow = null;

        // Cleanup Map features
        for (var i = 0; i < mapFeatures.length; i++) mapFeatures[i].setMap(null);
        mapFeatures = []
        var latLng = event.latLng;

        REP.Layer.Google.IdentifyByPoint(map, latLng, function(resp) {
            if (resp.results.length) {
                var respRow0 = resp.results[0];
                for (var respKey in respRow0) {
                    var respVal = respRow0[respKey];
                    if (respVal === null) continue;
                    if (respKey === 'geom') {
                        for (var i = 0; i < respVal.length; i++) {
                            respVal[i].setOptions({
                                fillColor: 'rgb(144,238,144)',
                                strokeColor: 'rgb(200,0,0)'
                            });
                            respVal[i].setMap(map);
                        }
                        mapFeatures = respVal;
                    } else {
                        if (wText !== '') wText += '\n<br>';
                        wText += respKey + ': ' + respVal;
                    }
                }
            }
        });
        // old stuff
        infoWindow = new google.maps.InfoWindow({
            content: contentString,
            position: event.latLng
        });
        infoWindow.open(map);
        updateLatLong(event.latLng.lat(), event.latLng.lng());
    }

    // Updates lat and long in the static variable as well as triggers a call to the ReportAllAPI
    function updateLatLong(lat, long) {
        var pointDataString = "POINT(" + long + " " + lat + ")"
        $scope.Variables.svPointData.setData(pointDataString);
        // setting the data
        $scope.Variables.svReportallUsa.setInput('spatial_intersect', pointDataString);
        // Calling the ReportAll Webservice with required parameters
        $scope.Variables.svReportallUsa.update()
    }

    // The code in this method is triggered when ReportAllUSA returns success
    $scope.svReportallUsaonSuccess = function(variable, data) {
        // ReportAllUSA returned SUCCESS
        var result = data.results[0];
        var contentString = "ERROR: Invalid response from ReportAll Portal";
        if (result) {
            contentString = "parcel_id: " + result.parcel_id + "<br> county_id: " + result.county_id + "<br> county_name: " + result.county_name + "<br> muni_name: " + result.muni_name + "<br> state_abbr:" + result.state_abbr + "<br> addr_street_name: " + result.addr_street_name + "<br> addr_street_type: " + result.addr_street_type + "<br> census_zip: " + result.census_zip + "<br> mail_address3: " + result.mail_address3 + "<br> mkt_val_land:" + result.mkt_val_land + "<br> mkt_val_bldg:" + result.mkt_val_bldg + "<br> mkt_val_tot: " + result.mkt_val_tot + "<br> muni_id:" + result.muni_id + "<br> school_dist_id:" + result.school_dist_id + "<br> acreage_calc: " + result.acreage_calc + "<br>";
        }
        infoWindow.setContent(contentString);
    };

    // Create a marker with drop animation, given lat,long and text
    function dropMarkersWithText(lat, long, text, inspected) {
        if (isNaN(lat)) {
            // ERROR: lat is not a number: lat
            return;
        }
        if (isNaN(long)) {
            // ERROR: long is not a number: long
            return;
        }
        //"lat: " + lat + "long: " + long + "order: " + text
        var location = {
            lat: lat,
            lng: long
        };
        map.panTo(location);

        var color = (inspected == 1) ? 'green' : 'red';
        var marker = new MarkerWithLabel({
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                fillColor: color,
                fillOpacity: 0.8,
                scale: 10,
                strokeColor: 'white',
                strokeWeight: 1
            },
            position: location,
            labelContent: text,
            labelInBackground: false,
            labelAnchor: new google.maps.Point(5, 5),
            animation: google.maps.Animation.DROP,
            map: map,
        });
        markers.push(marker);
    }


    function clearAndRenderMarkers(data) {
        // clear previous markers
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        data.map(function(coord) {
            if (coord['lookup_type'] == 'address') {
                geocodeAddress(coord);
            } else {
                addMarker(coord);
            }
        });
    }

    function geocodeAddress(coord) {
        $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + coord['address'] + '&sensor=false&key=' + GOOGLE_MAPS_API_KEY, null, function(data) {
            if (data.status === 'OK') {
                var lat = data.results[0].geometry.location.lat;
                var long = data.results[0].geometry.location.lng;
                var text = coord['order'];
                var inspected = coord['inspected'];
                coord['lookup'] = lat + "," + long;
                dropMarkersWithText(lat, long, text, inspected);
            }
        });
    }

    function addMarker(coord) {
        var lat = parseFloat(coord['lookup'].split(',')[0]);
        var long = parseFloat(coord['lookup'].split(',')[1]);
        var text = coord['order'];
        var inspected = coord['inspected'];
        dropMarkersWithText(lat, long, text, inspected);
    }

    // Workaround for Google Map not getting loaded when hidden
    $scope.$root.$on("fix-google-map", function() {
        var center = map.getCenter();
        google.maps.event.trigger(map, 'resize');
        map.setCenter(center);
    });

    /* Map Script - Ends Here */

    $scope.$root.$on("google-map-invoke-clear-and-render-markers", function() {
        clearAndRenderMarkers($scope.$parent.$parent.googleMapData);
    });

    function getGoogleMap() {
        if ($scope.$parent.Variables.svPortalPropertyData && $scope.$parent.Variables.svPortalPropertyData.dataSet && $scope.$parent.Variables.svPortalPropertyData.dataSet.content && $scope.$parent.Variables.svPortalPropertyData.dataSet.content[0].property_address) {
            var data = [{
                "lookup_type": $scope.$parent.Variables.svPortalPropertyData.dataSet.content[0].lookup_type,
                "address": $scope.$parent.Variables.svPortalPropertyData.dataSet.content[0].property_address,
                "lookup": $scope.$parent.Variables.svPortalPropertyData.dataSet.content[0].lookup,
                "order": 1,
                "inspected": 1
            }];
            clearAndRenderMarkers(data);
        }
    }

    $scope.$root.$on("get-google-street-view-or-map", getGoogleMap);
}]);