Application.$controller("AuctionStatPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        $scope.Widgets.label24_1.title = $scope.Widgets.label24_1.caption;
    };


    $scope.svUpdateGradeonError = function(variable, data) {
        $scope.Variables.ncUpdateGradeError.setMessage("Error while updating Area Rating. Please check the values. <br>" + data);
        $scope.Variables.ncUpdateGradeError.invoke();
    };

}]);


Application.$controller("UpdateAreaRatingDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.UpdateAreaRatingDialogOpened = function($event, $isolateScope) {
            $scope.Widgets.form1.resetForm();
        };

    }
]);