Application.$controller("CertificateStatusPageController", ["$scope", "DialogService", "$timeout", function($scope, DialogService, $timeout) {
    "use strict";

    // Counter to avoid Update Certificate Dialog getting opened before Certificate data is loaded
    $scope.counterSvCertificateDataByPropertyID = 0;

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {

        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        // if ($scope.activePageName === "service") {
        //     $scope.Widgets.gridcolumn36.horizontalalign = "center";
        // }

        if ($scope.activePageName === "inspect" || $scope.activePageName === "InspectMobile") {
            $scope.Widgets.certificate_status.show = "true";
        } else {
            $scope.Widgets.certificate_status.show = "false";
        }
        if ($scope.activePageName === "service") {
            // $scope.Widgets.bin_header.padding = "1em 0em 0em 0em";
            $scope.Widgets.layoutgridmain.padding = "1em 0em 0em 0em";
        }
    };


    // $scope.updateCaseBtnClick = function($event, $isolateScope) {
    //     $scope.$parent.$parent.UpdateCasebtnClick($event, $isolateScope);
    // };

    $scope.svUpdateCertificateStatusonError = function(variable, data) {
        $scope.Variables.ErrorSQL.setMessage(data);
        $scope.Variables.ErrorSQL.invoke();
    };


    $scope.updateCertificateStatusBtnClick = function($event, $isolateScope) {
        if ($scope.counterSvCertificateDataByPropertyID === 0) {
            $scope.Widgets.CertificateSpinner.show = false;
            DialogService.open("UpdateCertificateStatusDialog", $scope, {
                "certificate_id": $scope.$parent.Widgets.SearchResultLiveList.selecteditem.certificate_id
            });
        } else {
            $scope.Widgets.CertificateSpinner.show = true;
            $timeout(function() {
                $scope.updateCertificateStatusBtnClick({}, {});
            }, 500);
        }
    };


    $scope.svCertificateDataByPropertyIDonBeforeUpdate = function(variable, data) {
        debugger;
        $scope.counterSvCertificateDataByPropertyID = $scope.counterSvCertificateDataByPropertyID + 1;
    };


    $scope.svCertificateDataByPropertyIDonResult = function(variable, data) {
        $scope.counterSvCertificateDataByPropertyID = $scope.counterSvCertificateDataByPropertyID - 1;
    };

}]);

Application.$controller("UpdateCertificateStatusDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;



        $scope.UpdateCertificateStatusDialogOpened = function($event, $isolateScope) {
            $scope.Widgets.formCertificateDetails.resetForm();
        };

    }
]);