Application.$controller("HamburgerPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

    $scope.HamburgerMenuClick = function($event, $isolateScope) {
        if ($scope.activePageName === "inspect" || $scope.activePageName === "AdminTasks") {
            if ($scope.$parent.$parent.$parent.Widgets.SearchContainer.show) {
                $scope.$parent.$parent.$parent.Widgets.SearchContainer.show = false;
                $scope.$parent.$parent.$parent.Widgets.LeftGridColumn.columnwidth = 0;
                $scope.$parent.$parent.$parent.Widgets.MainGridColumn.columnwidth = 12;
                $scope.Widgets.HamburgerMenuContainer.class = "hamburger-empty-container";
            } else {
                $scope.$parent.$parent.$parent.Widgets.SearchContainer.show = true;
                $scope.$parent.$parent.$parent.Widgets.LeftGridColumn.columnwidth = 2;
                $scope.$parent.$parent.$parent.Widgets.MainGridColumn.columnwidth = 10;
                $scope.Widgets.HamburgerMenuContainer.class = "col-md-2";
            }
        } else {
            if ($scope.$parent.$parent.$parent.Widgets.LeftPanel.show) {
                $scope.$parent.$parent.$parent.Widgets.LeftPanel.show = false;
                $scope.$parent.$parent.$parent.Widgets.gridcolumn6.columnwidth = 12;
                $scope.$parent.$parent.$parent.Widgets.gridcolumn6.class = "portrait-rightpanel1";
                $scope.Widgets.HamburgerMenuContainer.class = "hamburger-empty-container";
            } else {
                $scope.$parent.$parent.$parent.Widgets.LeftPanel.show = true;
                $scope.$parent.$parent.$parent.Widgets.gridcolumn6.columnwidth = 10;
                $scope.$parent.$parent.$parent.Widgets.gridcolumn6.class = "portrait-rightpanel";
                $scope.Widgets.HamburgerMenuContainer.class = "col-md-2";
            }
        }
    };

}]);