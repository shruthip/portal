Application.$controller("PartiesPageController", ["$scope", "DialogService", function($scope, DialogService) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };


    $scope.containerUpdatePartyClick = function($event, $isolateScope, item, currentItemWidgets) {
        DialogService.open("dialogUpdateParty", $scope, {
            "selectedParty": item
        });
    };


    $scope.containerUpdateInterestClick = function($event, $isolateScope, item, currentItemWidgets) {
        DialogService.open("dialogUpdatePartyInterest", $scope, {
            "selectedPartyInterest": item
        });
    };


    $scope.svUpdatePartyInterestonError = function(variable, data) {
        $scope.Variables.ErrorSQL.setMessage(data);
        $scope.Variables.ErrorSQL.invoke();
    };


    $scope.svDeletePartyInterestonError = function(variable, data) {
        $scope.Variables.ErrorSQL.setMessage(data);
        $scope.Variables.ErrorSQL.invoke();
    };


    $scope.svUpdatePartyonError = function(variable, data) {
        $scope.Variables.ErrorSQL.setMessage(data);
        $scope.Variables.ErrorSQL.invoke();
    };


    $scope.containerDeleteInterestClick = function($event, $isolateScope, item, currentItemWidgets) {
        DialogService.open("dialogDeletePartyInterest", $scope, {
            "selected_certificate_party_id": item.id
        });
    };


    $scope.AppPartybtnClick = function($event, $isolateScope) {
        DialogService.open("dialogAddInterestParty", $scope, {
            "selectedExistingParty": {
                "certificate_id": $scope.$parent.Widgets.SearchResultLiveList.selecteditem.certificate_id
            }
        });
    };


    $scope.svAddNewPartyonError = function(variable, data) {
        $scope.Variables.ErrorSQL.setMessage(data);
        $scope.Variables.ErrorSQL.invoke();
    };


    $scope.svLinkExistingPartyonError = function(variable, data) {
        $scope.Variables.ErrorSQL.setMessage(data);
        $scope.Variables.ErrorSQL.invoke();
    };


    $scope.buttonMinusPartiesClick = function(buttonId) {

        if (buttonId == 'minus_btnParty') {
            document.getElementById("minus_btnParty").style.display = 'none'; //step 2 :additional feature hide button
            document.getElementById("plus_btnParty").style.display = ''; //step 3:additional feature show button
            $scope.Widgets.layoutgrid2.show = false;

        } else {
            document.getElementById("plus_btnParty").style.display = 'none'; //step 2 :additional feature hide button
            document.getElementById("minus_btnParty").style.display = ''; //step 3:additional feature show button
            $scope.Widgets.layoutgrid2.show = true;
        }
    }

    function formateDate(strDate) {
        if (strDate) {
            strDate = strDate.replace(/(\d{4})-(\d{1,2})-(\d{1,2})/, function(match, y, m, d) {
                return m + '/' + d + '/' + y;
            });
        }
        return strDate;
    }

    $scope.printbtnClick = function($event, $isolateScope) {
        var w = window.open();

        var cRecord = $scope.$parent.Widgets.SearchResultLiveList.selecteditem;
        var certificateNumber = cRecord.certificate_number;
        var pin = cRecord.pin;

        var matterDetails = $scope.$parent.$parent.Widgets.containerLegal.Widgets;
        var courtCase = matterDetails.label23_1.caption || '';
        var filedDate = matterDetails.label24.caption.split('<b>').pop().split('</b>').shift();
        var courtDate = matterDetails.courtDate.caption.split('<b>').pop().split('</b>').shift();


        if (filedDate) {
            filedDate = moment(filedDate, 'MM/DD/YY', true).format('MM/DD/YYYY');
        }


        if (courtDate) {
            courtDate = moment(courtDate, 'MM/DD/YY', true).format('MM/DD/YYYY');
        }

        var statusCertificate = $scope.$parent.$parent.Widgets.container17.Variables.svCertificateDataByPropertyID.firstRecord;
        var dateSold = formateDate(statusCertificate.date_sold);
        var expireDate = formateDate(statusCertificate.expiration_date);
        var taxYear = statusCertificate.tax_year;

        var addressLine1 = "";
        var piqName = "";

        if ($scope.Variables.svPortalPropertyAddress.dataSet.content[0]) {
            var data = $scope.Variables.svPortalPropertyAddress.dataSet.content[0];
            addressLine1 = data.property_address || '';
            piqName = data.assessee_name;
        }


        var piqSourceObj = $scope.Variables.svQryDataProperty.dataSet.content[0];
        var piqSource = (piqSourceObj.address_source1 || '') + ' ' + (piqSourceObj.address_source2 || '');
        //var piqSource = piqSourceObj.address_source1 || '';
        var html = "<html> <head> <style> .bodytext { font-size: 7px;} table { border-collapse: collapse; } table, th, td { border: 1px thin black; padding: 5px;font-size: 12px;  } .underline { text-decoration: underline; } </style> <title> Control Sheet </title></head> <body class='bodytext'> <table width='100%' cellspacing='0' cellpadding='0' border=1> <tr> <td colspan='8'> <table border='0' width='100%'> <tr> <td nowrap> Certificate # </td> <td class='underline' nowrap> " + certificateNumber + "</td> <td nowrap> CASE NUMBER: </td> <td class='underline' nowrap> " + courtCase + " </td> <td nowrap> DATE FILED </td> <td class='underline' nowrap> " + filedDate + " </td> <td nowrap> COURT DATE </td> <td class='underline' nowrap> " + courtDate + " </td> </tr> <tr><td> PIN </td> <td colspan='7'> " + pin + " </td> </tr> <tr><td> Tax Year </td> <td colspan='7'> " + taxYear + " </td> </tr> <tr><td> Date Sold </td> <td colspan='7'> " + dateSold + " </td> </tr> <tr><td> Expiration Date </td> <td colspan='7'> " + expireDate + " </td> </tr> <tr><td> PIQ Name </td> <td colspan='7'> " + piqName + " </td> </tr> <tr><td> PIQ Address </td> <td colspan='7' class='underline'> " + addressLine1 + " </td> </tr> <tr><td> PIQ Source </td> <td colspan='7'> " + piqSource + " </td> </tr> </table> <tr> <td colspan='4'> Name </td> <td colspan='2'> Interest Type/Source </td> <td colspan='2'> Service Type </td> </tr> </td> </tr>";


        var parties = "";
        if ($scope.Variables.svQryParties.dataSet.content.length) {
            var listParties = $scope.Variables.svQryParties.dataSet.content
            var coValue = "";
            for (var i = 0; i < listParties.length; i++) {
                coValue = listParties[i].care_of != null ? listParties[i].care_of + "<br>" : "";
                parties += "<tr> <td colspan='4'>" + listParties[i].name + "<br>" + coValue + listParties[i].address + "<br>" + listParties[i].city + " " + listParties[i].state + " " + listParties[i].zip + " </td> <td colspan='2'> " + listParties[i].interest_type1 + " " + listParties[i].interest_type2 + " " + listParties[i].interest_type3 + "<br> " + listParties[i].source1 + " " + listParties[i].source2 + " </td> <td colspan='2'> " + listParties[i].service_type + " </td> </tr>";
            }
        } else {
            parties = "<tr> <td colspan='8' align='center'> No Data Found</td> </tr>";
        }

        var htmlContent = html + parties + "</table> </body> </html>";


        //$(w.document.body).html(htmlContent.replace(new RegExp('null', 'g'), ''));
        w.document.write(htmlContent.replace(new RegExp('null', 'g'), ''));
        w.print();

    };


    $scope.AddCourtFeebtnClick = function($event, $isolateScope) {
        DialogService.open("dialog5", $scope, {
            "certificate_id": $scope.$parent.Widgets.SearchResultLiveList.selecteditem.certificate_id
        });
    };


    $scope.svPayFeePreviewonSuccess = function(variable, data) {
        $scope.Widgets.textAmount1.datavalue = data[0].amount;
    };


    $scope.svPayFeeExecuteonError = function(variable, data) {
        $scope.Variables.ErrorSQL.setMessage(data);
        $scope.Variables.ErrorSQL.invoke();
    };


    $scope.svPayFeeExecuteonSuccess = function(variable, data) {
        if ($scope.$parent.Widgets.tabpane1.Variables) {
            $scope.$parent.Widgets.tabpane1.Variables.svQryDisbursement.invoke();
        }
    };

}]);

Application.$controller("dialogUpdatePartyController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
        $scope.selectedParty;

        $scope.checkboxPIQChange = function($event, $isolateScope, newVal, oldVal) {
            if ($scope.Variables.svPortalPropertyAddress.dataSet.content[0] && newVal == true) {
                var data = $scope.Variables.svPortalPropertyAddress.dataSet.content[0];
                $scope.Widgets.textAddress.datavalue = (data.address_line1 || '') + " " + (data.address_line2 || '');
                $scope.Widgets.textCity.datavalue = data.address_city || '';
                $scope.Widgets.selectState.datavalue = data.state || '';
                $scope.Widgets.textZip.datavalue = data.address_zip || '';

            }
        };


        $scope.dialogUpdatePartyOpened = function($event, $isolateScope) {
            //$scope.Widgets.formPartyDetails.resetForm();
            console.log("nothing");
        };

    }
]);

Application.$controller("dialogUpdatePartyInterestController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
        $scope.selectedPartyInterest;

        // $scope.containerDeleteLinkClick = function($event, $isolateScope) {
        //     $scope.Variables.svDeletePartyInterest.setInput("selected_certificate_party_id", $scope.selectedPartyInterest.id);
        //     $scope.Variables.svDeletePartyInterest.invoke();
        // };


        $scope.formPartyInterestSubmit = function($event, $isolateScope, $formData) {
            $scope.Variables.svUpdatePartyInterest.setInput("selected_certificate_party_id", $scope.selectedPartyInterest.id);
            $scope.Variables.svUpdatePartyInterest.invoke();
        };


        $scope.dialogUpdatePartyInterestOpened = function($event, $isolateScope) {
            $scope.Widgets.formPartyInterest.resetForm();
        };

    }
]);

Application.$controller("dialogAddInterestPartyController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
        $scope.selectedExistingParty;

        $scope.searchExistingPartySubmit = function($event, $isolateScope) {
            // debugger
            // console.log($scope.Widgets.searchExistingParty.datavalue);
            var searchText = $scope.Widgets.searchExistingParty.datavalue != undefined ? $scope.Widgets.searchExistingParty.datavalue : "";
            if (searchText.trim().length == 0) {
                $scope.Variables.ErrorValidation.setMessage("Please enter name of party to search");
                $scope.Variables.ErrorValidation.invoke();
                return;
            }
            $scope.Variables.ExistingParties.setInput("selectedName", $scope.Widgets.searchExistingParty.datavalue);
            $scope.Variables.ExistingParties.invoke();
        };


        $scope.formAddInterestPartySubmit = function($event, $isolateScope, $formData) {
            $scope.Variables.svAddNewParty.setInput("certificate_id", $scope.selectedExistingParty.certificate_id);
            $scope.Variables.svAddNewParty.invoke();
        };


        $scope.buttonLinkExistingClick = function($event, $isolateScope) {

            //check is any party selected
            if ($scope.Widgets.gridExistingParties.selecteditem.length == 0) {
                $scope.Variables.ErrorValidation.setMessage("Please select an existing party to link");
                $scope.Variables.ErrorValidation.invoke();
                return;
            }

            //validate input fields
            if ($scope.Widgets.selectExistingPartyService.datavalue == undefined || $scope.Widgets.selectExistingPartyService.datavalue == null || $scope.Widgets.selectExistingPartyService.datavalue == "") {
                $scope.Variables.ErrorValidation.setMessage("Please select a Service Type");
                $scope.Variables.ErrorValidation.invoke();
                return;
            }

            //invoke update service
            $scope.Variables.svLinkExistingParty.setInput("certificate_id", $scope.selectedExistingParty.certificate_id);
            $scope.Variables.svLinkExistingParty.setInput("party_id", $scope.Widgets.gridExistingParties.selecteditem.id);
            // console.log($scope.Variables.svLinkExistingParty);
            $scope.Variables.svLinkExistingParty.invoke();
        };


        $scope.dialogAddInterestPartyClose = function($event, $isolateScope) {
            $scope.Widgets.formAddInterestParty.resetForm();
            $scope.Variables.ExistingParties.clearData();
        };




        $scope.checkboxExistingPIQChange = function($event, $isolateScope, newVal, oldVal) {

            if ($scope.Variables.svPortalPropertyAddress.dataSet.content[0] && newVal == true) {
                var data = $scope.Variables.svPortalPropertyAddress.dataSet.content[0];
                $scope.Widgets.textExistingPartyAddress.datavalue = (data.address_line1 || '') + " " + (data.address_line2 || '');
                $scope.Widgets.textExistingPartyCity.datavalue = data.address_city || '';
                $scope.Widgets.selectExistingPartyState.datavalue = data.state || '';
                $scope.Widgets.textExistingPartyZip.datavalue = data.address_zip;

            }
        };


        $scope.selectPartyAgentChange = function($event, $isolateScope, newVal, oldVal) {

            if (newVal) {

                $scope.Widgets.textExistingPartyAddress.datavalue = newVal.address || '';
                $scope.Widgets.textExistingPartyCity.datavalue = newVal.city || '';
                $scope.Widgets.selectExistingPartyState.datavalue = newVal.state || '';
                $scope.Widgets.textExistingPartyZip.datavalue = newVal.zip;
                $scope.Widgets.selectExistingPartyClass.datavalue = newVal.class_;
                $scope.Widgets.textExistingPartyCO.datavalue = newVal.careOf;
            }

        };


        $scope.dialogAddInterestPartyOpened = function($event, $isolateScope) {
            //$scope.Widgets.textExistingPartyCO.datavalue = "";
            // $scope.Widgets.formAddInterestParty.resetForm();
        };

    }
]);

Application.$controller("dialogDeletePartyInterestController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
        $scope.selected_certificate_party_id;

        $scope.dialogDeletePartyInterestOk = function($event, $isolateScope) {
            $scope.Variables.svDeletePartyInterest.setInput("selected_certificate_party_id", $scope.selected_certificate_party_id);
            $scope.Variables.svDeletePartyInterest.invoke();
        };

    }
]);

Application.$controller("gridExistingPartiesController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("dialogPayCourtFeesController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
        $scope.certificate_id;


        $scope.btnSearchClick = function($event, $isolateScope) {
            $scope.Widgets.paymentDate.required = false;
            $scope.Widgets.textAmount.required = false;
        };


        $scope.btnAddFeesClick = function($event, $isolateScope) {
            $scope.Widgets.paymentDate.required = true;
            $scope.Widgets.textAmount.required = true;
        };


        $scope.formPayCourtFeesSubmit = function($event, $isolateScope, $formData) {

            //$scope.Variables.svPayFeePreview.setInput("certificate_id", 2);
            if ($scope.Widgets.paymentDate.required) {
                $scope.Variables.svPayFeeExecute.setInput("certificate_id", $scope.certificate_id);
                $scope.Variables.svPayFeeExecute.invoke();
            } else {
                $scope.Variables.svPayFeePreview.setInput("certificate_id", $scope.certificate_id);
                $scope.Variables.svPayFeePreview.invoke();
            }
        };

    }
]);

Application.$controller("dialog5Controller", ["$scope", "$timeout",
    function($scope, $timeout) {
        "use strict";
        $scope.ctrlScope = $scope;
        $scope.certificate_id;

        $scope.btnSearch1Click = function($event, $isolateScope) {

            if ($scope.Widgets.selectService1.datavalue == null || $scope.Widgets.selectType1.datavalue == null) {
                $timeout(function() {
                    $('[name="btnTest11"]').click();
                });
            } else {
                $scope.Variables.svPayFeePreview.setInput("certificate_id", $scope.certificate_id);
                $scope.Variables.svPayFeePreview.invoke();
            }
        };


        $scope.btnAddFees2Click = function($event, $isolateScope) {
            if ($scope.Widgets.form1.form1.$invalid) {
                $timeout(function() {
                    $('[name="btnTest11"]').click();
                });
            } else {
                $scope.Variables.svPayFeeExecute.setInput("certificate_id", $scope.certificate_id);
                $scope.Variables.svPayFeeExecute.invoke();
            }
        };

    }
]);