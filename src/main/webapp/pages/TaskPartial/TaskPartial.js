Application.$controller("TaskPartialPageController", ["$scope", "$compile", "DialogService", "wmSpinner", function($scope, $compile, DialogService, wmSpinner) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */



        //  $scope.Widgets.accordionpane_task.$element.find('.panel-heading').click();
        $scope.Widgets.accordionpane_task.$element.find('.panel-heading').each(function(index) {
            var $header = WM.element(this),
                $button = '<button ng-click="onAccordionButtonClick($event, ' + index + ')" class="btn-default btn app-button" type="button"><span class="btn-caption">Add Task</span></button>',
                // <button ng-click="onAccordionButtonClick1($event, ' + index + ')" class="btn-default btn app-button" type="button"><span class="btn-caption">Add Multiple</span></button>
                $compiledTl = $compile($button)($scope);
            $header.find('.panel-actions').prepend($compiledTl);

        });
        $scope.sortByName = true;
    };

    $scope.onAccordionButtonClick = function($event, index) {
        $event.stopPropagation();
        $scope.Variables.GlobalTaskData.dataSet = {};
        wmSpinner.show("", "spinnner-id_1", "", "");
        $scope.Variables.DefaultProperty.setFilter("id", $scope.pageParams.assetId);
        $scope.Variables.DefaultProperty.update({}, function(data) {
            DialogService.open('task_dialog', $scope, {
                'partialPage': "TaskPartial",
            });
            wmSpinner.hide("spinnner-id_1");
        })
    };

    $scope.onAccordionButtonClick1 = function($event, index) {
        $event.stopPropagation();
        $scope.Widgets.dialog_task_multiple.open();
    };


    $scope.NoteTypeonSuccess = function(variable, data) {
        // preparing the dataset
        _.each(data, function(obj) {
            $scope.Variables.AddTasks.dataSet.push({
                "typeId": obj.id,
                "typeName": obj.name,
                "userName": "",
                "userId": "",
                "note": ""
            });
        });

    };


    $scope.getListofTypeIdsonSuccess = function(variable, data) {
        var id = [];
        _.each(data.content, function(obj) {
            id.push(obj.id);
        });
        $scope.Variables.Platform_Note.setFilter("propertyId", $scope.pageParams.propertyId);
        $scope.Variables.Platform_Note.setFilter("noteTypeId", id);
        $scope.Variables.Platform_Note.update();
    };


    $scope.PlatformNoteDataonBeforeDatasetReady = function(variable, data) {
        if ($scope.sortByName === true) {
            var sortedList = _.sortBy(data, function(o) {
                return o.userByUserId.name;
            });
            $scope.sortByName = false;
            return sortedList;
        }

    };

}]);





Application.$controller("dialog_task_multipleController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.AddTasksTableDatarender = function($isolateScope, $data) {
            $isolateScope.selecteditem = _.filter($isolateScope.gridData, function(rowData) {
                return _.includes(_.map($isolateScope.items, 'typeId'), rowData.typeId)
            });
        };

    }
]);


Application.$controller("TaskLiveFormController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("AddTasksTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.addNewRowAction = function($event) {
            // Add Tasks to db
            var data = [];
            var selectedData = $scope.Widgets.AddTasksTable.selecteditem
            _.each(selectedData, function(obj) {
                data.push({
                    "propertyId": $scope.pageParams.propertyId,
                    "userByUserId": obj.userId,
                    "certificateId": null,
                    "createdTimestamp": null,
                    "doneTimestamp": null,
                    "dueTimestamp": null,
                    "userId": obj.userId.id,
                    "user": _.find($scope.Variables.UserData.dataSet.data, {
                        id: obj.userId.id
                    }).email,
                    "noteTypeId": obj.typeId,
                    "comments": obj.note
                });
            });
            $scope.Variables.AddMutipleTasks.setInput("Note", data);
            $scope.Variables.AddMutipleTasks.update({}, function(data) {
                $scope.Variables.getListofTypeIds.update({}, function(data) {
                    $scope.Widgets.dialog_task_multiple.close();
                });
            });
        };



        $scope.userNameOnClick = function($event, $isolateScope, rowData) {
            var userId = _.find($scope.Variables.UserData.dataSet.data, {
                name: $isolateScope.datavalue
            })
            $scope.formfields.userId.setProperty('value', userId);
        };

    }
]);

Application.$controller("NoteTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("table_taskController", ["$scope", "wmSpinner", "wmToaster", "DialogService",
    function($scope, wmSpinner, wmToaster, DialogService) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.$on("updateTaskData", function(event, data) {
            //$scope.Variables.getListofTypeIds.update();
            $scope.Widgets.table_task.refreshData()

        })

        $scope.updaterowAction = function($event, $rowData) {
            wmSpinner.show("", "spinnner-id", "", "");
            $scope.Variables.GlobalTaskData.dataSet = $rowData;
            debugger;
            $scope.Variables.DefaultProperty.setFilter("id", $scope.pageParams.assetId);
            $scope.Variables.DefaultProperty.update({}, function(data) {
                DialogService.open('task_dialog', $scope, {
                    'partialPage': "TaskPartial",
                });
                wmSpinner.hide("spinnner-id");
            })
        };

    }
]);