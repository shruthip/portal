var _WM_APP_PROPERTIES = {
  "activeTheme" : "Portal4",
  "defaultLanguage" : "en",
  "displayName" : "Portal",
  "homePage" : "Main",
  "name" : "Portal",
  "platformType" : "WEB",
  "securityEnabled" : "true",
  "supportedLanguages" : "en",
  "type" : "APPLICATION",
  "version" : "1.0"
};