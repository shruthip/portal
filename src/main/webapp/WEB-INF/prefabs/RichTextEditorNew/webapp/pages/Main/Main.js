Application.$controller("RichTextEditorNewController", ["$scope", "$timeout", function($scope, $timeout) {
    "use strict";

    /* 
     * This function will be invoked when any of this prefab's property is changed
     * @key: property name
     * @newVal: new value of the property
     * @oldVal: old value of the property
     */
    function propertyChangeHandler(key, newVal, oldVal) {

        }
        /* register the property change handler */
    $scope.propertyManager.add($scope.propertyManager.ACTIONS.CHANGE, propertyChangeHandler);

    $scope.onInitPrefab = function() {
        // this method will be triggered post initialization of the prefab.
        var checkEditorAvailability = function() {
            var editorId = 'editor1';
            if (document.getElementById(editorId)) {

                CKEDITOR.replace(editorId, {
                    allowedContent: true
                });
                var updateEditorChanges = function() {
                    var currentScope = $scope;
                    $timeout(function() {
                        currentScope.editorvalue = CKEDITOR.instances[editorId].getData();
                    });
                };

                CKEDITOR.instances[editorId].on('key', updateEditorChanges);
                CKEDITOR.instances[editorId].on('change', updateEditorChanges);
            } else {
                $timeout(checkEditorAvailability);
            }
        };

        var richTextEditorJS = "//cdn.ckeditor.com/4.6.2/full/ckeditor.js";
        $.getScript(richTextEditorJS, function() {
            $timeout(checkEditorAvailability);
        });
    }
}]);