package com.portal;

import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.wavemaker.commons.WMRuntimeException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.Date;
import java.util.TimeZone;

// START Google Calendar Imports

import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;

// END Google Calendar Imports

/**
 * Created by anveshn on 10/27/2017.
 */
public class CalendarService {

    /////// BEGIN  Strings used ///////////
    private static final Logger logger = LoggerFactory.getLogger(CalendarService.class);
    /**
     * Global instance of the JSON factory.
     */
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    /**
     * Global instance of the HTTP transport.
     */
    private static HttpTransport httpTransport;
    //Review: Please check the applicationname is mandatory. I remember it's an optional input.
    @Value("${app.environment.APPLICATION_NAME}")
    private String APPLICATION_NAME;
    @Value("${app.environment.SERVICE_ACCOUNT_PRIVATE_KEY_FILE}")
    private String SERVICE_ACCOUNT_PRIVATE_KEY_FILE;

    @Value("${app.environment.SERVICE_ACCOUNT_EMAIL}")
    private String SERVICE_ACCOUNT_EMAIL;
    @Value("${app.environment.GOOGLE_SHARED_CALENDAR_ID}")
    private String CALENDAR_ID;
    
    /////// END Strings used ///////////
    
    // This method sets up the service class
    private Calendar configure() {
        //Review: Why we need two try-catch blocks. 
        try {
            try {
                httpTransport = new NetHttpTransport();
                URL loc = this.getClass().getResource(SERVICE_ACCOUNT_PRIVATE_KEY_FILE);
                String path = loc.getPath();
                File file = new File(path);

                // service account credential (uncomment setServiceAccountUser for domain-wide delegation)
                GoogleCredential credential = new GoogleCredential.Builder()
                        .setTransport(httpTransport)
                        .setJsonFactory(JSON_FACTORY)
                        .setServiceAccountId(SERVICE_ACCOUNT_EMAIL)
                        .setServiceAccountScopes(Collections.singleton(CalendarScopes.CALENDAR)) 
                        .setServiceAccountPrivateKeyFromP12File(file)
                        .build();
                Calendar client = new Calendar.Builder(
                        httpTransport, JSON_FACTORY, credential)
                        .setApplicationName(APPLICATION_NAME)
                        .build();
                logger.info("Client generation successfull : " + client);
                return client;
            } catch (IOException e) {
                //Review: Do not supress the exception, let's throw it.
                logger.error("Could not find p12");
                e.printStackTrace();
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
        //Review: this line is not required once we done with the exception handling.
        return null;
    }

    /*
    * Inserts the event if there is no id present in event object. Otherwise updates the event based on the id in event object.
    * startDateTime - The (inclusive) start time of the event.
    * endDateTime - The (exclusive) end time of the event.
    * timeZoneID - time zone; if null, it is interpreted as TimeZone.getDefault()
    * Reference:
    * (1) https://developers.google.com/resources/api-libraries/documentation/calendar/v3/java/latest/index.html?com/google/api/services/calendar/Calendar.Events.List.html
    * (2) https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
    */
    public Event insertOrUpdateEvent(Event event, Long startDateTime, Long endDateTime, String timeZoneID) throws Exception {
        logger.debug("startDateTime: " + startDateTime);
        logger.debug("endDateTime: " + endDateTime);
        logger.debug("timeZoneID: " + timeZoneID);
        TimeZone tz = TimeZone.getTimeZone(timeZoneID);

        if(startDateTime != null) {
            EventDateTime startEventDateTime = new EventDateTime();
            startEventDateTime.setDateTime(new DateTime(new Date(startDateTime), tz));
            event.setStart(startEventDateTime);
        }
        if(endDateTime != null) {
            EventDateTime endEventDateTime = new EventDateTime();
            endEventDateTime.setDateTime(new DateTime(new Date(endDateTime), tz));
            event.setEnd(endEventDateTime);
        }
        //Review: Let's have one try-catch block for both insert and update operations.
        if(StringUtils.isBlank(event.getId())) {
            try {
                // Added Event
                event = getEventService().insert(CALENDAR_ID, event).setSendNotifications(true).execute();
                logger.info("-----------Event Created Sucessfully -----------------");
            }catch (Exception e){
                throw new Exception(e.getMessage());
            }
        } else {
            try {
                // updated Event
                event = getEventService().update(CALENDAR_ID, event.getId(), event).setSendNotifications(true).execute();
            }catch (Exception e){
                throw new Exception(e.getMessage());
            }
        }
        return event;
    }

    /*
  * Deletes the event of given eventID.
  */
    public void deleteEvent(String eventID) throws Exception{
        try {
            // Delete Event
            getEventService().delete(CALENDAR_ID, eventID).setSendNotifications(true).execute();
        }catch (Exception e){
            throw new WMRuntimeException(e.getMessage());
        }
    }
    
    private com.google.api.services.calendar.Calendar.Events getEventService() throws Exception {
        return configure().events();
    }


}