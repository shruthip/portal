package com.portal;


import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventAttendee;
import com.portal.platform.Deal;
import com.portal.platform.service.DealServiceImpl;
import com.wavemaker.commons.WMRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * Created by anveshn on 10/27/2017.
 */
public class DealPreProcessor extends DealServiceImpl {

    private static final Logger LOGGER = LoggerFactory.getLogger(DealPreProcessor.class);


    @Autowired
    private CalendarService calendarService;

    @Override
    @Transactional(value = "platformTransactionManager")
    public Deal create(Deal deal) {
        Event event = new Event();
        ArrayList<EventAttendee> attendees = new ArrayList<EventAttendee>();
        attendees.add(new EventAttendee().setEmail(deal.getContact().getEmail()));
        event.setAttendees(attendees);
       // Review: Lets do string concat using StringBuilder's append method.
        event.setSummary(deal.getOptionByTypeId().getName() + " :  " + deal.getAsset().getProperty().getAddressLine1());
        event.setLocation(deal.getAsset().getProperty().getAddressLine1());
        event.setDescription(deal.getNote());
        //check if closed date null or not
        if (deal.getClosingDate() != null) {
            try {
                Event createdEvent = calendarService.insertOrUpdateEvent(event, deal.getClosingDate().toDateTime().getMillis(), deal.getClosingDate().toDateTime().getMillis(), "Etc/UTC");
                deal.setExternalEventId(createdEvent.getId());
            } catch (Exception e) {
                throw new WMRuntimeException(e.getMessage());
            }
        }
        return super.create(deal);
    }

    @Override
    @Transactional(value = "platformTransactionManager")
    public Deal update(Deal deal) {
        LOGGER.info("------- Updated Deal --------------------------");
        Event event = new Event();
        ArrayList<EventAttendee> attendees = new ArrayList<EventAttendee>();
        attendees.add(new EventAttendee().setEmail(deal.getContact().getEmail()));
        event.setAttendees(attendees);
        event.setSummary(deal.getOptionByTypeId().getName() + " :  " + deal.getAsset().getProperty().getAddressLine1());
        event.setLocation(deal.getAsset().getProperty().getAddressLine1());
        event.setDescription(deal.getNote());
        //Review: prepare the start/end time in one place, store it in a variable and use it wherever needed. -  deal.getClosingDate().toDateTime().getMillis()
        if (deal.getClosingDate() != null && deal.getExternalEventId() == null) {
            try {
                Event createdEvent = calendarService.insertOrUpdateEvent(event, deal.getClosingDate().toDateTime().getMillis(), deal.getClosingDate().toDateTime().getMillis(), "Etc/UTC");
                deal.setExternalEventId(createdEvent.getId());
            } catch (Exception e) {
                throw new WMRuntimeException(e.getMessage());
            }
        } else {
            try {
                event.setId(deal.getExternalEventId());
                Event updatedEvent = calendarService.insertOrUpdateEvent(event, deal.getClosingDate().toDateTime().getMillis(), deal.getClosingDate().toDateTime().getMillis(), "Etc/UTC");
                deal.setExternalEventId(updatedEvent.getId());
            } catch (Exception e) {
                throw new WMRuntimeException(e.getMessage());
            }
        }
        return super.update(deal);
    }

    @Override
    @Transactional(value = "platformTransactionManager")
    public Deal delete(Integer dealId) {
        LOGGER.info("Deal deleted ----------------------");
        try {
            Deal deal = super.findById(dealId);
            if (deal.getExternalEventId() != null) {
                calendarService.deleteEvent(deal.getExternalEventId());
            }
        } catch (Exception e) {
            throw new WMRuntimeException(e.getMessage());
        }
        return super.delete(dealId);
    }

}
