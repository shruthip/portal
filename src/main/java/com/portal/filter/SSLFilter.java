package com.portal.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by jvenugopal on 06-Jan-17.
 */
public class SSLFilter implements Filter {

    private static final Logger logger = LoggerFactory.getLogger(SSLFilter.class);
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        final HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        chain.doFilter(new HttpServletRequestWrapper(httpServletRequest){
            public boolean isSecure() {
                String header = httpServletRequest.getHeader("x-forwarded-proto");
                logger.debug("x-forwarded-proto received as {}", header);
                if(header != null && header.trim().length() > 0 && header.equalsIgnoreCase("https"))
                    return true;
                return false;
            }
        }, response);
    }

    @Override
    public void destroy() {

    }
}
