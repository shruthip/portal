package com.portal;

import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventAttendee;
import com.portal.platform.Note;
import com.portal.platform.service.NoteServiceImpl;
import com.wavemaker.commons.WMRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;


/**
 * Created by anveshn on 10/27/2017.
 */
public class TaskPreProcessor extends NoteServiceImpl  {

    private static final Logger logger = LoggerFactory.getLogger(TaskPreProcessor.class);

    @Autowired
    private CalendarService calendarService;

    @Override
    @Transactional(value = "platformTransactionManager")
    public Note create(Note note)  {
        logger.debug("inside create method");
        logger.debug(note.toString());
        Event event = new Event();
        ArrayList<EventAttendee> attendees = new ArrayList<EventAttendee>();
        attendees.add(new EventAttendee().setEmail(note.getUserByUserId().getEmail()));
        event.setAttendees(attendees);
        if(note.getNoteType() !=null && note.getProperty() != null){
        event.setSummary(note.getNoteType().getName()+" :  "+note.getProperty().getAddressLine1());
        event.setLocation(note.getProperty().getAddressLine1());
        }
        event.setDescription(note.getComments());
        if(note.getDueTimestamp() != null){
            try{
                Event createdEvent = calendarService.insertOrUpdateEvent(event,note.getDueTimestamp().toDateTime().getMillis(),note.getDueTimestamp().toDateTime().getMillis(),"Etc/UTC");
                note.setExternalEventId(createdEvent.getId());
            }catch (Exception e){
                throw new WMRuntimeException(e.getMessage());
            }
        }
        return super.create(note);
    }

    @Override
    @Transactional(value = "platformTransactionManager")
    public Note update(Note note) {
        logger.debug("inside update method");
        logger.debug(note.toString());
        Event event = new Event();
        ArrayList<EventAttendee> attendees = new ArrayList<EventAttendee>();
        attendees.add(new EventAttendee().setEmail(note.getUser()));
        event.setAttendees(attendees);
        event.setSummary(note.getNoteType().getName() + " :  " + note.getProperty().getAddressLine1());
        event.setLocation(note.getProperty().getAddressLine1());
        event.setDescription(note.getComments());
        try {
            /*if(note.getDueTimestamp() != null && note.getExternalEventId() == null) {
                try{
                    Event createdEvent = calendarService.insertOrUpdateEvent(event, note.getDueTimestamp().toDateTime().getMillis(), note.getDueTimestamp().toDateTime().getMillis(), "Etc/UTC");
                    note.setExternalEventId(createdEvent.getId());

                }catch (Exception e){
                    throw new WMRuntimeException(e.getMessage());
                }

            }else{
                try{
                    event.setId(note.getExternalEventId());
                    Event  updatedEvent = calendarService.insertOrUpdateEvent(event, note.getDueTimestamp().toDateTime().getMillis(), note.getDueTimestamp().toDateTime().getMillis(), "Etc/UTC");
                    note.setExternalEventId(updatedEvent.getId());
                }catch (Exception e){
                    throw new WMRuntimeException(e.getMessage());
                }
            }*/
            if(note.getDueTimestamp() == null) {
            	if(note.getExternalEventId() != null) {
            		//delete event
            		calendarService.deleteEvent(note.getExternalEventId());
            		note.setExternalEventId(null);
            	} 
            } else {
            	if(note.getExternalEventId() != null) {
            		//set eventid as externaleventid
            		event.setId(note.getExternalEventId());
            	} 
            	//create or update event
            	Event currentEvent = calendarService.insertOrUpdateEvent(event, note.getDueTimestamp().toDateTime().getMillis(), note.getDueTimestamp().toDateTime().getMillis(), "Etc/UTC");
            	note.setExternalEventId(currentEvent.getId());
            }
        } catch (Exception ex){
            throw new WMRuntimeException(ex.getMessage(), ex);
        }
        return super.update(note);
    }

    @Override
    @Transactional(value = "platformTransactionManager")
    public Note delete(Integer noteId) {
        Note note = super.findById(noteId);
        if(note.getExternalEventId() !=null){
            try {
                calendarService.deleteEvent(note.getExternalEventId());
            }catch (Exception e){
                throw new WMRuntimeException(e.getMessage());
            }
        }
        return super.delete(noteId);
    }



}