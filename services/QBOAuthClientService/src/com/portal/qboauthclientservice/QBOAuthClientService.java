/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.qboauthclientservice;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;


import com.wavemaker.runtime.security.SecurityService;
import com.wavemaker.runtime.service.annotations.ExposeToClient;
import com.wavemaker.runtime.service.annotations.HideFromClient;

//import com.qbotest.qboauthclientservice.model.*;

//my imports
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;

import com.intuit.oauth2.client.OAuth2PlatformClient;
import com.intuit.oauth2.config.Environment;
import com.intuit.oauth2.config.OAuth2Config;

@ExposeToClient
public class QBOAuthClientService {

    private static final Logger logger = LoggerFactory.getLogger(QBOAuthClientService.class);
    
    @Value("${app.environment.qbClientId}")
    private String clientId;
    
    @Value("${app.environment.qbClientSecret}")
    private String clientSecret;
    
    OAuth2PlatformClient client;
	OAuth2Config oauth2Config;
    
    @PostConstruct
	public void init() {
		oauth2Config = new OAuth2Config.OAuth2ConfigBuilder(clientId, clientSecret)
				.callDiscoveryAPI(Environment.SANDBOX)
				.buildConfig();
		client  = new OAuth2PlatformClient(oauth2Config);
	}
	
	public OAuth2PlatformClient getOAuth2PlatformClient()  {
		return client;
	}
	
	public OAuth2Config getOAuth2Config()  {
		return oauth2Config;
	}

}
