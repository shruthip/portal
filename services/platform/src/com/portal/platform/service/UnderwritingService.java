/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.portal.platform.Underwriting;

/**
 * Service object for domain model class {@link Underwriting}.
 */
public interface UnderwritingService {

    /**
     * Creates a new Underwriting. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on Underwriting if any.
     *
     * @param underwriting Details of the Underwriting to be created; value cannot be null.
     * @return The newly created Underwriting.
     */
	Underwriting create(@Valid Underwriting underwriting);


	/**
	 * Returns Underwriting by given id if exists.
	 *
	 * @param underwritingId The id of the Underwriting to get; value cannot be null.
	 * @return Underwriting associated with the given underwritingId.
     * @throws EntityNotFoundException If no Underwriting is found.
	 */
	Underwriting getById(Integer underwritingId) throws EntityNotFoundException;

    /**
	 * Find and return the Underwriting by given id if exists, returns null otherwise.
	 *
	 * @param underwritingId The id of the Underwriting to get; value cannot be null.
	 * @return Underwriting associated with the given underwritingId.
	 */
	Underwriting findById(Integer underwritingId);


	/**
	 * Updates the details of an existing Underwriting. It replaces all fields of the existing Underwriting with the given underwriting.
	 *
     * This method overrides the input field values using Server side or database managed properties defined on Underwriting if any.
     *
	 * @param underwriting The details of the Underwriting to be updated; value cannot be null.
	 * @return The updated Underwriting.
	 * @throws EntityNotFoundException if no Underwriting is found with given input.
	 */
	Underwriting update(@Valid Underwriting underwriting) throws EntityNotFoundException;

    /**
	 * Deletes an existing Underwriting with the given id.
	 *
	 * @param underwritingId The id of the Underwriting to be deleted; value cannot be null.
	 * @return The deleted Underwriting.
	 * @throws EntityNotFoundException if no Underwriting found with the given id.
	 */
	Underwriting delete(Integer underwritingId) throws EntityNotFoundException;

	/**
	 * Find all Underwritings matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
	 *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
	 *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching Underwritings.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
	 */
    @Deprecated
	Page<Underwriting> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
	 * Find all Underwritings matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching Underwritings.
     *
     * @see Pageable
     * @see Page
	 */
    Page<Underwriting> findAll(String query, Pageable pageable);

    /**
	 * Exports all Underwritings matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
	 */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

	/**
	 * Retrieve the count of the Underwritings in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
	 * @return The count of the Underwriting.
	 */
	long count(String query);

	/**
	 * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
	 * @return Paginated data with included fields.

     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
	Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);


}

