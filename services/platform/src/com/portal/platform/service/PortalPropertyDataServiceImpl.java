/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.portal.platform.PortalPropertyData;
import com.portal.platform.PortalPropertyDataId;


/**
 * ServiceImpl object for domain model class PortalPropertyData.
 *
 * @see PortalPropertyData
 */
@Service("platform.PortalPropertyDataService")
@Validated
public class PortalPropertyDataServiceImpl implements PortalPropertyDataService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PortalPropertyDataServiceImpl.class);


    @Autowired
    @Qualifier("platform.PortalPropertyDataDao")
    private WMGenericDao<PortalPropertyData, PortalPropertyDataId> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<PortalPropertyData, PortalPropertyDataId> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "platformTransactionManager")
    @Override
	public PortalPropertyData create(PortalPropertyData portalPropertyData) {
        LOGGER.debug("Creating a new PortalPropertyData with information: {}", portalPropertyData);
        PortalPropertyData portalPropertyDataCreated = this.wmGenericDao.create(portalPropertyData);
        return portalPropertyDataCreated;
    }

	@Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
	public PortalPropertyData getById(PortalPropertyDataId portalpropertydataId) throws EntityNotFoundException {
        LOGGER.debug("Finding PortalPropertyData by id: {}", portalpropertydataId);
        PortalPropertyData portalPropertyData = this.wmGenericDao.findById(portalpropertydataId);
        if (portalPropertyData == null){
            LOGGER.debug("No PortalPropertyData found with id: {}", portalpropertydataId);
            throw new EntityNotFoundException(String.valueOf(portalpropertydataId));
        }
        return portalPropertyData;
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
	public PortalPropertyData findById(PortalPropertyDataId portalpropertydataId) {
        LOGGER.debug("Finding PortalPropertyData by id: {}", portalpropertydataId);
        return this.wmGenericDao.findById(portalpropertydataId);
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "platformTransactionManager")
	@Override
	public PortalPropertyData update(PortalPropertyData portalPropertyData) throws EntityNotFoundException {
        LOGGER.debug("Updating PortalPropertyData with information: {}", portalPropertyData);
        this.wmGenericDao.update(portalPropertyData);

        PortalPropertyDataId portalpropertydataId = new PortalPropertyDataId();
        portalpropertydataId.setPropertyId(portalPropertyData.getPropertyId());
        portalpropertydataId.setGradeId(portalPropertyData.getGradeId());
        portalpropertydataId.setPin(portalPropertyData.getPin());
        portalpropertydataId.setVolume(portalPropertyData.getVolume());
        portalpropertydataId.setMetesAndBounds(portalPropertyData.getMetesAndBounds());
        portalpropertydataId.setAddressLine1(portalPropertyData.getAddressLine1());
        portalpropertydataId.setAddressLine2(portalPropertyData.getAddressLine2());
        portalpropertydataId.setAddressCity(portalPropertyData.getAddressCity());
        portalpropertydataId.setAddressZip(portalPropertyData.getAddressZip());
        portalpropertydataId.setState(portalPropertyData.getState());
        portalpropertydataId.setTownshipName(portalPropertyData.getTownshipName());
        portalpropertydataId.setCountyName(portalPropertyData.getCountyName());
        portalpropertydataId.setCountyId(portalPropertyData.getCountyId());
        portalpropertydataId.setLatitude(portalPropertyData.getLatitude());
        portalpropertydataId.setLongitude(portalPropertyData.getLongitude());
        portalpropertydataId.setPropertyAddress(portalPropertyData.getPropertyAddress());
        portalpropertydataId.setAddressVerified(portalPropertyData.getAddressVerified());
        portalpropertydataId.setAddressSource1(portalPropertyData.getAddressSource1());
        portalpropertydataId.setAddressSource2(portalPropertyData.getAddressSource2());
        portalpropertydataId.setLotSize(portalPropertyData.getLotSize());
        portalpropertydataId.setYearBuilt(portalPropertyData.getYearBuilt());
        portalpropertydataId.setSqftTotal(portalPropertyData.getSqftTotal());
        portalpropertydataId.setLotBldg(portalPropertyData.getLotBldg());
        portalpropertydataId.setPropertyClass(portalPropertyData.getPropertyClass());
        portalpropertydataId.setAssessorCode(portalPropertyData.getAssessorCode());
        portalpropertydataId.setPropertyType(portalPropertyData.getPropertyType());
        portalpropertydataId.setPropertyTypeId(portalPropertyData.getPropertyTypeId());
        portalpropertydataId.setPropertyExterior(portalPropertyData.getPropertyExterior());
        portalpropertydataId.setPropertyExteriorId(portalPropertyData.getPropertyExteriorId());
        portalpropertydataId.setAssesseeName(portalPropertyData.getAssesseeName());
        portalpropertydataId.setAssesseeAddressLine1(portalPropertyData.getAssesseeAddressLine1());
        portalpropertydataId.setAssesseeAddressCity(portalPropertyData.getAssesseeAddressCity());
        portalpropertydataId.setAssesseeAddressState(portalPropertyData.getAssesseeAddressState());
        portalpropertydataId.setAssesseeAddressZip(portalPropertyData.getAssesseeAddressZip());
        portalpropertydataId.setGis(portalPropertyData.getGis());
        portalpropertydataId.setAssessor(portalPropertyData.getAssessor());
        portalpropertydataId.setTreasurer(portalPropertyData.getTreasurer());
        portalpropertydataId.setRecorder(portalPropertyData.getRecorder());
        portalpropertydataId.setClerk(portalPropertyData.getClerk());
        portalpropertydataId.setAssessorPhoto(portalPropertyData.getAssessorPhoto());
        portalpropertydataId.setGooglePhoto(portalPropertyData.getGooglePhoto());
        portalpropertydataId.setNewlinePhoto(portalPropertyData.getNewlinePhoto());
        portalpropertydataId.setLookupType(portalPropertyData.getLookupType());
        portalpropertydataId.setLookup(portalPropertyData.getLookup());

        return this.wmGenericDao.findById(portalpropertydataId);
    }

    @Transactional(value = "platformTransactionManager")
	@Override
	public PortalPropertyData delete(PortalPropertyDataId portalpropertydataId) throws EntityNotFoundException {
        LOGGER.debug("Deleting PortalPropertyData with id: {}", portalpropertydataId);
        PortalPropertyData deleted = this.wmGenericDao.findById(portalpropertydataId);
        if (deleted == null) {
            LOGGER.debug("No PortalPropertyData found with id: {}", portalpropertydataId);
            throw new EntityNotFoundException(String.valueOf(portalpropertydataId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

	@Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
	public Page<PortalPropertyData> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all PortalPropertyDatas");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
    @Override
    public Page<PortalPropertyData> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all PortalPropertyDatas");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service platform for table PortalPropertyData to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}

