/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.portal.platform.NumberEntity;


/**
 * ServiceImpl object for domain model class NumberEntity.
 *
 * @see NumberEntity
 */
@Service("platform.NumberEntityService")
@Validated
public class NumberEntityServiceImpl implements NumberEntityService {

    private static final Logger LOGGER = LoggerFactory.getLogger(NumberEntityServiceImpl.class);


    @Autowired
    @Qualifier("platform.NumberEntityDao")
    private WMGenericDao<NumberEntity, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<NumberEntity, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "platformTransactionManager")
    @Override
	public NumberEntity create(NumberEntity numberEntity) {
        LOGGER.debug("Creating a new NumberEntity with information: {}", numberEntity);
        NumberEntity numberEntityCreated = this.wmGenericDao.create(numberEntity);
        return numberEntityCreated;
    }

	@Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
	public NumberEntity getById(Integer numberentityId) throws EntityNotFoundException {
        LOGGER.debug("Finding NumberEntity by id: {}", numberentityId);
        NumberEntity numberEntity = this.wmGenericDao.findById(numberentityId);
        if (numberEntity == null){
            LOGGER.debug("No NumberEntity found with id: {}", numberentityId);
            throw new EntityNotFoundException(String.valueOf(numberentityId));
        }
        return numberEntity;
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
	public NumberEntity findById(Integer numberentityId) {
        LOGGER.debug("Finding NumberEntity by id: {}", numberentityId);
        return this.wmGenericDao.findById(numberentityId);
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "platformTransactionManager")
	@Override
	public NumberEntity update(NumberEntity numberEntity) throws EntityNotFoundException {
        LOGGER.debug("Updating NumberEntity with information: {}", numberEntity);
        this.wmGenericDao.update(numberEntity);

        Integer numberentityId = numberEntity.getNumber();

        return this.wmGenericDao.findById(numberentityId);
    }

    @Transactional(value = "platformTransactionManager")
	@Override
	public NumberEntity delete(Integer numberentityId) throws EntityNotFoundException {
        LOGGER.debug("Deleting NumberEntity with id: {}", numberentityId);
        NumberEntity deleted = this.wmGenericDao.findById(numberentityId);
        if (deleted == null) {
            LOGGER.debug("No NumberEntity found with id: {}", numberentityId);
            throw new EntityNotFoundException(String.valueOf(numberentityId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

	@Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
	public Page<NumberEntity> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all NumberEntities");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
    @Override
    public Page<NumberEntity> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all NumberEntities");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service platform for table NumberEntity to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}

