/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.portal.platform.CertificateParty;
import com.portal.platform.InterestType;

/**
 * Service object for domain model class {@link InterestType}.
 */
public interface InterestTypeService {

    /**
     * Creates a new InterestType. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on InterestType if any.
     *
     * @param interestType Details of the InterestType to be created; value cannot be null.
     * @return The newly created InterestType.
     */
	InterestType create(@Valid InterestType interestType);


	/**
	 * Returns InterestType by given id if exists.
	 *
	 * @param interesttypeId The id of the InterestType to get; value cannot be null.
	 * @return InterestType associated with the given interesttypeId.
     * @throws EntityNotFoundException If no InterestType is found.
	 */
	InterestType getById(String interesttypeId) throws EntityNotFoundException;

    /**
	 * Find and return the InterestType by given id if exists, returns null otherwise.
	 *
	 * @param interesttypeId The id of the InterestType to get; value cannot be null.
	 * @return InterestType associated with the given interesttypeId.
	 */
	InterestType findById(String interesttypeId);

    /**
	 * Find and return the InterestType for given code  andname  if exists.
	 *
	 * @param code value of code; value cannot be null.
	 * @param name value of name; value cannot be null.
	 * @return InterestType associated with the given inputs.
     * @throws EntityNotFoundException if no matching InterestType found.
	 */
    InterestType getByCodeAndName(String code, String name)throws EntityNotFoundException;

	/**
	 * Updates the details of an existing InterestType. It replaces all fields of the existing InterestType with the given interestType.
	 *
     * This method overrides the input field values using Server side or database managed properties defined on InterestType if any.
     *
	 * @param interestType The details of the InterestType to be updated; value cannot be null.
	 * @return The updated InterestType.
	 * @throws EntityNotFoundException if no InterestType is found with given input.
	 */
	InterestType update(@Valid InterestType interestType) throws EntityNotFoundException;

    /**
	 * Deletes an existing InterestType with the given id.
	 *
	 * @param interesttypeId The id of the InterestType to be deleted; value cannot be null.
	 * @return The deleted InterestType.
	 * @throws EntityNotFoundException if no InterestType found with the given id.
	 */
	InterestType delete(String interesttypeId) throws EntityNotFoundException;

	/**
	 * Find all InterestTypes matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
	 *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
	 *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching InterestTypes.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
	 */
    @Deprecated
	Page<InterestType> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
	 * Find all InterestTypes matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching InterestTypes.
     *
     * @see Pageable
     * @see Page
	 */
    Page<InterestType> findAll(String query, Pageable pageable);

    /**
	 * Exports all InterestTypes matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
	 */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

	/**
	 * Retrieve the count of the InterestTypes in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
	 * @return The count of the InterestType.
	 */
	long count(String query);

	/**
	 * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
	 * @return Paginated data with included fields.

     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
	Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);

    /*
     * Returns the associated certificatePartiesForInterestType1 for given InterestType id.
     *
     * @param code value of code; value cannot be null
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of associated CertificateParty instances.
     *
     * @see Pageable
     * @see Page
     */
    Page<CertificateParty> findAssociatedCertificatePartiesForInterestType1(String code, Pageable pageable);

    /*
     * Returns the associated certificatePartiesForInterestType2 for given InterestType id.
     *
     * @param code value of code; value cannot be null
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of associated CertificateParty instances.
     *
     * @see Pageable
     * @see Page
     */
    Page<CertificateParty> findAssociatedCertificatePartiesForInterestType2(String code, Pageable pageable);

    /*
     * Returns the associated certificatePartiesForInterestType3 for given InterestType id.
     *
     * @param code value of code; value cannot be null
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of associated CertificateParty instances.
     *
     * @see Pageable
     * @see Page
     */
    Page<CertificateParty> findAssociatedCertificatePartiesForInterestType3(String code, Pageable pageable);

}

