/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.portal.platform.Property;
import com.portal.platform.PropertyExterior;

/**
 * Service object for domain model class {@link PropertyExterior}.
 */
public interface PropertyExteriorService {

    /**
     * Creates a new PropertyExterior. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on PropertyExterior if any.
     *
     * @param propertyExterior Details of the PropertyExterior to be created; value cannot be null.
     * @return The newly created PropertyExterior.
     */
	PropertyExterior create(@Valid PropertyExterior propertyExterior);


	/**
	 * Returns PropertyExterior by given id if exists.
	 *
	 * @param propertyexteriorId The id of the PropertyExterior to get; value cannot be null.
	 * @return PropertyExterior associated with the given propertyexteriorId.
     * @throws EntityNotFoundException If no PropertyExterior is found.
	 */
	PropertyExterior getById(Integer propertyexteriorId) throws EntityNotFoundException;

    /**
	 * Find and return the PropertyExterior by given id if exists, returns null otherwise.
	 *
	 * @param propertyexteriorId The id of the PropertyExterior to get; value cannot be null.
	 * @return PropertyExterior associated with the given propertyexteriorId.
	 */
	PropertyExterior findById(Integer propertyexteriorId);

    /**
	 * Find and return the PropertyExterior for given exterior  if exists.
	 *
	 * @param exterior value of exterior; value cannot be null.
	 * @return PropertyExterior associated with the given inputs.
     * @throws EntityNotFoundException if no matching PropertyExterior found.
	 */
    PropertyExterior getByExterior(String exterior)throws EntityNotFoundException;

	/**
	 * Updates the details of an existing PropertyExterior. It replaces all fields of the existing PropertyExterior with the given propertyExterior.
	 *
     * This method overrides the input field values using Server side or database managed properties defined on PropertyExterior if any.
     *
	 * @param propertyExterior The details of the PropertyExterior to be updated; value cannot be null.
	 * @return The updated PropertyExterior.
	 * @throws EntityNotFoundException if no PropertyExterior is found with given input.
	 */
	PropertyExterior update(@Valid PropertyExterior propertyExterior) throws EntityNotFoundException;

    /**
	 * Deletes an existing PropertyExterior with the given id.
	 *
	 * @param propertyexteriorId The id of the PropertyExterior to be deleted; value cannot be null.
	 * @return The deleted PropertyExterior.
	 * @throws EntityNotFoundException if no PropertyExterior found with the given id.
	 */
	PropertyExterior delete(Integer propertyexteriorId) throws EntityNotFoundException;

	/**
	 * Find all PropertyExteriors matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
	 *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
	 *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching PropertyExteriors.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
	 */
    @Deprecated
	Page<PropertyExterior> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
	 * Find all PropertyExteriors matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching PropertyExteriors.
     *
     * @see Pageable
     * @see Page
	 */
    Page<PropertyExterior> findAll(String query, Pageable pageable);

    /**
	 * Exports all PropertyExteriors matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
	 */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

	/**
	 * Retrieve the count of the PropertyExteriors in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
	 * @return The count of the PropertyExterior.
	 */
	long count(String query);

	/**
	 * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
	 * @return Paginated data with included fields.

     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
	Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);

    /*
     * Returns the associated properties for given PropertyExterior id.
     *
     * @param id value of id; value cannot be null
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of associated Property instances.
     *
     * @see Pageable
     * @see Page
     */
    Page<Property> findAssociatedProperties(Integer id, Pageable pageable);

}

