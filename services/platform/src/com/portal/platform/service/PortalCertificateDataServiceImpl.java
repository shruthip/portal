/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.portal.platform.PortalCertificateData;
import com.portal.platform.PortalCertificateDataId;


/**
 * ServiceImpl object for domain model class PortalCertificateData.
 *
 * @see PortalCertificateData
 */
@Service("platform.PortalCertificateDataService")
@Validated
public class PortalCertificateDataServiceImpl implements PortalCertificateDataService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PortalCertificateDataServiceImpl.class);


    @Autowired
    @Qualifier("platform.PortalCertificateDataDao")
    private WMGenericDao<PortalCertificateData, PortalCertificateDataId> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<PortalCertificateData, PortalCertificateDataId> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "platformTransactionManager")
    @Override
	public PortalCertificateData create(PortalCertificateData portalCertificateData) {
        LOGGER.debug("Creating a new PortalCertificateData with information: {}", portalCertificateData);
        PortalCertificateData portalCertificateDataCreated = this.wmGenericDao.create(portalCertificateData);
        return portalCertificateDataCreated;
    }

	@Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
	public PortalCertificateData getById(PortalCertificateDataId portalcertificatedataId) throws EntityNotFoundException {
        LOGGER.debug("Finding PortalCertificateData by id: {}", portalcertificatedataId);
        PortalCertificateData portalCertificateData = this.wmGenericDao.findById(portalcertificatedataId);
        if (portalCertificateData == null){
            LOGGER.debug("No PortalCertificateData found with id: {}", portalcertificatedataId);
            throw new EntityNotFoundException(String.valueOf(portalcertificatedataId));
        }
        return portalCertificateData;
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
	public PortalCertificateData findById(PortalCertificateDataId portalcertificatedataId) {
        LOGGER.debug("Finding PortalCertificateData by id: {}", portalcertificatedataId);
        return this.wmGenericDao.findById(portalcertificatedataId);
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "platformTransactionManager")
	@Override
	public PortalCertificateData update(PortalCertificateData portalCertificateData) throws EntityNotFoundException {
        LOGGER.debug("Updating PortalCertificateData with information: {}", portalCertificateData);
        this.wmGenericDao.update(portalCertificateData);

        PortalCertificateDataId portalcertificatedataId = new PortalCertificateDataId();
        portalcertificatedataId.setCertificateId(portalCertificateData.getCertificateId());
        portalcertificatedataId.setPropertyId(portalCertificateData.getPropertyId());
        portalcertificatedataId.setMatterId(portalCertificateData.getMatterId());
        portalcertificatedataId.setCountyName(portalCertificateData.getCountyName());
        portalcertificatedataId.setCertificateNumber(portalCertificateData.getCertificateNumber());
        portalcertificatedataId.setTaxYear(portalCertificateData.getTaxYear());
        portalcertificatedataId.setBidRate(portalCertificateData.getBidRate());
        portalcertificatedataId.setDateSold(portalCertificateData.getDateSold());
        portalcertificatedataId.setStatus(portalCertificateData.getStatus());
        portalcertificatedataId.setStatusChangeDate(portalCertificateData.getStatusChangeDate());
        portalcertificatedataId.setBank(portalCertificateData.getBank());
        portalcertificatedataId.setStorageStatus(portalCertificateData.getStorageStatus());
        portalcertificatedataId.setReleasedDate(portalCertificateData.getReleasedDate());
        portalcertificatedataId.setExpirationDate(portalCertificateData.getExpirationDate());
        portalcertificatedataId.setPinOverride(portalCertificateData.getPinOverride());
        portalcertificatedataId.setAddressOverride(portalCertificateData.getAddressOverride());
        portalcertificatedataId.setTaxYearNotices(portalCertificateData.getTaxYearNotices());
        portalcertificatedataId.setCertificatePrincipal(portalCertificateData.getCertificatePrincipal());
        portalcertificatedataId.setSubPrincipal(portalCertificateData.getSubPrincipal());
        portalcertificatedataId.setFeePrincipal(portalCertificateData.getFeePrincipal());
        portalcertificatedataId.setCertificateRevenue(portalCertificateData.getCertificateRevenue());
        portalcertificatedataId.setSubRevenue(portalCertificateData.getSubRevenue());
        portalcertificatedataId.setFeeRevenue(portalCertificateData.getFeeRevenue());
        portalcertificatedataId.setPrincipal(portalCertificateData.getPrincipal());
        portalcertificatedataId.setRevenue(portalCertificateData.getRevenue());
        portalcertificatedataId.setRedemptionAmount(portalCertificateData.getRedemptionAmount());
        portalcertificatedataId.setBalanceDue(portalCertificateData.getBalanceDue());
        portalcertificatedataId.setPaymentInTransit(portalCertificateData.getPaymentInTransit());
        portalcertificatedataId.setDueAfterSettlement(portalCertificateData.getDueAfterSettlement());
        portalcertificatedataId.setMatterNumber(portalCertificateData.getMatterNumber());
        portalcertificatedataId.setFiledDate(portalCertificateData.getFiledDate());
        portalcertificatedataId.setLastWorkDate(portalCertificateData.getLastWorkDate());
        portalcertificatedataId.setInitialCourtDate(portalCertificateData.getInitialCourtDate());
        portalcertificatedataId.setClosedDate(portalCertificateData.getClosedDate());
        portalcertificatedataId.setPin(portalCertificateData.getPin());
        portalcertificatedataId.setVolume(portalCertificateData.getVolume());
        portalcertificatedataId.setHeader(portalCertificateData.getHeader());
        portalcertificatedataId.setCode(portalCertificateData.getCode());
        portalcertificatedataId.setPropertyType(portalCertificateData.getPropertyType());

        return this.wmGenericDao.findById(portalcertificatedataId);
    }

    @Transactional(value = "platformTransactionManager")
	@Override
	public PortalCertificateData delete(PortalCertificateDataId portalcertificatedataId) throws EntityNotFoundException {
        LOGGER.debug("Deleting PortalCertificateData with id: {}", portalcertificatedataId);
        PortalCertificateData deleted = this.wmGenericDao.findById(portalcertificatedataId);
        if (deleted == null) {
            LOGGER.debug("No PortalCertificateData found with id: {}", portalcertificatedataId);
            throw new EntityNotFoundException(String.valueOf(portalcertificatedataId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

	@Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
	public Page<PortalCertificateData> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all PortalCertificateDatas");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
    @Override
    public Page<PortalCertificateData> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all PortalCertificateDatas");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service platform for table PortalCertificateData to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}

