/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.portal.platform.Pdf;
import com.portal.platform.PdfTemplate;


/**
 * ServiceImpl object for domain model class PdfTemplate.
 *
 * @see PdfTemplate
 */
@Service("platform.PdfTemplateService")
@Validated
public class PdfTemplateServiceImpl implements PdfTemplateService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PdfTemplateServiceImpl.class);

    @Lazy
    @Autowired
	@Qualifier("platform.PdfService")
	private PdfService pdfService;

    @Autowired
    @Qualifier("platform.PdfTemplateDao")
    private WMGenericDao<PdfTemplate, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<PdfTemplate, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "platformTransactionManager")
    @Override
	public PdfTemplate create(PdfTemplate pdfTemplate) {
        LOGGER.debug("Creating a new PdfTemplate with information: {}", pdfTemplate);
        PdfTemplate pdfTemplateCreated = this.wmGenericDao.create(pdfTemplate);
        if(pdfTemplateCreated.getPdfs() != null) {
            for(Pdf pdf : pdfTemplateCreated.getPdfs()) {
                pdf.setPdfTemplate(pdfTemplateCreated);
                LOGGER.debug("Creating a new child Pdf with information: {}", pdf);
                pdfService.create(pdf);
            }
        }
        return pdfTemplateCreated;
    }

	@Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
	public PdfTemplate getById(Integer pdftemplateId) throws EntityNotFoundException {
        LOGGER.debug("Finding PdfTemplate by id: {}", pdftemplateId);
        PdfTemplate pdfTemplate = this.wmGenericDao.findById(pdftemplateId);
        if (pdfTemplate == null){
            LOGGER.debug("No PdfTemplate found with id: {}", pdftemplateId);
            throw new EntityNotFoundException(String.valueOf(pdftemplateId));
        }
        return pdfTemplate;
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
	public PdfTemplate findById(Integer pdftemplateId) {
        LOGGER.debug("Finding PdfTemplate by id: {}", pdftemplateId);
        return this.wmGenericDao.findById(pdftemplateId);
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
    @Override
    public PdfTemplate getByName(String name) {
        Map<String, Object> nameMap = new HashMap<>();
        nameMap.put("name", name);

        LOGGER.debug("Finding PdfTemplate by unique keys: {}", nameMap);
        PdfTemplate pdfTemplate = this.wmGenericDao.findByUniqueKey(nameMap);

        if (pdfTemplate == null){
            LOGGER.debug("No PdfTemplate found with given unique key values: {}", nameMap);
            throw new EntityNotFoundException(String.valueOf(nameMap));
        }

        return pdfTemplate;
    }

	@Transactional(rollbackFor = EntityNotFoundException.class, value = "platformTransactionManager")
	@Override
	public PdfTemplate update(PdfTemplate pdfTemplate) throws EntityNotFoundException {
        LOGGER.debug("Updating PdfTemplate with information: {}", pdfTemplate);
        this.wmGenericDao.update(pdfTemplate);

        Integer pdftemplateId = pdfTemplate.getId();

        return this.wmGenericDao.findById(pdftemplateId);
    }

    @Transactional(value = "platformTransactionManager")
	@Override
	public PdfTemplate delete(Integer pdftemplateId) throws EntityNotFoundException {
        LOGGER.debug("Deleting PdfTemplate with id: {}", pdftemplateId);
        PdfTemplate deleted = this.wmGenericDao.findById(pdftemplateId);
        if (deleted == null) {
            LOGGER.debug("No PdfTemplate found with id: {}", pdftemplateId);
            throw new EntityNotFoundException(String.valueOf(pdftemplateId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

	@Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
	public Page<PdfTemplate> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all PdfTemplates");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
    @Override
    public Page<PdfTemplate> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all PdfTemplates");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service platform for table PdfTemplate to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
    @Override
    public Page<Pdf> findAssociatedPdfs(Integer id, Pageable pageable) {
        LOGGER.debug("Fetching all associated pdfs");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("pdfTemplate.id = '" + id + "'");

        return pdfService.findAll(queryBuilder.toString(), pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service PdfService instance
	 */
	protected void setPdfService(PdfService service) {
        this.pdfService = service;
    }

}

