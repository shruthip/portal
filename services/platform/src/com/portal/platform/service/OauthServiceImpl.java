/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.portal.platform.Oauth;


/**
 * ServiceImpl object for domain model class Oauth.
 *
 * @see Oauth
 */
@Service("platform.OauthService")
@Validated
public class OauthServiceImpl implements OauthService {

    private static final Logger LOGGER = LoggerFactory.getLogger(OauthServiceImpl.class);


    @Autowired
    @Qualifier("platform.OauthDao")
    private WMGenericDao<Oauth, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Oauth, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "platformTransactionManager")
    @Override
	public Oauth create(Oauth oauth) {
        LOGGER.debug("Creating a new Oauth with information: {}", oauth);
        Oauth oauthCreated = this.wmGenericDao.create(oauth);
        return oauthCreated;
    }

	@Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
	public Oauth getById(Integer oauthId) throws EntityNotFoundException {
        LOGGER.debug("Finding Oauth by id: {}", oauthId);
        Oauth oauth = this.wmGenericDao.findById(oauthId);
        if (oauth == null){
            LOGGER.debug("No Oauth found with id: {}", oauthId);
            throw new EntityNotFoundException(String.valueOf(oauthId));
        }
        return oauth;
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
	public Oauth findById(Integer oauthId) {
        LOGGER.debug("Finding Oauth by id: {}", oauthId);
        return this.wmGenericDao.findById(oauthId);
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "platformTransactionManager")
	@Override
	public Oauth update(Oauth oauth) throws EntityNotFoundException {
        LOGGER.debug("Updating Oauth with information: {}", oauth);
        this.wmGenericDao.update(oauth);

        Integer oauthId = oauth.getId();

        return this.wmGenericDao.findById(oauthId);
    }

    @Transactional(value = "platformTransactionManager")
	@Override
	public Oauth delete(Integer oauthId) throws EntityNotFoundException {
        LOGGER.debug("Deleting Oauth with id: {}", oauthId);
        Oauth deleted = this.wmGenericDao.findById(oauthId);
        if (deleted == null) {
            LOGGER.debug("No Oauth found with id: {}", oauthId);
            throw new EntityNotFoundException(String.valueOf(oauthId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

	@Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
	public Page<Oauth> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all Oauths");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
    @Override
    public Page<Oauth> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all Oauths");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service platform for table Oauth to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}

