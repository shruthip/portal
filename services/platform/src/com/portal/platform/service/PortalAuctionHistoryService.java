/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.portal.platform.PortalAuctionHistory;
import com.portal.platform.PortalAuctionHistoryId;

/**
 * Service object for domain model class {@link PortalAuctionHistory}.
 */
public interface PortalAuctionHistoryService {

    /**
     * Creates a new PortalAuctionHistory. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on PortalAuctionHistory if any.
     *
     * @param portalAuctionHistory Details of the PortalAuctionHistory to be created; value cannot be null.
     * @return The newly created PortalAuctionHistory.
     */
	PortalAuctionHistory create(@Valid PortalAuctionHistory portalAuctionHistory);


	/**
	 * Returns PortalAuctionHistory by given id if exists.
	 *
	 * @param portalauctionhistoryId The id of the PortalAuctionHistory to get; value cannot be null.
	 * @return PortalAuctionHistory associated with the given portalauctionhistoryId.
     * @throws EntityNotFoundException If no PortalAuctionHistory is found.
	 */
	PortalAuctionHistory getById(PortalAuctionHistoryId portalauctionhistoryId) throws EntityNotFoundException;

    /**
	 * Find and return the PortalAuctionHistory by given id if exists, returns null otherwise.
	 *
	 * @param portalauctionhistoryId The id of the PortalAuctionHistory to get; value cannot be null.
	 * @return PortalAuctionHistory associated with the given portalauctionhistoryId.
	 */
	PortalAuctionHistory findById(PortalAuctionHistoryId portalauctionhistoryId);


	/**
	 * Updates the details of an existing PortalAuctionHistory. It replaces all fields of the existing PortalAuctionHistory with the given portalAuctionHistory.
	 *
     * This method overrides the input field values using Server side or database managed properties defined on PortalAuctionHistory if any.
     *
	 * @param portalAuctionHistory The details of the PortalAuctionHistory to be updated; value cannot be null.
	 * @return The updated PortalAuctionHistory.
	 * @throws EntityNotFoundException if no PortalAuctionHistory is found with given input.
	 */
	PortalAuctionHistory update(@Valid PortalAuctionHistory portalAuctionHistory) throws EntityNotFoundException;

    /**
	 * Deletes an existing PortalAuctionHistory with the given id.
	 *
	 * @param portalauctionhistoryId The id of the PortalAuctionHistory to be deleted; value cannot be null.
	 * @return The deleted PortalAuctionHistory.
	 * @throws EntityNotFoundException if no PortalAuctionHistory found with the given id.
	 */
	PortalAuctionHistory delete(PortalAuctionHistoryId portalauctionhistoryId) throws EntityNotFoundException;

	/**
	 * Find all PortalAuctionHistories matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
	 *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
	 *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching PortalAuctionHistories.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
	 */
    @Deprecated
	Page<PortalAuctionHistory> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
	 * Find all PortalAuctionHistories matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching PortalAuctionHistories.
     *
     * @see Pageable
     * @see Page
	 */
    Page<PortalAuctionHistory> findAll(String query, Pageable pageable);

    /**
	 * Exports all PortalAuctionHistories matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
	 */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

	/**
	 * Retrieve the count of the PortalAuctionHistories in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
	 * @return The count of the PortalAuctionHistory.
	 */
	long count(String query);

	/**
	 * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
	 * @return Paginated data with included fields.

     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
	Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);


}

