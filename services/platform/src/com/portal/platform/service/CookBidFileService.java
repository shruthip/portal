/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.portal.platform.CookBidFile;
import com.portal.platform.CookBidFileId;

/**
 * Service object for domain model class {@link CookBidFile}.
 */
public interface CookBidFileService {

    /**
     * Creates a new CookBidFile. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on CookBidFile if any.
     *
     * @param cookBidFile Details of the CookBidFile to be created; value cannot be null.
     * @return The newly created CookBidFile.
     */
	CookBidFile create(@Valid CookBidFile cookBidFile);


	/**
	 * Returns CookBidFile by given id if exists.
	 *
	 * @param cookbidfileId The id of the CookBidFile to get; value cannot be null.
	 * @return CookBidFile associated with the given cookbidfileId.
     * @throws EntityNotFoundException If no CookBidFile is found.
	 */
	CookBidFile getById(CookBidFileId cookbidfileId) throws EntityNotFoundException;

    /**
	 * Find and return the CookBidFile by given id if exists, returns null otherwise.
	 *
	 * @param cookbidfileId The id of the CookBidFile to get; value cannot be null.
	 * @return CookBidFile associated with the given cookbidfileId.
	 */
	CookBidFile findById(CookBidFileId cookbidfileId);


	/**
	 * Updates the details of an existing CookBidFile. It replaces all fields of the existing CookBidFile with the given cookBidFile.
	 *
     * This method overrides the input field values using Server side or database managed properties defined on CookBidFile if any.
     *
	 * @param cookBidFile The details of the CookBidFile to be updated; value cannot be null.
	 * @return The updated CookBidFile.
	 * @throws EntityNotFoundException if no CookBidFile is found with given input.
	 */
	CookBidFile update(@Valid CookBidFile cookBidFile) throws EntityNotFoundException;

    /**
	 * Deletes an existing CookBidFile with the given id.
	 *
	 * @param cookbidfileId The id of the CookBidFile to be deleted; value cannot be null.
	 * @return The deleted CookBidFile.
	 * @throws EntityNotFoundException if no CookBidFile found with the given id.
	 */
	CookBidFile delete(CookBidFileId cookbidfileId) throws EntityNotFoundException;

	/**
	 * Find all CookBidFiles matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
	 *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
	 *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching CookBidFiles.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
	 */
    @Deprecated
	Page<CookBidFile> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
	 * Find all CookBidFiles matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching CookBidFiles.
     *
     * @see Pageable
     * @see Page
	 */
    Page<CookBidFile> findAll(String query, Pageable pageable);

    /**
	 * Exports all CookBidFiles matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
	 */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

	/**
	 * Retrieve the count of the CookBidFiles in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
	 * @return The count of the CookBidFile.
	 */
	long count(String query);

	/**
	 * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
	 * @return Paginated data with included fields.

     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
	Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);


}

