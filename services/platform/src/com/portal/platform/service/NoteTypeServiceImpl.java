/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.portal.platform.Note;
import com.portal.platform.NoteType;


/**
 * ServiceImpl object for domain model class NoteType.
 *
 * @see NoteType
 */
@Service("platform.NoteTypeService")
@Validated
public class NoteTypeServiceImpl implements NoteTypeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(NoteTypeServiceImpl.class);

    @Lazy
    @Autowired
	@Qualifier("platform.NoteService")
	private NoteService noteService;

    @Autowired
    @Qualifier("platform.NoteTypeDao")
    private WMGenericDao<NoteType, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<NoteType, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "platformTransactionManager")
    @Override
	public NoteType create(NoteType noteType) {
        LOGGER.debug("Creating a new NoteType with information: {}", noteType);
        NoteType noteTypeCreated = this.wmGenericDao.create(noteType);
        if(noteTypeCreated.getNotes() != null) {
            for(Note note : noteTypeCreated.getNotes()) {
                note.setNoteType(noteTypeCreated);
                LOGGER.debug("Creating a new child Note with information: {}", note);
                noteService.create(note);
            }
        }
        return noteTypeCreated;
    }

	@Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
	public NoteType getById(Integer notetypeId) throws EntityNotFoundException {
        LOGGER.debug("Finding NoteType by id: {}", notetypeId);
        NoteType noteType = this.wmGenericDao.findById(notetypeId);
        if (noteType == null){
            LOGGER.debug("No NoteType found with id: {}", notetypeId);
            throw new EntityNotFoundException(String.valueOf(notetypeId));
        }
        return noteType;
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
	public NoteType findById(Integer notetypeId) {
        LOGGER.debug("Finding NoteType by id: {}", notetypeId);
        return this.wmGenericDao.findById(notetypeId);
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "platformTransactionManager")
	@Override
	public NoteType update(NoteType noteType) throws EntityNotFoundException {
        LOGGER.debug("Updating NoteType with information: {}", noteType);
        this.wmGenericDao.update(noteType);

        Integer notetypeId = noteType.getId();

        return this.wmGenericDao.findById(notetypeId);
    }

    @Transactional(value = "platformTransactionManager")
	@Override
	public NoteType delete(Integer notetypeId) throws EntityNotFoundException {
        LOGGER.debug("Deleting NoteType with id: {}", notetypeId);
        NoteType deleted = this.wmGenericDao.findById(notetypeId);
        if (deleted == null) {
            LOGGER.debug("No NoteType found with id: {}", notetypeId);
            throw new EntityNotFoundException(String.valueOf(notetypeId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

	@Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
	public Page<NoteType> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all NoteTypes");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
    @Override
    public Page<NoteType> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all NoteTypes");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service platform for table NoteType to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }

    @Transactional(readOnly = true, value = "platformTransactionManager")
    @Override
    public Page<Note> findAssociatedNotes(Integer id, Pageable pageable) {
        LOGGER.debug("Fetching all associated notes");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("noteType.id = '" + id + "'");

        return noteService.findAll(queryBuilder.toString(), pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service NoteService instance
	 */
	protected void setNoteService(NoteService service) {
        this.noteService = service;
    }

}

