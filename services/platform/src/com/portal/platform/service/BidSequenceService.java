/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.portal.platform.BidSequence;
import com.portal.platform.BidSequenceId;

/**
 * Service object for domain model class {@link BidSequence}.
 */
public interface BidSequenceService {

    /**
     * Creates a new BidSequence. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on BidSequence if any.
     *
     * @param bidSequence Details of the BidSequence to be created; value cannot be null.
     * @return The newly created BidSequence.
     */
	BidSequence create(@Valid BidSequence bidSequence);


	/**
	 * Returns BidSequence by given id if exists.
	 *
	 * @param bidsequenceId The id of the BidSequence to get; value cannot be null.
	 * @return BidSequence associated with the given bidsequenceId.
     * @throws EntityNotFoundException If no BidSequence is found.
	 */
	BidSequence getById(BidSequenceId bidsequenceId) throws EntityNotFoundException;

    /**
	 * Find and return the BidSequence by given id if exists, returns null otherwise.
	 *
	 * @param bidsequenceId The id of the BidSequence to get; value cannot be null.
	 * @return BidSequence associated with the given bidsequenceId.
	 */
	BidSequence findById(BidSequenceId bidsequenceId);


	/**
	 * Updates the details of an existing BidSequence. It replaces all fields of the existing BidSequence with the given bidSequence.
	 *
     * This method overrides the input field values using Server side or database managed properties defined on BidSequence if any.
     *
	 * @param bidSequence The details of the BidSequence to be updated; value cannot be null.
	 * @return The updated BidSequence.
	 * @throws EntityNotFoundException if no BidSequence is found with given input.
	 */
	BidSequence update(@Valid BidSequence bidSequence) throws EntityNotFoundException;

    /**
	 * Deletes an existing BidSequence with the given id.
	 *
	 * @param bidsequenceId The id of the BidSequence to be deleted; value cannot be null.
	 * @return The deleted BidSequence.
	 * @throws EntityNotFoundException if no BidSequence found with the given id.
	 */
	BidSequence delete(BidSequenceId bidsequenceId) throws EntityNotFoundException;

	/**
	 * Find all BidSequences matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
	 *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
	 *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching BidSequences.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
	 */
    @Deprecated
	Page<BidSequence> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
	 * Find all BidSequences matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching BidSequences.
     *
     * @see Pageable
     * @see Page
	 */
    Page<BidSequence> findAll(String query, Pageable pageable);

    /**
	 * Exports all BidSequences matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
	 */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

	/**
	 * Retrieve the count of the BidSequences in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
	 * @return The count of the BidSequence.
	 */
	long count(String query);

	/**
	 * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
	 * @return Paginated data with included fields.

     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
	Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);


}

