/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.models.query;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpdateUserNameRequest implements Serializable {


    @JsonProperty("user_name")
    @NotNull
    private String userName;

    @JsonProperty("user_ID")
    @NotNull
    private Integer userId;

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getUserId() {
        return this.userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UpdateUserNameRequest)) return false;
        final UpdateUserNameRequest updateUserNameRequest = (UpdateUserNameRequest) o;
        return Objects.equals(getUserName(), updateUserNameRequest.getUserName()) &&
                Objects.equals(getUserId(), updateUserNameRequest.getUserId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUserName(),
                getUserId());
    }
}
