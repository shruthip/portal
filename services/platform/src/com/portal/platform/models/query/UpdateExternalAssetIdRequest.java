/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.models.query;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpdateExternalAssetIdRequest implements Serializable {


    @JsonProperty("external_asset_id")
    @NotNull
    private Integer externalAssetId;

    @JsonProperty("asset_id")
    @NotNull
    private Integer assetId;

    public Integer getExternalAssetId() {
        return this.externalAssetId;
    }

    public void setExternalAssetId(Integer externalAssetId) {
        this.externalAssetId = externalAssetId;
    }

    public Integer getAssetId() {
        return this.assetId;
    }

    public void setAssetId(Integer assetId) {
        this.assetId = assetId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UpdateExternalAssetIdRequest)) return false;
        final UpdateExternalAssetIdRequest updateExternalAssetIdRequest = (UpdateExternalAssetIdRequest) o;
        return Objects.equals(getExternalAssetId(), updateExternalAssetIdRequest.getExternalAssetId()) &&
                Objects.equals(getAssetId(), updateExternalAssetIdRequest.getAssetId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getExternalAssetId(),
                getAssetId());
    }
}
