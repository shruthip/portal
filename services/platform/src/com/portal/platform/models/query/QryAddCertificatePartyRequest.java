/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.models.query;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QryAddCertificatePartyRequest implements Serializable {


    @JsonProperty("certificate_id")
    private Integer certificateId;

    @JsonProperty("party_id")
    private Integer partyId;

    @JsonProperty("interest_type1")
    private String interestType1;

    @JsonProperty("interest_type2")
    private String interestType2;

    @JsonProperty("interest_type3")
    private String interestType3;

    @JsonProperty("service_type")
    private String serviceType;

    public Integer getCertificateId() {
        return this.certificateId;
    }

    public void setCertificateId(Integer certificateId) {
        this.certificateId = certificateId;
    }

    public Integer getPartyId() {
        return this.partyId;
    }

    public void setPartyId(Integer partyId) {
        this.partyId = partyId;
    }

    public String getInterestType1() {
        return this.interestType1;
    }

    public void setInterestType1(String interestType1) {
        this.interestType1 = interestType1;
    }

    public String getInterestType2() {
        return this.interestType2;
    }

    public void setInterestType2(String interestType2) {
        this.interestType2 = interestType2;
    }

    public String getInterestType3() {
        return this.interestType3;
    }

    public void setInterestType3(String interestType3) {
        this.interestType3 = interestType3;
    }

    public String getServiceType() {
        return this.serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof QryAddCertificatePartyRequest)) return false;
        final QryAddCertificatePartyRequest qryAddCertificatePartyRequest = (QryAddCertificatePartyRequest) o;
        return Objects.equals(getCertificateId(), qryAddCertificatePartyRequest.getCertificateId()) &&
                Objects.equals(getPartyId(), qryAddCertificatePartyRequest.getPartyId()) &&
                Objects.equals(getInterestType1(), qryAddCertificatePartyRequest.getInterestType1()) &&
                Objects.equals(getInterestType2(), qryAddCertificatePartyRequest.getInterestType2()) &&
                Objects.equals(getInterestType3(), qryAddCertificatePartyRequest.getInterestType3()) &&
                Objects.equals(getServiceType(), qryAddCertificatePartyRequest.getServiceType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCertificateId(),
                getPartyId(),
                getInterestType1(),
                getInterestType2(),
                getInterestType3(),
                getServiceType());
    }
}
