/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.models.query;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wavemaker.runtime.data.annotations.ColumnAlias;

public class QryPortalAuctionResultByPropertyIdResponse implements Serializable {


    @JsonProperty("auction_id")
    @ColumnAlias("auction_id")
    private Integer auctionId;

    @JsonProperty("auction_year_billed")
    @ColumnAlias("auction_year_billed")
    private String auctionYearBilled;

    @JsonProperty("auction_year_due")
    @ColumnAlias("auction_year_due")
    private BigDecimal auctionYearDue;

    @JsonProperty("buyer")
    @ColumnAlias("buyer")
    private String buyer;

    @JsonProperty("buyer_id")
    @ColumnAlias("buyer_id")
    private Integer buyerId;

    @JsonProperty("disposition_date")
    @ColumnAlias("disposition_date")
    private Date dispositionDate;

    @JsonProperty("disposition_research_date")
    @ColumnAlias("disposition_research_date")
    private Date dispositionResearchDate;

    @JsonProperty("id")
    @ColumnAlias("id")
    private Integer id;

    @JsonProperty("prior_years_due")
    @ColumnAlias("prior_years_due")
    private BigDecimal priorYearsDue;

    @JsonProperty("sale_amount")
    @ColumnAlias("sale_amount")
    private BigDecimal saleAmount;

    @JsonProperty("sale_date")
    @ColumnAlias("sale_date")
    private Date saleDate;

    @JsonProperty("status")
    @ColumnAlias("status")
    private String status;

    @JsonProperty("tax_year")
    @ColumnAlias("tax_year")
    private Integer taxYear;

    public Integer getAuctionId() {
        return this.auctionId;
    }

    public void setAuctionId(Integer auctionId) {
        this.auctionId = auctionId;
    }

    public String getAuctionYearBilled() {
        return this.auctionYearBilled;
    }

    public void setAuctionYearBilled(String auctionYearBilled) {
        this.auctionYearBilled = auctionYearBilled;
    }

    public BigDecimal getAuctionYearDue() {
        return this.auctionYearDue;
    }

    public void setAuctionYearDue(BigDecimal auctionYearDue) {
        this.auctionYearDue = auctionYearDue;
    }

    public String getBuyer() {
        return this.buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public Integer getBuyerId() {
        return this.buyerId;
    }

    public void setBuyerId(Integer buyerId) {
        this.buyerId = buyerId;
    }

    public Date getDispositionDate() {
        return this.dispositionDate;
    }

    public void setDispositionDate(Date dispositionDate) {
        this.dispositionDate = dispositionDate;
    }

    public Date getDispositionResearchDate() {
        return this.dispositionResearchDate;
    }

    public void setDispositionResearchDate(Date dispositionResearchDate) {
        this.dispositionResearchDate = dispositionResearchDate;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getPriorYearsDue() {
        return this.priorYearsDue;
    }

    public void setPriorYearsDue(BigDecimal priorYearsDue) {
        this.priorYearsDue = priorYearsDue;
    }

    public BigDecimal getSaleAmount() {
        return this.saleAmount;
    }

    public void setSaleAmount(BigDecimal saleAmount) {
        this.saleAmount = saleAmount;
    }

    public Date getSaleDate() {
        return this.saleDate;
    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getTaxYear() {
        return this.taxYear;
    }

    public void setTaxYear(Integer taxYear) {
        this.taxYear = taxYear;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof QryPortalAuctionResultByPropertyIdResponse)) return false;
        final QryPortalAuctionResultByPropertyIdResponse qryPortalAuctionResultByPropertyIdResponse = (QryPortalAuctionResultByPropertyIdResponse) o;
        return Objects.equals(getAuctionId(), qryPortalAuctionResultByPropertyIdResponse.getAuctionId()) &&
                Objects.equals(getAuctionYearBilled(), qryPortalAuctionResultByPropertyIdResponse.getAuctionYearBilled()) &&
                Objects.equals(getAuctionYearDue(), qryPortalAuctionResultByPropertyIdResponse.getAuctionYearDue()) &&
                Objects.equals(getBuyer(), qryPortalAuctionResultByPropertyIdResponse.getBuyer()) &&
                Objects.equals(getBuyerId(), qryPortalAuctionResultByPropertyIdResponse.getBuyerId()) &&
                Objects.equals(getDispositionDate(), qryPortalAuctionResultByPropertyIdResponse.getDispositionDate()) &&
                Objects.equals(getDispositionResearchDate(), qryPortalAuctionResultByPropertyIdResponse.getDispositionResearchDate()) &&
                Objects.equals(getId(), qryPortalAuctionResultByPropertyIdResponse.getId()) &&
                Objects.equals(getPriorYearsDue(), qryPortalAuctionResultByPropertyIdResponse.getPriorYearsDue()) &&
                Objects.equals(getSaleAmount(), qryPortalAuctionResultByPropertyIdResponse.getSaleAmount()) &&
                Objects.equals(getSaleDate(), qryPortalAuctionResultByPropertyIdResponse.getSaleDate()) &&
                Objects.equals(getStatus(), qryPortalAuctionResultByPropertyIdResponse.getStatus()) &&
                Objects.equals(getTaxYear(), qryPortalAuctionResultByPropertyIdResponse.getTaxYear());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAuctionId(),
                getAuctionYearBilled(),
                getAuctionYearDue(),
                getBuyer(),
                getBuyerId(),
                getDispositionDate(),
                getDispositionResearchDate(),
                getId(),
                getPriorYearsDue(),
                getSaleAmount(),
                getSaleDate(),
                getStatus(),
                getTaxYear());
    }
}
