/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.models.query;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QryUpdateMatterClosedRequest implements Serializable {


    @JsonProperty("selected_matter_id")
    private Integer selectedMatterId;

    public Integer getSelectedMatterId() {
        return this.selectedMatterId;
    }

    public void setSelectedMatterId(Integer selectedMatterId) {
        this.selectedMatterId = selectedMatterId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof QryUpdateMatterClosedRequest)) return false;
        final QryUpdateMatterClosedRequest qryUpdateMatterClosedRequest = (QryUpdateMatterClosedRequest) o;
        return Objects.equals(getSelectedMatterId(), qryUpdateMatterClosedRequest.getSelectedMatterId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSelectedMatterId());
    }
}
