/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.models.query;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpdateGradeRequest implements Serializable {


    @JsonProperty("grade")
    private String grade;

    @JsonProperty("note")
    private String note;

    @JsonProperty("grade_id")
    private Integer gradeId;

    public String getGrade() {
        return this.grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getGradeId() {
        return this.gradeId;
    }

    public void setGradeId(Integer gradeId) {
        this.gradeId = gradeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UpdateGradeRequest)) return false;
        final UpdateGradeRequest updateGradeRequest = (UpdateGradeRequest) o;
        return Objects.equals(getGrade(), updateGradeRequest.getGrade()) &&
                Objects.equals(getNote(), updateGradeRequest.getNote()) &&
                Objects.equals(getGradeId(), updateGradeRequest.getGradeId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getGrade(),
                getNote(),
                getGradeId());
    }
}
