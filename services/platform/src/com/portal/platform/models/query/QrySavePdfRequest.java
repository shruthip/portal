/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.models.query;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QrySavePdfRequest implements Serializable {


    @JsonProperty("tempate_id")
    private Integer tempateId;

    @JsonProperty("url")
    private String url;

    @JsonProperty("certificate_ids")
    private String certificateIds;

    public Integer getTempateId() {
        return this.tempateId;
    }

    public void setTempateId(Integer tempateId) {
        this.tempateId = tempateId;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCertificateIds() {
        return this.certificateIds;
    }

    public void setCertificateIds(String certificateIds) {
        this.certificateIds = certificateIds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof QrySavePdfRequest)) return false;
        final QrySavePdfRequest qrySavePdfRequest = (QrySavePdfRequest) o;
        return Objects.equals(getTempateId(), qrySavePdfRequest.getTempateId()) &&
                Objects.equals(getUrl(), qrySavePdfRequest.getUrl()) &&
                Objects.equals(getCertificateIds(), qrySavePdfRequest.getCertificateIds());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTempateId(),
                getUrl(),
                getCertificateIds());
    }
}
