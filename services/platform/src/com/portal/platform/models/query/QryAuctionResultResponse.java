/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.models.query;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Objects;

import com.wavemaker.runtime.data.annotations.ColumnAlias;

public class QryAuctionResultResponse implements Serializable {


    @ColumnAlias("auctionId")
    private Integer auctionId;

    @ColumnAlias("tax_year")
    private Integer taxYear;

    @ColumnAlias("auction_year_billed")
    private Double auctionYearBilled;

    @ColumnAlias("auction_year_due")
    private Double auctionYearDue;

    @ColumnAlias("prior_years_due")
    private Double priorYearsDue;

    @ColumnAlias("sale_amount")
    private Double saleAmount;

    @ColumnAlias("sale_date")
    private Date saleDate;

    @ColumnAlias("bid_rate")
    private BigDecimal bidRate;

    @ColumnAlias("buyer_name")
    private String buyerName;

    @ColumnAlias("showRecord")
    private String showRecord;

    @ColumnAlias("status")
    private String status;

    @ColumnAlias("disposition_date")
    private Date dispositionDate;

    @ColumnAlias("disposition_research_date")
    private Date dispositionResearchDate;

    public Integer getAuctionId() {
        return this.auctionId;
    }

    public void setAuctionId(Integer auctionId) {
        this.auctionId = auctionId;
    }

    public Integer getTaxYear() {
        return this.taxYear;
    }

    public void setTaxYear(Integer taxYear) {
        this.taxYear = taxYear;
    }

    public Double getAuctionYearBilled() {
        return this.auctionYearBilled;
    }

    public void setAuctionYearBilled(Double auctionYearBilled) {
        this.auctionYearBilled = auctionYearBilled;
    }

    public Double getAuctionYearDue() {
        return this.auctionYearDue;
    }

    public void setAuctionYearDue(Double auctionYearDue) {
        this.auctionYearDue = auctionYearDue;
    }

    public Double getPriorYearsDue() {
        return this.priorYearsDue;
    }

    public void setPriorYearsDue(Double priorYearsDue) {
        this.priorYearsDue = priorYearsDue;
    }

    public Double getSaleAmount() {
        return this.saleAmount;
    }

    public void setSaleAmount(Double saleAmount) {
        this.saleAmount = saleAmount;
    }

    public Date getSaleDate() {
        return this.saleDate;
    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

    public BigDecimal getBidRate() {
        return this.bidRate;
    }

    public void setBidRate(BigDecimal bidRate) {
        this.bidRate = bidRate;
    }

    public String getBuyerName() {
        return this.buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getShowRecord() {
        return this.showRecord;
    }

    public void setShowRecord(String showRecord) {
        this.showRecord = showRecord;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDispositionDate() {
        return this.dispositionDate;
    }

    public void setDispositionDate(Date dispositionDate) {
        this.dispositionDate = dispositionDate;
    }

    public Date getDispositionResearchDate() {
        return this.dispositionResearchDate;
    }

    public void setDispositionResearchDate(Date dispositionResearchDate) {
        this.dispositionResearchDate = dispositionResearchDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof QryAuctionResultResponse)) return false;
        final QryAuctionResultResponse qryAuctionResultResponse = (QryAuctionResultResponse) o;
        return Objects.equals(getAuctionId(), qryAuctionResultResponse.getAuctionId()) &&
                Objects.equals(getTaxYear(), qryAuctionResultResponse.getTaxYear()) &&
                Objects.equals(getAuctionYearBilled(), qryAuctionResultResponse.getAuctionYearBilled()) &&
                Objects.equals(getAuctionYearDue(), qryAuctionResultResponse.getAuctionYearDue()) &&
                Objects.equals(getPriorYearsDue(), qryAuctionResultResponse.getPriorYearsDue()) &&
                Objects.equals(getSaleAmount(), qryAuctionResultResponse.getSaleAmount()) &&
                Objects.equals(getSaleDate(), qryAuctionResultResponse.getSaleDate()) &&
                Objects.equals(getBidRate(), qryAuctionResultResponse.getBidRate()) &&
                Objects.equals(getBuyerName(), qryAuctionResultResponse.getBuyerName()) &&
                Objects.equals(getShowRecord(), qryAuctionResultResponse.getShowRecord()) &&
                Objects.equals(getStatus(), qryAuctionResultResponse.getStatus()) &&
                Objects.equals(getDispositionDate(), qryAuctionResultResponse.getDispositionDate()) &&
                Objects.equals(getDispositionResearchDate(), qryAuctionResultResponse.getDispositionResearchDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAuctionId(),
                getTaxYear(),
                getAuctionYearBilled(),
                getAuctionYearDue(),
                getPriorYearsDue(),
                getSaleAmount(),
                getSaleDate(),
                getBidRate(),
                getBuyerName(),
                getShowRecord(),
                getStatus(),
                getDispositionDate(),
                getDispositionResearchDate());
    }
}
