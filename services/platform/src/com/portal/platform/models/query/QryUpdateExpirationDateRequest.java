/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.models.query;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QryUpdateExpirationDateRequest implements Serializable {


    @JsonProperty("expiration_date")
    private Date expirationDate;

    @JsonProperty("selected_certificate_id")
    private Integer selectedCertificateId;

    public Date getExpirationDate() {
        return this.expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Integer getSelectedCertificateId() {
        return this.selectedCertificateId;
    }

    public void setSelectedCertificateId(Integer selectedCertificateId) {
        this.selectedCertificateId = selectedCertificateId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof QryUpdateExpirationDateRequest)) return false;
        final QryUpdateExpirationDateRequest qryUpdateExpirationDateRequest = (QryUpdateExpirationDateRequest) o;
        return Objects.equals(getExpirationDate(), qryUpdateExpirationDateRequest.getExpirationDate()) &&
                Objects.equals(getSelectedCertificateId(), qryUpdateExpirationDateRequest.getSelectedCertificateId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getExpirationDate(),
                getSelectedCertificateId());
    }
}
