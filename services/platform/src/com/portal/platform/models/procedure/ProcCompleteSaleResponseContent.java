/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.models.procedure;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wavemaker.runtime.data.annotations.ColumnAlias;

public class ProcCompleteSaleResponseContent implements Serializable {


    @JsonProperty("count_records")
    @ColumnAlias("count_records")
    private Integer countRecords;

    public Integer getCountRecords() {
        return this.countRecords;
    }

    public void setCountRecords(Integer countRecords) {
        this.countRecords = countRecords;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProcCompleteSaleResponseContent)) return false;
        final ProcCompleteSaleResponseContent procCompleteSaleResponseContent = (ProcCompleteSaleResponseContent) o;
        return Objects.equals(getCountRecords(), procCompleteSaleResponseContent.getCountRecords());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCountRecords());
    }
}
