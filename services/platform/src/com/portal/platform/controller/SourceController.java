/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.portal.platform.Party;
import com.portal.platform.Property;
import com.portal.platform.Source;
import com.portal.platform.service.SourceService;


/**
 * Controller object for domain model class Source.
 * @see Source
 */
@RestController("platform.SourceController")
@Api(value = "SourceController", description = "Exposes APIs to work with Source resource.")
@RequestMapping("/platform/Source")
public class SourceController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SourceController.class);

    @Autowired
	@Qualifier("platform.SourceService")
	private SourceService sourceService;

	@ApiOperation(value = "Creates a new Source instance.")
@RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
public Source createSource(@RequestBody Source source) {
		LOGGER.debug("Create Source with information: {}" , source);

		source = sourceService.create(source);
		LOGGER.debug("Created Source with information: {}" , source);

	    return source;
	}

    @ApiOperation(value = "Returns the Source instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Source getSource(@PathVariable("id") String id) throws EntityNotFoundException {
        LOGGER.debug("Getting Source with id: {}" , id);

        Source foundSource = sourceService.getById(id);
        LOGGER.debug("Source details with id: {}" , foundSource);

        return foundSource;
    }

    @ApiOperation(value = "Updates the Source instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Source editSource(@PathVariable("id") String id, @RequestBody Source source) throws EntityNotFoundException {
        LOGGER.debug("Editing Source with id: {}" , source.getCode());

        source.setCode(id);
        source = sourceService.update(source);
        LOGGER.debug("Source details with id: {}" , source);

        return source;
    }

    @ApiOperation(value = "Deletes the Source instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteSource(@PathVariable("id") String id) throws EntityNotFoundException {
        LOGGER.debug("Deleting Source with id: {}" , id);

        Source deletedSource = sourceService.delete(id);

        return deletedSource != null;
    }

    @RequestMapping(value = "/code-name", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the matching Source with given unique key values.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Source getByCodeAndName(@RequestParam("code") String code, @RequestParam("name") String name) {
        LOGGER.debug("Getting Source with uniques key CodeAndName");
        return sourceService.getByCodeAndName(code, name);
    }

    /**
     * @deprecated Use {@link #findSources(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of Source instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Source> searchSourcesByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering Sources list");
        return sourceService.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Source instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Source> findSources(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Sources list");
        return sourceService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Source instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Source> filterSources(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Sources list");
        return sourceService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportSources(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return sourceService.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of Source instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countSources( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting Sources");
		return sourceService.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getSourceAggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return sourceService.getAggregatedValues(aggregationInfo, pageable);
    }

    @RequestMapping(value="/{id:.+}/propertiesForAddressSource1", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the propertiesForAddressSource1 instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Property> findAssociatedPropertiesForAddressSource1(@PathVariable("id") String id, Pageable pageable) {

        LOGGER.debug("Fetching all associated propertiesForAddressSource1");
        return sourceService.findAssociatedPropertiesForAddressSource1(id, pageable);
    }

    @RequestMapping(value="/{id:.+}/propertiesForAddressSource2", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the propertiesForAddressSource2 instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Property> findAssociatedPropertiesForAddressSource2(@PathVariable("id") String id, Pageable pageable) {

        LOGGER.debug("Fetching all associated propertiesForAddressSource2");
        return sourceService.findAssociatedPropertiesForAddressSource2(id, pageable);
    }

    @RequestMapping(value="/{id:.+}/partiesForSource1", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the partiesForSource1 instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Party> findAssociatedPartiesForSource1(@PathVariable("id") String id, Pageable pageable) {

        LOGGER.debug("Fetching all associated partiesForSource1");
        return sourceService.findAssociatedPartiesForSource1(id, pageable);
    }

    @RequestMapping(value="/{id:.+}/partiesForSource2", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the partiesForSource2 instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Party> findAssociatedPartiesForSource2(@PathVariable("id") String id, Pageable pageable) {

        LOGGER.debug("Fetching all associated partiesForSource2");
        return sourceService.findAssociatedPartiesForSource2(id, pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service SourceService instance
	 */
	protected void setSourceService(SourceService service) {
		this.sourceService = service;
	}

}

