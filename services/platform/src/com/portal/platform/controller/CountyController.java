/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.portal.platform.Auction;
import com.portal.platform.Certificate;
import com.portal.platform.County;
import com.portal.platform.Grade;
import com.portal.platform.Property;
import com.portal.platform.PropertyClass;
import com.portal.platform.TaxSchedule;
import com.portal.platform.Township;
import com.portal.platform.service.CountyService;


/**
 * Controller object for domain model class County.
 * @see County
 */
@RestController("platform.CountyController")
@Api(value = "CountyController", description = "Exposes APIs to work with County resource.")
@RequestMapping("/platform/County")
public class CountyController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CountyController.class);

    @Autowired
	@Qualifier("platform.CountyService")
	private CountyService countyService;

	@ApiOperation(value = "Creates a new County instance.")
@RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
public County createCounty(@RequestBody County county) {
		LOGGER.debug("Create County with information: {}" , county);

		county = countyService.create(county);
		LOGGER.debug("Created County with information: {}" , county);

	    return county;
	}

    @ApiOperation(value = "Returns the County instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public County getCounty(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting County with id: {}" , id);

        County foundCounty = countyService.getById(id);
        LOGGER.debug("County details with id: {}" , foundCounty);

        return foundCounty;
    }

    @ApiOperation(value = "Updates the County instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public County editCounty(@PathVariable("id") Integer id, @RequestBody County county) throws EntityNotFoundException {
        LOGGER.debug("Editing County with id: {}" , county.getId());

        county.setId(id);
        county = countyService.update(county);
        LOGGER.debug("County details with id: {}" , county);

        return county;
    }

    @ApiOperation(value = "Deletes the County instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteCounty(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting County with id: {}" , id);

        County deletedCounty = countyService.delete(id);

        return deletedCounty != null;
    }

    /**
     * @deprecated Use {@link #findCounties(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of County instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<County> searchCountiesByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering Counties list");
        return countyService.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of County instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<County> findCounties(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Counties list");
        return countyService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of County instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<County> filterCounties(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Counties list");
        return countyService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportCounties(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return countyService.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of County instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countCounties( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting Counties");
		return countyService.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getCountyAggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return countyService.getAggregatedValues(aggregationInfo, pageable);
    }

    @RequestMapping(value="/{id:.+}/auctions", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the auctions instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Auction> findAssociatedAuctions(@PathVariable("id") Integer id, Pageable pageable) {

        LOGGER.debug("Fetching all associated auctions");
        return countyService.findAssociatedAuctions(id, pageable);
    }

    @RequestMapping(value="/{id:.+}/properties", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the properties instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Property> findAssociatedProperties(@PathVariable("id") Integer id, Pageable pageable) {

        LOGGER.debug("Fetching all associated properties");
        return countyService.findAssociatedProperties(id, pageable);
    }

    @RequestMapping(value="/{id:.+}/propertyClasses", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the propertyClasses instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<PropertyClass> findAssociatedPropertyClasses(@PathVariable("id") Integer id, Pageable pageable) {

        LOGGER.debug("Fetching all associated propertyClasses");
        return countyService.findAssociatedPropertyClasses(id, pageable);
    }

    @RequestMapping(value="/{id:.+}/grades", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the grades instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Grade> findAssociatedGrades(@PathVariable("id") Integer id, Pageable pageable) {

        LOGGER.debug("Fetching all associated grades");
        return countyService.findAssociatedGrades(id, pageable);
    }

    @RequestMapping(value="/{id:.+}/taxSchedules", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the taxSchedules instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<TaxSchedule> findAssociatedTaxSchedules(@PathVariable("id") Integer id, Pageable pageable) {

        LOGGER.debug("Fetching all associated taxSchedules");
        return countyService.findAssociatedTaxSchedules(id, pageable);
    }

    @RequestMapping(value="/{id:.+}/townships", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the townships instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Township> findAssociatedTownships(@PathVariable("id") Integer id, Pageable pageable) {

        LOGGER.debug("Fetching all associated townships");
        return countyService.findAssociatedTownships(id, pageable);
    }

    @RequestMapping(value="/{id:.+}/certificates", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the certificates instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Certificate> findAssociatedCertificates(@PathVariable("id") Integer id, Pageable pageable) {

        LOGGER.debug("Fetching all associated certificates");
        return countyService.findAssociatedCertificates(id, pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service CountyService instance
	 */
	protected void setCountyService(CountyService service) {
		this.countyService = service;
	}

}

