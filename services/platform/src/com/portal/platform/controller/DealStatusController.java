/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.portal.platform.Deal;
import com.portal.platform.DealStatus;
import com.portal.platform.service.DealStatusService;


/**
 * Controller object for domain model class DealStatus.
 * @see DealStatus
 */
@RestController("platform.DealStatusController")
@Api(value = "DealStatusController", description = "Exposes APIs to work with DealStatus resource.")
@RequestMapping("/platform/DealStatus")
public class DealStatusController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DealStatusController.class);

    @Autowired
	@Qualifier("platform.DealStatusService")
	private DealStatusService dealStatusService;

	@ApiOperation(value = "Creates a new DealStatus instance.")
@RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
public DealStatus createDealStatus(@RequestBody DealStatus dealStatus) {
		LOGGER.debug("Create DealStatus with information: {}" , dealStatus);

		dealStatus = dealStatusService.create(dealStatus);
		LOGGER.debug("Created DealStatus with information: {}" , dealStatus);

	    return dealStatus;
	}

    @ApiOperation(value = "Returns the DealStatus instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public DealStatus getDealStatus(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting DealStatus with id: {}" , id);

        DealStatus foundDealStatus = dealStatusService.getById(id);
        LOGGER.debug("DealStatus details with id: {}" , foundDealStatus);

        return foundDealStatus;
    }

    @ApiOperation(value = "Updates the DealStatus instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public DealStatus editDealStatus(@PathVariable("id") Integer id, @RequestBody DealStatus dealStatus) throws EntityNotFoundException {
        LOGGER.debug("Editing DealStatus with id: {}" , dealStatus.getId());

        dealStatus.setId(id);
        dealStatus = dealStatusService.update(dealStatus);
        LOGGER.debug("DealStatus details with id: {}" , dealStatus);

        return dealStatus;
    }

    @ApiOperation(value = "Deletes the DealStatus instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteDealStatus(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting DealStatus with id: {}" , id);

        DealStatus deletedDealStatus = dealStatusService.delete(id);

        return deletedDealStatus != null;
    }

    @RequestMapping(value = "/status/{status}", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the matching DealStatus with given unique key values.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public DealStatus getByStatus(@PathVariable("status") String status) {
        LOGGER.debug("Getting DealStatus with uniques key Status");
        return dealStatusService.getByStatus(status);
    }

    /**
     * @deprecated Use {@link #findDealStatuses(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of DealStatus instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<DealStatus> searchDealStatusesByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering DealStatuses list");
        return dealStatusService.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of DealStatus instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<DealStatus> findDealStatuses(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering DealStatuses list");
        return dealStatusService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of DealStatus instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<DealStatus> filterDealStatuses(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering DealStatuses list");
        return dealStatusService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportDealStatuses(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return dealStatusService.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of DealStatus instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countDealStatuses( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting DealStatuses");
		return dealStatusService.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getDealStatusAggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return dealStatusService.getAggregatedValues(aggregationInfo, pageable);
    }

    @RequestMapping(value="/{id:.+}/deals", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the deals instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Deal> findAssociatedDeals(@PathVariable("id") Integer id, Pageable pageable) {

        LOGGER.debug("Fetching all associated deals");
        return dealStatusService.findAssociatedDeals(id, pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service DealStatusService instance
	 */
	protected void setDealStatusService(DealStatusService service) {
		this.dealStatusService = service;
	}

}

