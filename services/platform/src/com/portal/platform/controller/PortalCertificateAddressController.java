/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.sql.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.portal.platform.PortalCertificateAddress;
import com.portal.platform.PortalCertificateAddressId;
import com.portal.platform.service.PortalCertificateAddressService;


/**
 * Controller object for domain model class PortalCertificateAddress.
 * @see PortalCertificateAddress
 */
@RestController("platform.PortalCertificateAddressController")
@Api(value = "PortalCertificateAddressController", description = "Exposes APIs to work with PortalCertificateAddress resource.")
@RequestMapping("/platform/PortalCertificateAddress")
public class PortalCertificateAddressController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PortalCertificateAddressController.class);

    @Autowired
	@Qualifier("platform.PortalCertificateAddressService")
	private PortalCertificateAddressService portalCertificateAddressService;

	@ApiOperation(value = "Creates a new PortalCertificateAddress instance.")
@RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
public PortalCertificateAddress createPortalCertificateAddress(@RequestBody PortalCertificateAddress portalCertificateAddress) {
		LOGGER.debug("Create PortalCertificateAddress with information: {}" , portalCertificateAddress);

		portalCertificateAddress = portalCertificateAddressService.create(portalCertificateAddress);
		LOGGER.debug("Created PortalCertificateAddress with information: {}" , portalCertificateAddress);

	    return portalCertificateAddress;
	}

@ApiOperation(value = "Returns the PortalCertificateAddress instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public PortalCertificateAddress getPortalCertificateAddress(@RequestParam("countyName") String countyName,@RequestParam("certificateId") Integer certificateId,@RequestParam("bank") String bank,@RequestParam("taxYear") Integer taxYear,@RequestParam("volume") Integer volume,@RequestParam("pin") String pin,@RequestParam("certificateNumber") String certificateNumber,@RequestParam("dateSold") Date dateSold,@RequestParam("expirationDate") Date expirationDate,@RequestParam("status") String status,@RequestParam("taxYearNotices") String taxYearNotices,@RequestParam("addressLine1") String addressLine1,@RequestParam("addressLine2") String addressLine2,@RequestParam("addressCity") String addressCity,@RequestParam("addressZip") String addressZip,@RequestParam("addressVerified") Boolean addressVerified,@RequestParam("addressSource1") String addressSource1,@RequestParam("addressSource2") String addressSource2,@RequestParam("assesseeName") String assesseeName,@RequestParam("assesseeAddressLine1") String assesseeAddressLine1,@RequestParam("assesseeAddressCity") String assesseeAddressCity,@RequestParam("assesseeAddressState") String assesseeAddressState,@RequestParam("assesseeAddressZip") String assesseeAddressZip,@RequestParam("code") String code,@RequestParam("zoning") String zoning,@RequestParam("improvementLevel") String improvementLevel,@RequestParam("certificatePrincipal") Double certificatePrincipal,@RequestParam("subPrincipal") Double subPrincipal,@RequestParam("feePrincipal") Double feePrincipal,@RequestParam("principal") Double principal,@RequestParam("certificateRevenue") Double certificateRevenue,@RequestParam("subRevenue") Double subRevenue,@RequestParam("feeRevenue") Double feeRevenue,@RequestParam("revenue") Double revenue,@RequestParam("redemptionAmount") Double redemptionAmount,@RequestParam("amountPaid") Double amountPaid) throws EntityNotFoundException {

        PortalCertificateAddressId portalcertificateaddressId = new PortalCertificateAddressId();
        portalcertificateaddressId.setCountyName(countyName);
        portalcertificateaddressId.setCertificateId(certificateId);
        portalcertificateaddressId.setBank(bank);
        portalcertificateaddressId.setTaxYear(taxYear);
        portalcertificateaddressId.setVolume(volume);
        portalcertificateaddressId.setPin(pin);
        portalcertificateaddressId.setCertificateNumber(certificateNumber);
        portalcertificateaddressId.setDateSold(dateSold);
        portalcertificateaddressId.setExpirationDate(expirationDate);
        portalcertificateaddressId.setStatus(status);
        portalcertificateaddressId.setTaxYearNotices(taxYearNotices);
        portalcertificateaddressId.setAddressLine1(addressLine1);
        portalcertificateaddressId.setAddressLine2(addressLine2);
        portalcertificateaddressId.setAddressCity(addressCity);
        portalcertificateaddressId.setAddressZip(addressZip);
        portalcertificateaddressId.setAddressVerified(addressVerified);
        portalcertificateaddressId.setAddressSource1(addressSource1);
        portalcertificateaddressId.setAddressSource2(addressSource2);
        portalcertificateaddressId.setAssesseeName(assesseeName);
        portalcertificateaddressId.setAssesseeAddressLine1(assesseeAddressLine1);
        portalcertificateaddressId.setAssesseeAddressCity(assesseeAddressCity);
        portalcertificateaddressId.setAssesseeAddressState(assesseeAddressState);
        portalcertificateaddressId.setAssesseeAddressZip(assesseeAddressZip);
        portalcertificateaddressId.setCode(code);
        portalcertificateaddressId.setZoning(zoning);
        portalcertificateaddressId.setImprovementLevel(improvementLevel);
        portalcertificateaddressId.setCertificatePrincipal(certificatePrincipal);
        portalcertificateaddressId.setSubPrincipal(subPrincipal);
        portalcertificateaddressId.setFeePrincipal(feePrincipal);
        portalcertificateaddressId.setPrincipal(principal);
        portalcertificateaddressId.setCertificateRevenue(certificateRevenue);
        portalcertificateaddressId.setSubRevenue(subRevenue);
        portalcertificateaddressId.setFeeRevenue(feeRevenue);
        portalcertificateaddressId.setRevenue(revenue);
        portalcertificateaddressId.setRedemptionAmount(redemptionAmount);
        portalcertificateaddressId.setAmountPaid(amountPaid);

        LOGGER.debug("Getting PortalCertificateAddress with id: {}" , portalcertificateaddressId);
        PortalCertificateAddress portalCertificateAddress = portalCertificateAddressService.getById(portalcertificateaddressId);
        LOGGER.debug("PortalCertificateAddress details with id: {}" , portalCertificateAddress);

        return portalCertificateAddress;
    }



    @ApiOperation(value = "Updates the PortalCertificateAddress instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public PortalCertificateAddress editPortalCertificateAddress(@RequestParam("countyName") String countyName,@RequestParam("certificateId") Integer certificateId,@RequestParam("bank") String bank,@RequestParam("taxYear") Integer taxYear,@RequestParam("volume") Integer volume,@RequestParam("pin") String pin,@RequestParam("certificateNumber") String certificateNumber,@RequestParam("dateSold") Date dateSold,@RequestParam("expirationDate") Date expirationDate,@RequestParam("status") String status,@RequestParam("taxYearNotices") String taxYearNotices,@RequestParam("addressLine1") String addressLine1,@RequestParam("addressLine2") String addressLine2,@RequestParam("addressCity") String addressCity,@RequestParam("addressZip") String addressZip,@RequestParam("addressVerified") Boolean addressVerified,@RequestParam("addressSource1") String addressSource1,@RequestParam("addressSource2") String addressSource2,@RequestParam("assesseeName") String assesseeName,@RequestParam("assesseeAddressLine1") String assesseeAddressLine1,@RequestParam("assesseeAddressCity") String assesseeAddressCity,@RequestParam("assesseeAddressState") String assesseeAddressState,@RequestParam("assesseeAddressZip") String assesseeAddressZip,@RequestParam("code") String code,@RequestParam("zoning") String zoning,@RequestParam("improvementLevel") String improvementLevel,@RequestParam("certificatePrincipal") Double certificatePrincipal,@RequestParam("subPrincipal") Double subPrincipal,@RequestParam("feePrincipal") Double feePrincipal,@RequestParam("principal") Double principal,@RequestParam("certificateRevenue") Double certificateRevenue,@RequestParam("subRevenue") Double subRevenue,@RequestParam("feeRevenue") Double feeRevenue,@RequestParam("revenue") Double revenue,@RequestParam("redemptionAmount") Double redemptionAmount,@RequestParam("amountPaid") Double amountPaid, @RequestBody PortalCertificateAddress portalCertificateAddress) throws EntityNotFoundException {

        portalCertificateAddress.setCountyName(countyName);
        portalCertificateAddress.setCertificateId(certificateId);
        portalCertificateAddress.setBank(bank);
        portalCertificateAddress.setTaxYear(taxYear);
        portalCertificateAddress.setVolume(volume);
        portalCertificateAddress.setPin(pin);
        portalCertificateAddress.setCertificateNumber(certificateNumber);
        portalCertificateAddress.setDateSold(dateSold);
        portalCertificateAddress.setExpirationDate(expirationDate);
        portalCertificateAddress.setStatus(status);
        portalCertificateAddress.setTaxYearNotices(taxYearNotices);
        portalCertificateAddress.setAddressLine1(addressLine1);
        portalCertificateAddress.setAddressLine2(addressLine2);
        portalCertificateAddress.setAddressCity(addressCity);
        portalCertificateAddress.setAddressZip(addressZip);
        portalCertificateAddress.setAddressVerified(addressVerified);
        portalCertificateAddress.setAddressSource1(addressSource1);
        portalCertificateAddress.setAddressSource2(addressSource2);
        portalCertificateAddress.setAssesseeName(assesseeName);
        portalCertificateAddress.setAssesseeAddressLine1(assesseeAddressLine1);
        portalCertificateAddress.setAssesseeAddressCity(assesseeAddressCity);
        portalCertificateAddress.setAssesseeAddressState(assesseeAddressState);
        portalCertificateAddress.setAssesseeAddressZip(assesseeAddressZip);
        portalCertificateAddress.setCode(code);
        portalCertificateAddress.setZoning(zoning);
        portalCertificateAddress.setImprovementLevel(improvementLevel);
        portalCertificateAddress.setCertificatePrincipal(certificatePrincipal);
        portalCertificateAddress.setSubPrincipal(subPrincipal);
        portalCertificateAddress.setFeePrincipal(feePrincipal);
        portalCertificateAddress.setPrincipal(principal);
        portalCertificateAddress.setCertificateRevenue(certificateRevenue);
        portalCertificateAddress.setSubRevenue(subRevenue);
        portalCertificateAddress.setFeeRevenue(feeRevenue);
        portalCertificateAddress.setRevenue(revenue);
        portalCertificateAddress.setRedemptionAmount(redemptionAmount);
        portalCertificateAddress.setAmountPaid(amountPaid);

        LOGGER.debug("PortalCertificateAddress details with id is updated with: {}" , portalCertificateAddress);

        return portalCertificateAddressService.update(portalCertificateAddress);
    }


    @ApiOperation(value = "Deletes the PortalCertificateAddress instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deletePortalCertificateAddress(@RequestParam("countyName") String countyName,@RequestParam("certificateId") Integer certificateId,@RequestParam("bank") String bank,@RequestParam("taxYear") Integer taxYear,@RequestParam("volume") Integer volume,@RequestParam("pin") String pin,@RequestParam("certificateNumber") String certificateNumber,@RequestParam("dateSold") Date dateSold,@RequestParam("expirationDate") Date expirationDate,@RequestParam("status") String status,@RequestParam("taxYearNotices") String taxYearNotices,@RequestParam("addressLine1") String addressLine1,@RequestParam("addressLine2") String addressLine2,@RequestParam("addressCity") String addressCity,@RequestParam("addressZip") String addressZip,@RequestParam("addressVerified") Boolean addressVerified,@RequestParam("addressSource1") String addressSource1,@RequestParam("addressSource2") String addressSource2,@RequestParam("assesseeName") String assesseeName,@RequestParam("assesseeAddressLine1") String assesseeAddressLine1,@RequestParam("assesseeAddressCity") String assesseeAddressCity,@RequestParam("assesseeAddressState") String assesseeAddressState,@RequestParam("assesseeAddressZip") String assesseeAddressZip,@RequestParam("code") String code,@RequestParam("zoning") String zoning,@RequestParam("improvementLevel") String improvementLevel,@RequestParam("certificatePrincipal") Double certificatePrincipal,@RequestParam("subPrincipal") Double subPrincipal,@RequestParam("feePrincipal") Double feePrincipal,@RequestParam("principal") Double principal,@RequestParam("certificateRevenue") Double certificateRevenue,@RequestParam("subRevenue") Double subRevenue,@RequestParam("feeRevenue") Double feeRevenue,@RequestParam("revenue") Double revenue,@RequestParam("redemptionAmount") Double redemptionAmount,@RequestParam("amountPaid") Double amountPaid) throws EntityNotFoundException {

        PortalCertificateAddressId portalcertificateaddressId = new PortalCertificateAddressId();
        portalcertificateaddressId.setCountyName(countyName);
        portalcertificateaddressId.setCertificateId(certificateId);
        portalcertificateaddressId.setBank(bank);
        portalcertificateaddressId.setTaxYear(taxYear);
        portalcertificateaddressId.setVolume(volume);
        portalcertificateaddressId.setPin(pin);
        portalcertificateaddressId.setCertificateNumber(certificateNumber);
        portalcertificateaddressId.setDateSold(dateSold);
        portalcertificateaddressId.setExpirationDate(expirationDate);
        portalcertificateaddressId.setStatus(status);
        portalcertificateaddressId.setTaxYearNotices(taxYearNotices);
        portalcertificateaddressId.setAddressLine1(addressLine1);
        portalcertificateaddressId.setAddressLine2(addressLine2);
        portalcertificateaddressId.setAddressCity(addressCity);
        portalcertificateaddressId.setAddressZip(addressZip);
        portalcertificateaddressId.setAddressVerified(addressVerified);
        portalcertificateaddressId.setAddressSource1(addressSource1);
        portalcertificateaddressId.setAddressSource2(addressSource2);
        portalcertificateaddressId.setAssesseeName(assesseeName);
        portalcertificateaddressId.setAssesseeAddressLine1(assesseeAddressLine1);
        portalcertificateaddressId.setAssesseeAddressCity(assesseeAddressCity);
        portalcertificateaddressId.setAssesseeAddressState(assesseeAddressState);
        portalcertificateaddressId.setAssesseeAddressZip(assesseeAddressZip);
        portalcertificateaddressId.setCode(code);
        portalcertificateaddressId.setZoning(zoning);
        portalcertificateaddressId.setImprovementLevel(improvementLevel);
        portalcertificateaddressId.setCertificatePrincipal(certificatePrincipal);
        portalcertificateaddressId.setSubPrincipal(subPrincipal);
        portalcertificateaddressId.setFeePrincipal(feePrincipal);
        portalcertificateaddressId.setPrincipal(principal);
        portalcertificateaddressId.setCertificateRevenue(certificateRevenue);
        portalcertificateaddressId.setSubRevenue(subRevenue);
        portalcertificateaddressId.setFeeRevenue(feeRevenue);
        portalcertificateaddressId.setRevenue(revenue);
        portalcertificateaddressId.setRedemptionAmount(redemptionAmount);
        portalcertificateaddressId.setAmountPaid(amountPaid);

        LOGGER.debug("Deleting PortalCertificateAddress with id: {}" , portalcertificateaddressId);
        PortalCertificateAddress portalCertificateAddress = portalCertificateAddressService.delete(portalcertificateaddressId);

        return portalCertificateAddress != null;
    }


    /**
     * @deprecated Use {@link #findPortalCertificateAddresses(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of PortalCertificateAddress instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<PortalCertificateAddress> searchPortalCertificateAddressesByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering PortalCertificateAddresses list");
        return portalCertificateAddressService.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of PortalCertificateAddress instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<PortalCertificateAddress> findPortalCertificateAddresses(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering PortalCertificateAddresses list");
        return portalCertificateAddressService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of PortalCertificateAddress instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<PortalCertificateAddress> filterPortalCertificateAddresses(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering PortalCertificateAddresses list");
        return portalCertificateAddressService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportPortalCertificateAddresses(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return portalCertificateAddressService.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of PortalCertificateAddress instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countPortalCertificateAddresses( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting PortalCertificateAddresses");
		return portalCertificateAddressService.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getPortalCertificateAddressAggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return portalCertificateAddressService.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service PortalCertificateAddressService instance
	 */
	protected void setPortalCertificateAddressService(PortalCertificateAddressService service) {
		this.portalCertificateAddressService = service;
	}

}

