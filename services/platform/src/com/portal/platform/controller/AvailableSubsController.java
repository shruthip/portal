/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.portal.platform.AvailableSubs;
import com.portal.platform.service.AvailableSubsService;


/**
 * Controller object for domain model class AvailableSubs.
 * @see AvailableSubs
 */
@RestController("platform.AvailableSubsController")
@Api(value = "AvailableSubsController", description = "Exposes APIs to work with AvailableSubs resource.")
@RequestMapping("/platform/AvailableSubs")
public class AvailableSubsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AvailableSubsController.class);

    @Autowired
	@Qualifier("platform.AvailableSubsService")
	private AvailableSubsService availableSubsService;

	@ApiOperation(value = "Creates a new AvailableSubs instance.")
@RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
public AvailableSubs createAvailableSubs(@RequestBody AvailableSubs availableSubs) {
		LOGGER.debug("Create AvailableSubs with information: {}" , availableSubs);

		availableSubs = availableSubsService.create(availableSubs);
		LOGGER.debug("Created AvailableSubs with information: {}" , availableSubs);

	    return availableSubs;
	}

    @ApiOperation(value = "Returns the AvailableSubs instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public AvailableSubs getAvailableSubs(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting AvailableSubs with id: {}" , id);

        AvailableSubs foundAvailableSubs = availableSubsService.getById(id);
        LOGGER.debug("AvailableSubs details with id: {}" , foundAvailableSubs);

        return foundAvailableSubs;
    }

    @ApiOperation(value = "Updates the AvailableSubs instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public AvailableSubs editAvailableSubs(@PathVariable("id") Integer id, @RequestBody AvailableSubs availableSubs) throws EntityNotFoundException {
        LOGGER.debug("Editing AvailableSubs with id: {}" , availableSubs.getId());

        availableSubs.setId(id);
        availableSubs = availableSubsService.update(availableSubs);
        LOGGER.debug("AvailableSubs details with id: {}" , availableSubs);

        return availableSubs;
    }

    @ApiOperation(value = "Deletes the AvailableSubs instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteAvailableSubs(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting AvailableSubs with id: {}" , id);

        AvailableSubs deletedAvailableSubs = availableSubsService.delete(id);

        return deletedAvailableSubs != null;
    }

    @RequestMapping(value = "/propertyId-taxYear-installment", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the matching AvailableSubs with given unique key values.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public AvailableSubs getByPropertyIdAndTaxYearAndInstallment(@RequestParam("propertyId") int propertyId, @RequestParam("taxYear") int taxYear, @RequestParam("installment") char installment) {
        LOGGER.debug("Getting AvailableSubs with uniques key PropertyIdAndTaxYearAndInstallment");
        return availableSubsService.getByPropertyIdAndTaxYearAndInstallment(propertyId, taxYear, installment);
    }

    /**
     * @deprecated Use {@link #findAvailableSubs(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of AvailableSubs instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<AvailableSubs> searchAvailableSubsByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering AvailableSubs list");
        return availableSubsService.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of AvailableSubs instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<AvailableSubs> findAvailableSubs(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering AvailableSubs list");
        return availableSubsService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of AvailableSubs instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<AvailableSubs> filterAvailableSubs(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering AvailableSubs list");
        return availableSubsService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportAvailableSubs(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return availableSubsService.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of AvailableSubs instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countAvailableSubs( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting AvailableSubs");
		return availableSubsService.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getAvailableSubsAggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return availableSubsService.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service AvailableSubsService instance
	 */
	protected void setAvailableSubsService(AvailableSubsService service) {
		this.availableSubsService = service;
	}

}

