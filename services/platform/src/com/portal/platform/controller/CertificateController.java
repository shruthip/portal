/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.portal.platform.Accrual;
import com.portal.platform.AvailableSubs;
import com.portal.platform.Certificate;
import com.portal.platform.CertificateParty;
import com.portal.platform.Disbursement;
import com.portal.platform.Matter;
import com.portal.platform.Note;
import com.portal.platform.Payment;
import com.portal.platform.Pdf;
import com.portal.platform.service.CertificateService;


/**
 * Controller object for domain model class Certificate.
 * @see Certificate
 */
@RestController("platform.CertificateController")
@Api(value = "CertificateController", description = "Exposes APIs to work with Certificate resource.")
@RequestMapping("/platform/Certificate")
public class CertificateController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CertificateController.class);

    @Autowired
	@Qualifier("platform.CertificateService")
	private CertificateService certificateService;

	@ApiOperation(value = "Creates a new Certificate instance.")
@RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
public Certificate createCertificate(@RequestBody Certificate certificate) {
		LOGGER.debug("Create Certificate with information: {}" , certificate);

		certificate = certificateService.create(certificate);
		LOGGER.debug("Created Certificate with information: {}" , certificate);

	    return certificate;
	}

    @ApiOperation(value = "Returns the Certificate instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Certificate getCertificate(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting Certificate with id: {}" , id);

        Certificate foundCertificate = certificateService.getById(id);
        LOGGER.debug("Certificate details with id: {}" , foundCertificate);

        return foundCertificate;
    }

    @ApiOperation(value = "Updates the Certificate instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Certificate editCertificate(@PathVariable("id") Integer id, @RequestBody Certificate certificate) throws EntityNotFoundException {
        LOGGER.debug("Editing Certificate with id: {}" , certificate.getId());

        certificate.setId(id);
        certificate = certificateService.update(certificate);
        LOGGER.debug("Certificate details with id: {}" , certificate);

        return certificate;
    }

    @ApiOperation(value = "Deletes the Certificate instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteCertificate(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting Certificate with id: {}" , id);

        Certificate deletedCertificate = certificateService.delete(id);

        return deletedCertificate != null;
    }

    @RequestMapping(value = "/certificateNumber-taxYear-propertyId", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the matching Certificate with given unique key values.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Certificate getByCertificateNumberAndTaxYearAndPropertyId(@RequestParam("certificateNumber") String certificateNumber, @RequestParam("taxYear") int taxYear, @RequestParam("propertyId") int propertyId) {
        LOGGER.debug("Getting Certificate with uniques key CertificateNumberAndTaxYearAndPropertyId");
        return certificateService.getByCertificateNumberAndTaxYearAndPropertyId(certificateNumber, taxYear, propertyId);
    }

    /**
     * @deprecated Use {@link #findCertificates(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of Certificate instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Certificate> searchCertificatesByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering Certificates list");
        return certificateService.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Certificate instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Certificate> findCertificates(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Certificates list");
        return certificateService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Certificate instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Certificate> filterCertificates(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Certificates list");
        return certificateService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportCertificates(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return certificateService.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of Certificate instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countCertificates( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting Certificates");
		return certificateService.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getCertificateAggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return certificateService.getAggregatedValues(aggregationInfo, pageable);
    }

    @RequestMapping(value="/{id:.+}/availableSubses", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the availableSubses instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<AvailableSubs> findAssociatedAvailableSubses(@PathVariable("id") Integer id, Pageable pageable) {

        LOGGER.debug("Fetching all associated availableSubses");
        return certificateService.findAssociatedAvailableSubses(id, pageable);
    }

    @RequestMapping(value="/{id:.+}/notes", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the notes instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Note> findAssociatedNotes(@PathVariable("id") Integer id, Pageable pageable) {

        LOGGER.debug("Fetching all associated notes");
        return certificateService.findAssociatedNotes(id, pageable);
    }

    @RequestMapping(value="/{id:.+}/accruals", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the accruals instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Accrual> findAssociatedAccruals(@PathVariable("id") Integer id, Pageable pageable) {

        LOGGER.debug("Fetching all associated accruals");
        return certificateService.findAssociatedAccruals(id, pageable);
    }

    @RequestMapping(value="/{id:.+}/certificateParties", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the certificateParties instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<CertificateParty> findAssociatedCertificateParties(@PathVariable("id") Integer id, Pageable pageable) {

        LOGGER.debug("Fetching all associated certificateParties");
        return certificateService.findAssociatedCertificateParties(id, pageable);
    }

    @RequestMapping(value="/{id:.+}/disbursements", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the disbursements instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Disbursement> findAssociatedDisbursements(@PathVariable("id") Integer id, Pageable pageable) {

        LOGGER.debug("Fetching all associated disbursements");
        return certificateService.findAssociatedDisbursements(id, pageable);
    }

    @RequestMapping(value="/{id:.+}/matters", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the matters instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Matter> findAssociatedMatters(@PathVariable("id") Integer id, Pageable pageable) {

        LOGGER.debug("Fetching all associated matters");
        return certificateService.findAssociatedMatters(id, pageable);
    }

    @RequestMapping(value="/{id:.+}/payments", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the payments instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Payment> findAssociatedPayments(@PathVariable("id") Integer id, Pageable pageable) {

        LOGGER.debug("Fetching all associated payments");
        return certificateService.findAssociatedPayments(id, pageable);
    }

    @RequestMapping(value="/{id:.+}/pdfs", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the pdfs instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Pdf> findAssociatedPdfs(@PathVariable("id") Integer id, Pageable pageable) {

        LOGGER.debug("Fetching all associated pdfs");
        return certificateService.findAssociatedPdfs(id, pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service CertificateService instance
	 */
	protected void setCertificateService(CertificateService service) {
		this.certificateService = service;
	}

}

