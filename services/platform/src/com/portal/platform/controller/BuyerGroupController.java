/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.portal.platform.Buyer;
import com.portal.platform.BuyerGroup;
import com.portal.platform.service.BuyerGroupService;


/**
 * Controller object for domain model class BuyerGroup.
 * @see BuyerGroup
 */
@RestController("platform.BuyerGroupController")
@Api(value = "BuyerGroupController", description = "Exposes APIs to work with BuyerGroup resource.")
@RequestMapping("/platform/BuyerGroup")
public class BuyerGroupController {

    private static final Logger LOGGER = LoggerFactory.getLogger(BuyerGroupController.class);

    @Autowired
	@Qualifier("platform.BuyerGroupService")
	private BuyerGroupService buyerGroupService;

	@ApiOperation(value = "Creates a new BuyerGroup instance.")
@RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
public BuyerGroup createBuyerGroup(@RequestBody BuyerGroup buyerGroup) {
		LOGGER.debug("Create BuyerGroup with information: {}" , buyerGroup);

		buyerGroup = buyerGroupService.create(buyerGroup);
		LOGGER.debug("Created BuyerGroup with information: {}" , buyerGroup);

	    return buyerGroup;
	}

    @ApiOperation(value = "Returns the BuyerGroup instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public BuyerGroup getBuyerGroup(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting BuyerGroup with id: {}" , id);

        BuyerGroup foundBuyerGroup = buyerGroupService.getById(id);
        LOGGER.debug("BuyerGroup details with id: {}" , foundBuyerGroup);

        return foundBuyerGroup;
    }

    @ApiOperation(value = "Updates the BuyerGroup instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public BuyerGroup editBuyerGroup(@PathVariable("id") Integer id, @RequestBody BuyerGroup buyerGroup) throws EntityNotFoundException {
        LOGGER.debug("Editing BuyerGroup with id: {}" , buyerGroup.getId());

        buyerGroup.setId(id);
        buyerGroup = buyerGroupService.update(buyerGroup);
        LOGGER.debug("BuyerGroup details with id: {}" , buyerGroup);

        return buyerGroup;
    }

    @ApiOperation(value = "Deletes the BuyerGroup instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteBuyerGroup(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting BuyerGroup with id: {}" , id);

        BuyerGroup deletedBuyerGroup = buyerGroupService.delete(id);

        return deletedBuyerGroup != null;
    }

    /**
     * @deprecated Use {@link #findBuyerGroups(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of BuyerGroup instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<BuyerGroup> searchBuyerGroupsByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering BuyerGroups list");
        return buyerGroupService.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of BuyerGroup instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<BuyerGroup> findBuyerGroups(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering BuyerGroups list");
        return buyerGroupService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of BuyerGroup instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<BuyerGroup> filterBuyerGroups(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering BuyerGroups list");
        return buyerGroupService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportBuyerGroups(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return buyerGroupService.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of BuyerGroup instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countBuyerGroups( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting BuyerGroups");
		return buyerGroupService.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getBuyerGroupAggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return buyerGroupService.getAggregatedValues(aggregationInfo, pageable);
    }

    @RequestMapping(value="/{id:.+}/buyers", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the buyers instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Buyer> findAssociatedBuyers(@PathVariable("id") Integer id, Pageable pageable) {

        LOGGER.debug("Fetching all associated buyers");
        return buyerGroupService.findAssociatedBuyers(id, pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service BuyerGroupService instance
	 */
	protected void setBuyerGroupService(BuyerGroupService service) {
		this.buyerGroupService = service;
	}

}

