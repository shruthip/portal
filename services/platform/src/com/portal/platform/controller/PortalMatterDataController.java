/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.sql.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.portal.platform.PortalMatterData;
import com.portal.platform.PortalMatterDataId;
import com.portal.platform.service.PortalMatterDataService;


/**
 * Controller object for domain model class PortalMatterData.
 * @see PortalMatterData
 */
@RestController("platform.PortalMatterDataController")
@Api(value = "PortalMatterDataController", description = "Exposes APIs to work with PortalMatterData resource.")
@RequestMapping("/platform/PortalMatterData")
public class PortalMatterDataController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PortalMatterDataController.class);

    @Autowired
	@Qualifier("platform.PortalMatterDataService")
	private PortalMatterDataService portalMatterDataService;

	@ApiOperation(value = "Creates a new PortalMatterData instance.")
@RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
public PortalMatterData createPortalMatterData(@RequestBody PortalMatterData portalMatterData) {
		LOGGER.debug("Create PortalMatterData with information: {}" , portalMatterData);

		portalMatterData = portalMatterDataService.create(portalMatterData);
		LOGGER.debug("Created PortalMatterData with information: {}" , portalMatterData);

	    return portalMatterData;
	}

@ApiOperation(value = "Returns the PortalMatterData instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public PortalMatterData getPortalMatterData(@RequestParam("certificateId") Integer certificateId,@RequestParam("propertyId") Integer propertyId,@RequestParam("header") String header,@RequestParam("certificateNumber") String certificateNumber,@RequestParam("taxYear") Integer taxYear,@RequestParam("dateSold") Date dateSold,@RequestParam("status") String status,@RequestParam("expirationDate") Date expirationDate,@RequestParam("countyName") String countyName,@RequestParam("volume") Integer volume,@RequestParam("taxYearNotices") String taxYearNotices,@RequestParam("matterId") Integer matterId,@RequestParam("matterNumber") String matterNumber,@RequestParam("filedDate") Date filedDate,@RequestParam("lastWorkDate") Date lastWorkDate,@RequestParam("initialCourtDate") Date initialCourtDate,@RequestParam("closedDate") Date closedDate,@RequestParam("addressOverride") String addressOverride,@RequestParam("pinOverride") String pinOverride,@RequestParam("piqAddress") String piqAddress,@RequestParam("piqCity") String piqCity,@RequestParam("piqState") String piqState,@RequestParam("piqZip") String piqZip,@RequestParam("pin") String pin,@RequestParam("addressSource1") String addressSource1,@RequestParam("addressSource2") String addressSource2,@RequestParam("finalPin") String finalPin) throws EntityNotFoundException {

        PortalMatterDataId portalmatterdataId = new PortalMatterDataId();
        portalmatterdataId.setCertificateId(certificateId);
        portalmatterdataId.setPropertyId(propertyId);
        portalmatterdataId.setHeader(header);
        portalmatterdataId.setCertificateNumber(certificateNumber);
        portalmatterdataId.setTaxYear(taxYear);
        portalmatterdataId.setDateSold(dateSold);
        portalmatterdataId.setStatus(status);
        portalmatterdataId.setExpirationDate(expirationDate);
        portalmatterdataId.setCountyName(countyName);
        portalmatterdataId.setVolume(volume);
        portalmatterdataId.setTaxYearNotices(taxYearNotices);
        portalmatterdataId.setMatterId(matterId);
        portalmatterdataId.setMatterNumber(matterNumber);
        portalmatterdataId.setFiledDate(filedDate);
        portalmatterdataId.setLastWorkDate(lastWorkDate);
        portalmatterdataId.setInitialCourtDate(initialCourtDate);
        portalmatterdataId.setClosedDate(closedDate);
        portalmatterdataId.setAddressOverride(addressOverride);
        portalmatterdataId.setPinOverride(pinOverride);
        portalmatterdataId.setPiqAddress(piqAddress);
        portalmatterdataId.setPiqCity(piqCity);
        portalmatterdataId.setPiqState(piqState);
        portalmatterdataId.setPiqZip(piqZip);
        portalmatterdataId.setPin(pin);
        portalmatterdataId.setAddressSource1(addressSource1);
        portalmatterdataId.setAddressSource2(addressSource2);
        portalmatterdataId.setFinalPin(finalPin);

        LOGGER.debug("Getting PortalMatterData with id: {}" , portalmatterdataId);
        PortalMatterData portalMatterData = portalMatterDataService.getById(portalmatterdataId);
        LOGGER.debug("PortalMatterData details with id: {}" , portalMatterData);

        return portalMatterData;
    }



    @ApiOperation(value = "Updates the PortalMatterData instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public PortalMatterData editPortalMatterData(@RequestParam("certificateId") Integer certificateId,@RequestParam("propertyId") Integer propertyId,@RequestParam("header") String header,@RequestParam("certificateNumber") String certificateNumber,@RequestParam("taxYear") Integer taxYear,@RequestParam("dateSold") Date dateSold,@RequestParam("status") String status,@RequestParam("expirationDate") Date expirationDate,@RequestParam("countyName") String countyName,@RequestParam("volume") Integer volume,@RequestParam("taxYearNotices") String taxYearNotices,@RequestParam("matterId") Integer matterId,@RequestParam("matterNumber") String matterNumber,@RequestParam("filedDate") Date filedDate,@RequestParam("lastWorkDate") Date lastWorkDate,@RequestParam("initialCourtDate") Date initialCourtDate,@RequestParam("closedDate") Date closedDate,@RequestParam("addressOverride") String addressOverride,@RequestParam("pinOverride") String pinOverride,@RequestParam("piqAddress") String piqAddress,@RequestParam("piqCity") String piqCity,@RequestParam("piqState") String piqState,@RequestParam("piqZip") String piqZip,@RequestParam("pin") String pin,@RequestParam("addressSource1") String addressSource1,@RequestParam("addressSource2") String addressSource2,@RequestParam("finalPin") String finalPin, @RequestBody PortalMatterData portalMatterData) throws EntityNotFoundException {

        portalMatterData.setCertificateId(certificateId);
        portalMatterData.setPropertyId(propertyId);
        portalMatterData.setHeader(header);
        portalMatterData.setCertificateNumber(certificateNumber);
        portalMatterData.setTaxYear(taxYear);
        portalMatterData.setDateSold(dateSold);
        portalMatterData.setStatus(status);
        portalMatterData.setExpirationDate(expirationDate);
        portalMatterData.setCountyName(countyName);
        portalMatterData.setVolume(volume);
        portalMatterData.setTaxYearNotices(taxYearNotices);
        portalMatterData.setMatterId(matterId);
        portalMatterData.setMatterNumber(matterNumber);
        portalMatterData.setFiledDate(filedDate);
        portalMatterData.setLastWorkDate(lastWorkDate);
        portalMatterData.setInitialCourtDate(initialCourtDate);
        portalMatterData.setClosedDate(closedDate);
        portalMatterData.setAddressOverride(addressOverride);
        portalMatterData.setPinOverride(pinOverride);
        portalMatterData.setPiqAddress(piqAddress);
        portalMatterData.setPiqCity(piqCity);
        portalMatterData.setPiqState(piqState);
        portalMatterData.setPiqZip(piqZip);
        portalMatterData.setPin(pin);
        portalMatterData.setAddressSource1(addressSource1);
        portalMatterData.setAddressSource2(addressSource2);
        portalMatterData.setFinalPin(finalPin);

        LOGGER.debug("PortalMatterData details with id is updated with: {}" , portalMatterData);

        return portalMatterDataService.update(portalMatterData);
    }


    @ApiOperation(value = "Deletes the PortalMatterData instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deletePortalMatterData(@RequestParam("certificateId") Integer certificateId,@RequestParam("propertyId") Integer propertyId,@RequestParam("header") String header,@RequestParam("certificateNumber") String certificateNumber,@RequestParam("taxYear") Integer taxYear,@RequestParam("dateSold") Date dateSold,@RequestParam("status") String status,@RequestParam("expirationDate") Date expirationDate,@RequestParam("countyName") String countyName,@RequestParam("volume") Integer volume,@RequestParam("taxYearNotices") String taxYearNotices,@RequestParam("matterId") Integer matterId,@RequestParam("matterNumber") String matterNumber,@RequestParam("filedDate") Date filedDate,@RequestParam("lastWorkDate") Date lastWorkDate,@RequestParam("initialCourtDate") Date initialCourtDate,@RequestParam("closedDate") Date closedDate,@RequestParam("addressOverride") String addressOverride,@RequestParam("pinOverride") String pinOverride,@RequestParam("piqAddress") String piqAddress,@RequestParam("piqCity") String piqCity,@RequestParam("piqState") String piqState,@RequestParam("piqZip") String piqZip,@RequestParam("pin") String pin,@RequestParam("addressSource1") String addressSource1,@RequestParam("addressSource2") String addressSource2,@RequestParam("finalPin") String finalPin) throws EntityNotFoundException {

        PortalMatterDataId portalmatterdataId = new PortalMatterDataId();
        portalmatterdataId.setCertificateId(certificateId);
        portalmatterdataId.setPropertyId(propertyId);
        portalmatterdataId.setHeader(header);
        portalmatterdataId.setCertificateNumber(certificateNumber);
        portalmatterdataId.setTaxYear(taxYear);
        portalmatterdataId.setDateSold(dateSold);
        portalmatterdataId.setStatus(status);
        portalmatterdataId.setExpirationDate(expirationDate);
        portalmatterdataId.setCountyName(countyName);
        portalmatterdataId.setVolume(volume);
        portalmatterdataId.setTaxYearNotices(taxYearNotices);
        portalmatterdataId.setMatterId(matterId);
        portalmatterdataId.setMatterNumber(matterNumber);
        portalmatterdataId.setFiledDate(filedDate);
        portalmatterdataId.setLastWorkDate(lastWorkDate);
        portalmatterdataId.setInitialCourtDate(initialCourtDate);
        portalmatterdataId.setClosedDate(closedDate);
        portalmatterdataId.setAddressOverride(addressOverride);
        portalmatterdataId.setPinOverride(pinOverride);
        portalmatterdataId.setPiqAddress(piqAddress);
        portalmatterdataId.setPiqCity(piqCity);
        portalmatterdataId.setPiqState(piqState);
        portalmatterdataId.setPiqZip(piqZip);
        portalmatterdataId.setPin(pin);
        portalmatterdataId.setAddressSource1(addressSource1);
        portalmatterdataId.setAddressSource2(addressSource2);
        portalmatterdataId.setFinalPin(finalPin);

        LOGGER.debug("Deleting PortalMatterData with id: {}" , portalmatterdataId);
        PortalMatterData portalMatterData = portalMatterDataService.delete(portalmatterdataId);

        return portalMatterData != null;
    }


    /**
     * @deprecated Use {@link #findPortalMatterDatas(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of PortalMatterData instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<PortalMatterData> searchPortalMatterDatasByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering PortalMatterDatas list");
        return portalMatterDataService.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of PortalMatterData instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<PortalMatterData> findPortalMatterDatas(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering PortalMatterDatas list");
        return portalMatterDataService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of PortalMatterData instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<PortalMatterData> filterPortalMatterDatas(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering PortalMatterDatas list");
        return portalMatterDataService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportPortalMatterDatas(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return portalMatterDataService.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of PortalMatterData instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countPortalMatterDatas( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting PortalMatterDatas");
		return portalMatterDataService.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getPortalMatterDataAggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return portalMatterDataService.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service PortalMatterDataService instance
	 */
	protected void setPortalMatterDataService(PortalMatterDataService service) {
		this.portalMatterDataService = service;
	}

}

