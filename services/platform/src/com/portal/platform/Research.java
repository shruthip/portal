/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Research generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`research`", schema = "data", uniqueConstraints = {
        @UniqueConstraint(name = "`AK_research_duplicate`", columnNames = {"`property_id`", "`type`", "`date`"})})
public class Research implements Serializable {

    private Integer id;
    private int propertyId;
    private String type;
    private java.sql.Date date;
    private Property property;
    private List<PropertySale> propertySales;
    private List<TaxHistory> taxHistories;
    private List<TitleTransaction> titleTransactions;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`id`", nullable = false, scale = 0, precision = 10)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "`property_id`", nullable = false, scale = 0, precision = 10)
    public int getPropertyId() {
        return this.propertyId;
    }

    public void setPropertyId(int propertyId) {
        this.propertyId = propertyId;
    }

    @Column(name = "`type`", nullable = false, length = 20)
    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Column(name = "`date`", nullable = false)
    public java.sql.Date getDate() {
        return this.date;
    }

    public void setDate(java.sql.Date date) {
        this.date = date;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`property_id`", referencedColumnName = "`ID`", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "`FK_research_property`"))
    public Property getProperty() {
        return this.property;
    }

    public void setProperty(Property property) {
        if(property != null) {
            this.propertyId = property.getId();
        }

        this.property = property;
    }

    @JsonInclude(Include.NON_EMPTY)
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "research")
    public List<PropertySale> getPropertySales() {
        return this.propertySales;
    }

    public void setPropertySales(List<PropertySale> propertySales) {
        this.propertySales = propertySales;
    }

    @JsonInclude(Include.NON_EMPTY)
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "research")
    public List<TaxHistory> getTaxHistories() {
        return this.taxHistories;
    }

    public void setTaxHistories(List<TaxHistory> taxHistories) {
        this.taxHistories = taxHistories;
    }

    @JsonInclude(Include.NON_EMPTY)
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "research")
    public List<TitleTransaction> getTitleTransactions() {
        return this.titleTransactions;
    }

    public void setTitleTransactions(List<TitleTransaction> titleTransactions) {
        this.titleTransactions = titleTransactions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Research)) return false;
        final Research research = (Research) o;
        return Objects.equals(getId(), research.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}

