/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.platform;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Disbursement generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`disbursement`", uniqueConstraints = {
        @UniqueConstraint(name = "`AK_disbursement_duplicate`", columnNames = {"`certificate_id`", "`payment_date`", "`amount`"})})
public class Disbursement implements Serializable {

    private Integer id;
    private int certificateId;
    private Timestamp createdTimestamp;
    private Timestamp updatedTimestamp;
    private String taxYearPaid;
    private Integer installment;
    private java.sql.Date paymentDate;
    private double amount;
    private String reference;
    private int currentPenaltyPeriod;
    private String user;
    private Integer lastImportId;
    private int disbursementTypeId;
    private Integer userId;
    private Certificate certificate;
    private DisbursementType disbursementType;
    private User userByUserId;
    private List<Accrual> accruals;
    private List<Payment> payments;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`id`", nullable = false, scale = 0, precision = 10)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "`certificate_id`", nullable = false, scale = 0, precision = 10)
    public int getCertificateId() {
        return this.certificateId;
    }

    public void setCertificateId(int certificateId) {
        this.certificateId = certificateId;
    }

    @Column(name = "`created_timestamp`", nullable = false)
    public Timestamp getCreatedTimestamp() {
        return this.createdTimestamp;
    }

    public void setCreatedTimestamp(Timestamp createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }

    @Column(name = "`updated_timestamp`", nullable = false)
    public Timestamp getUpdatedTimestamp() {
        return this.updatedTimestamp;
    }

    public void setUpdatedTimestamp(Timestamp updatedTimestamp) {
        this.updatedTimestamp = updatedTimestamp;
    }

    @Column(name = "`tax_year_paid`", nullable = true, length = 4)
    public String getTaxYearPaid() {
        return this.taxYearPaid;
    }

    public void setTaxYearPaid(String taxYearPaid) {
        this.taxYearPaid = taxYearPaid;
    }

    @Column(name = "`installment`", nullable = true, scale = 0, precision = 10)
    public Integer getInstallment() {
        return this.installment;
    }

    public void setInstallment(Integer installment) {
        this.installment = installment;
    }

    @Column(name = "`payment_date`", nullable = false)
    public java.sql.Date getPaymentDate() {
        return this.paymentDate;
    }

    public void setPaymentDate(java.sql.Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    @Column(name = "`amount`", nullable = false, scale = 4, precision = 19)
    public double getAmount() {
        return this.amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Column(name = "`reference`", nullable = true, length = 50)
    public String getReference() {
        return this.reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    @Column(name = "`current_penalty_period`", nullable = false, scale = 0, precision = 10)
    public int getCurrentPenaltyPeriod() {
        return this.currentPenaltyPeriod;
    }

    public void setCurrentPenaltyPeriod(int currentPenaltyPeriod) {
        this.currentPenaltyPeriod = currentPenaltyPeriod;
    }

    @Column(name = "`user`", nullable = false, length = 50)
    public String getUser() {
        return this.user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Column(name = "`last_import_id`", nullable = true, scale = 0, precision = 10)
    public Integer getLastImportId() {
        return this.lastImportId;
    }

    public void setLastImportId(Integer lastImportId) {
        this.lastImportId = lastImportId;
    }

    @Column(name = "`disbursement_type_id`", nullable = false, scale = 0, precision = 10)
    public int getDisbursementTypeId() {
        return this.disbursementTypeId;
    }

    public void setDisbursementTypeId(int disbursementTypeId) {
        this.disbursementTypeId = disbursementTypeId;
    }

    @Column(name = "`user_id`", nullable = true, scale = 0, precision = 10)
    public Integer getUserId() {
        return this.userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`certificate_id`", referencedColumnName = "`id`", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "`FK_disbursement_certificate_id`"))
    public Certificate getCertificate() {
        return this.certificate;
    }

    public void setCertificate(Certificate certificate) {
        if(certificate != null) {
            this.certificateId = certificate.getId();
        }

        this.certificate = certificate;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`disbursement_type_id`", referencedColumnName = "`id`", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "`FK_disbursement_type_id`"))
    public DisbursementType getDisbursementType() {
        return this.disbursementType;
    }

    public void setDisbursementType(DisbursementType disbursementType) {
        if(disbursementType != null) {
            this.disbursementTypeId = disbursementType.getId();
        }

        this.disbursementType = disbursementType;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`user_id`", referencedColumnName = "`id`", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "`FK_disbursement_user_id`"))
    public User getUserByUserId() {
        return this.userByUserId;
    }

    public void setUserByUserId(User userByUserId) {
        if(userByUserId != null) {
            this.userId = userByUserId.getId();
        }

        this.userByUserId = userByUserId;
    }

    @JsonInclude(Include.NON_EMPTY)
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "disbursement")
    public List<Accrual> getAccruals() {
        return this.accruals;
    }

    public void setAccruals(List<Accrual> accruals) {
        this.accruals = accruals;
    }

    @JsonInclude(Include.NON_EMPTY)
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "disbursement")
    public List<Payment> getPayments() {
        return this.payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Disbursement)) return false;
        final Disbursement disbursement = (Disbursement) o;
        return Objects.equals(getId(), disbursement.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}

