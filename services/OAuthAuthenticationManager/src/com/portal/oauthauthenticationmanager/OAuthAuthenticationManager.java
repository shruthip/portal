/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.oauthauthenticationmanager;


import com.portal.platform.User;
import com.portal.platform.models.query.GetUserByEmailResponse;
import com.portal.platform.models.query.UpdateUserNameRequest;
import com.portal.platform.service.PlatformQueryExecutorService;
import com.portal.platform.service.UserService;
import com.wavemaker.runtime.security.AuthRequestContext;
import com.wavemaker.runtime.security.SecurityService;
import com.wavemaker.runtime.security.WMCustomAuthenticationManager;
import com.wavemaker.runtime.security.WMUser;
import com.wavemaker.runtime.service.annotations.ExposeToClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.social.google.api.Google;
import org.springframework.social.google.api.impl.GoogleTemplate;
import org.springframework.social.google.api.plus.Person;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

/**
 * This is a singleton class with all its public methods exposed as REST APIs via generated controller class.
 * To avoid exposing an API for a particular public method, annotate it with @HideFromClient.
 *
 * Method names will play a major role in defining the Http Method for the generated APIs. For example, a method name
 * that starts with delete/remove, will make the API exposed as Http Method "DELETE".
 *
 * Method Parameters of type primitives (including java.lang.String) will be exposed as Query Parameters &
 * Complex Types/Objects will become part of the Request body in the generated API.
 */
@ExposeToClient
public class OAuthAuthenticationManager  implements WMCustomAuthenticationManager {

    private static final Logger logger = LoggerFactory.getLogger(OAuthAuthenticationManager.class);

    @Autowired
    private SecurityService securityService;

    @Autowired
    private PlatformQueryExecutorService queryExceutorService;

    @Autowired
    private UserService userService;

    private WMUser user = null;


    /**
     * This is sample java operation that accepts an input from the caller and responds with "Hello".
     * <p>
     * SecurityService that is Autowired will provide access to the security context of the caller. It has methods like isAuthenticated(),
     * getUserName() and getUserId() etc which returns the information based on the caller context.
     * <p>
     * Methods in this class can declare HttpServletRequest, HttpServletResponse as input parameters to access the
     * caller's request/response objects respectively. These parameters will be injected when request is made (during API invocation).
     */
    @Override
    public WMUser authenticate(AuthRequestContext authRequestContext) {
        String securityProvider = authRequestContext.getUsername();
        String token = authRequestContext.getPassword();
        if(securityProvider != null && token != null){
            //Using Spring Social and Validating the Token with getGoogleProfile Method
            Google google = new GoogleTemplate(token);
            Person person = google.plusOperations().getGoogleProfile();
            String loggedInEmail = person.getAccountEmail();
            String domainName = loggedInEmail.substring(loggedInEmail.lastIndexOf("@") + 1);
            // Domain Check
            if(domainName.equals("imaginea.com") || domainName.equals("newlinefinancial.com") || domainName.equals("wavemaker.com")) {
               try {
                   List<GetUserByEmailResponse> userData = queryExceutorService.executeGetUserByEmail(loggedInEmail,null).getContent();
                  if(userData != null && userData.size() > 0){
                      for (GetUserByEmailResponse userObj : userData) {
                          if(userObj.getName() == null){
                              try {
                                  UpdateUserNameRequest updateUserNameRequest = new UpdateUserNameRequest();
                                  updateUserNameRequest.setUserId(userObj.getId());
                                  updateUserNameRequest.setUserName(person.getDisplayName().toString());
                                  queryExceutorService.executeUpdateUserName(updateUserNameRequest);
                              }catch (Exception e){
                                  throw new BadCredentialsException(e.getMessage());
                              }
                              user = new WMUser(String.valueOf(userObj.getId()), loggedInEmail, "", "", 123, Arrays.asList((new String[]{userObj.getGroupName(), token})));
                          }else{
                              user = new WMUser(String.valueOf(userObj.getId()), loggedInEmail, "", "", 123, Arrays.asList((new String[]{userObj.getGroupName(), token})));
                          }
                      }
                  }else{
                      // Create a New User Object in DB.
                      User userObj = new User();
                      userObj.setEmail(loggedInEmail);
                      userObj.setName(person.getDisplayName());
                       userObj.setGroupName("user");
                      String userID = String.valueOf(userService.create(userObj).getId());
                      user = new WMUser(userID, loggedInEmail, "", "", 123, Arrays.asList((new String[]{userObj.getGroupName(), token})));
                  }
               }catch (Exception e){
                   throw new BadCredentialsException(e.getMessage());
               }
            }else {
                throw new BadCredentialsException("UNAUTHORIZED DOMAIN");
            }
        }
        return user;
    }
}
