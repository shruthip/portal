/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.dynamicprocedure;

import java.util.List;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;


import com.wavemaker.runtime.security.SecurityService;
import com.wavemaker.runtime.service.annotations.ExposeToClient;
import com.wavemaker.runtime.service.annotations.HideFromClient;

import com.wavemaker.runtime.data.model.JavaType;
import com.wavemaker.runtime.data.model.procedures.ProcedureParameter;
import com.wavemaker.runtime.data.model.procedures.ProcedureParameterType;
import com.wavemaker.runtime.data.model.procedures.RuntimeProcedure;

import com.wavemaker.runtime.data.dao.procedure.WMProcedureExecutor;

//import com.portal.dynamicprocedure.model.*;

/**
 * This is a singleton class with all its public methods exposed as REST APIs via generated controller class.
 * To avoid exposing an API for a particular public method, annotate it with @HideFromClient.
 *
 * Method names will play a major role in defining the Http Method for the generated APIs. For example, a method name
 * that starts with delete/remove, will make the API exposed as Http Method "DELETE".
 *
 * Method Parameters of type primitives (including java.lang.String) will be exposed as Query Parameters &
 * Complex Types/Objects will become part of the Request body in the generated API.
 */
@ExposeToClient
public class DynamicProcedure {

    private static final Logger logger = LoggerFactory.getLogger(DynamicProcedure.class);

    @Autowired
    private SecurityService securityService;
    
    @Autowired
    @Qualifier("platformWMProcedureExecutor")
    private WMProcedureExecutor procedureExecutor;
    
    public Object executeDynamicProcedure(String template_id, String county, String tax_year, String disbursement_date, String expiration_date, String release_date_start, String release_date_end, String certificate_id, String volume_floor, String volume_ceiling) {
        
        logger.debug("Starting executeDynamicProcedure operation ");
        
        String procedureCall = "exec servicing.pdf_filter " +
                               " @template = :template_id, " +
                               " @county = :county, " +
                               " @tax_year = :tax_year, " +
                               " @disbursement_date = :disbursement_date, " +
                               " @expiration_date = :expiration_date, " +
                               " @release_date_start = :release_date_start, " +
                               " @release_date_end = :release_date_end, " +
                               " @certificate_id = :certificate_id, " +
                               " @volume_floor = :volume_floor, " +
                               " @volume_ceiling = :volume_ceiling ";
        
        RuntimeProcedure procedure = new RuntimeProcedure();
        procedure.setProcedureString(procedureCall);
        
        List<ProcedureParameter> parameters = new ArrayList<>();
        
        ProcedureParameter parameter1 = new ProcedureParameter();
        parameter1.setName("template_id");
        parameter1.setType(JavaType.INTEGER); // BYTE, SHORT, INTEGER, LONG, BIG_INTEGER, FLOAT, DOUBLE, BIG_DECIMAL, BOOLEAN, CHARACTER, STRING, TEXT, CLOB, BLOB, TIME, DATE, DATETIME, TIMESTAMP, CURSOR
        parameter1.setTestValue(template_id);
        parameter1.setParameterType(ProcedureParameterType.IN); // IN, OUT, IN_OUT
        parameters.add(parameter1);
        
        ProcedureParameter parameter2 = new ProcedureParameter();
        parameter2.setName("county");
        parameter2.setType(JavaType.STRING); // BYTE, SHORT, INTEGER, LONG, BIG_INTEGER, FLOAT, DOUBLE, BIG_DECIMAL, BOOLEAN, CHARACTER, STRING, TEXT, CLOB, BLOB, TIME, DATE, DATETIME, TIMESTAMP, CURSOR
        parameter2.setTestValue(county);
        parameter2.setParameterType(ProcedureParameterType.IN); // IN, OUT, IN_OUT
        parameters.add(parameter2);
        
        ProcedureParameter parameter3 = new ProcedureParameter();
        parameter3.setName("tax_year");
        parameter3.setType(JavaType.INTEGER); // BYTE, SHORT, INTEGER, LONG, BIG_INTEGER, FLOAT, DOUBLE, BIG_DECIMAL, BOOLEAN, CHARACTER, STRING, TEXT, CLOB, BLOB, TIME, DATE, DATETIME, TIMESTAMP, CURSOR
        parameter3.setTestValue(tax_year);
        parameter3.setParameterType(ProcedureParameterType.IN); // IN, OUT, IN_OUT
        parameters.add(parameter3);
        
        ProcedureParameter parameter4 = new ProcedureParameter();
        parameter4.setName("disbursement_date");
        parameter4.setType(JavaType.DATE); // BYTE, SHORT, INTEGER, LONG, BIG_INTEGER, FLOAT, DOUBLE, BIG_DECIMAL, BOOLEAN, CHARACTER, STRING, TEXT, CLOB, BLOB, TIME, DATE, DATETIME, TIMESTAMP, CURSOR
        parameter4.setTestValue(disbursement_date);
        parameter4.setParameterType(ProcedureParameterType.IN); // IN, OUT, IN_OUT
        parameters.add(parameter4);
        
        ProcedureParameter parameter5 = new ProcedureParameter();
        parameter5.setName("expiration_date");
        parameter5.setType(JavaType.DATE); // BYTE, SHORT, INTEGER, LONG, BIG_INTEGER, FLOAT, DOUBLE, BIG_DECIMAL, BOOLEAN, CHARACTER, STRING, TEXT, CLOB, BLOB, TIME, DATE, DATETIME, TIMESTAMP, CURSOR
        parameter5.setTestValue(expiration_date);
        parameter5.setParameterType(ProcedureParameterType.IN); // IN, OUT, IN_OUT
        parameters.add(parameter5);
        
        ProcedureParameter parameter6 = new ProcedureParameter();
        parameter6.setName("release_date_start");
        parameter6.setType(JavaType.DATE); // BYTE, SHORT, INTEGER, LONG, BIG_INTEGER, FLOAT, DOUBLE, BIG_DECIMAL, BOOLEAN, CHARACTER, STRING, TEXT, CLOB, BLOB, TIME, DATE, DATETIME, TIMESTAMP, CURSOR
        parameter6.setTestValue(release_date_start);
        parameter6.setParameterType(ProcedureParameterType.IN); // IN, OUT, IN_OUT
        parameters.add(parameter6);
        
        ProcedureParameter parameter7 = new ProcedureParameter();
        parameter7.setName("release_date_end");
        parameter7.setType(JavaType.DATE); // BYTE, SHORT, INTEGER, LONG, BIG_INTEGER, FLOAT, DOUBLE, BIG_DECIMAL, BOOLEAN, CHARACTER, STRING, TEXT, CLOB, BLOB, TIME, DATE, DATETIME, TIMESTAMP, CURSOR
        parameter7.setTestValue(release_date_end);
        parameter7.setParameterType(ProcedureParameterType.IN); // IN, OUT, IN_OUT
        parameters.add(parameter7);
        
        ProcedureParameter parameter8 = new ProcedureParameter();
        parameter8.setName("certificate_id");
        parameter8.setType(JavaType.INTEGER); // BYTE, SHORT, INTEGER, LONG, BIG_INTEGER, FLOAT, DOUBLE, BIG_DECIMAL, BOOLEAN, CHARACTER, STRING, TEXT, CLOB, BLOB, TIME, DATE, DATETIME, TIMESTAMP, CURSOR
        parameter8.setTestValue(certificate_id);
        parameter8.setParameterType(ProcedureParameterType.IN); // IN, OUT, IN_OUT
        parameters.add(parameter8);
        
        ProcedureParameter parameter9 = new ProcedureParameter();
        parameter9.setName("volume_floor");
        parameter9.setType(JavaType.INTEGER); // BYTE, SHORT, INTEGER, LONG, BIG_INTEGER, FLOAT, DOUBLE, BIG_DECIMAL, BOOLEAN, CHARACTER, STRING, TEXT, CLOB, BLOB, TIME, DATE, DATETIME, TIMESTAMP, CURSOR
        parameter9.setTestValue(volume_floor);
        parameter9.setParameterType(ProcedureParameterType.IN); // IN, OUT, IN_OUT
        parameters.add(parameter9);
        
        ProcedureParameter parameter10 = new ProcedureParameter();
        parameter10.setName("volume_ceiling");
        parameter10.setType(JavaType.INTEGER); // BYTE, SHORT, INTEGER, LONG, BIG_INTEGER, FLOAT, DOUBLE, BIG_DECIMAL, BOOLEAN, CHARACTER, STRING, TEXT, CLOB, BLOB, TIME, DATE, DATETIME, TIMESTAMP, CURSOR
        parameter10.setTestValue(volume_ceiling);
        parameter10.setParameterType(ProcedureParameterType.IN); // IN, OUT, IN_OUT
        parameters.add(parameter10);
        
        procedure.setParameters(parameters);
        
        logger.debug("Returning Results - executeDynamicProcedure");
        
        return procedureExecutor.executeRuntimeProcedure(procedure);
    }

}
