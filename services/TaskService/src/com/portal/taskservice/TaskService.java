/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.taskservice;

import com.portal.platform.Note;
import com.portal.platform.service.NoteService;
import com.wavemaker.commons.WMRuntimeException;
import com.wavemaker.runtime.security.SecurityService;
import com.wavemaker.runtime.service.annotations.ExposeToClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;


import java.util.ArrayList;

//import com.portal.taskservice.model.*;

/**
 * This is a singleton class with all its public methods exposed as REST APIs via generated controller class.
 * To avoid exposing an API for a particular public method, annotate it with @HideFromClient.
 * <p>
 * Method names will play a major role in defining the Http Method for the generated APIs. For example, a method name
 * that starts with delete/remove, will make the API exposed as Http Method "DELETE".
 * <p>
 * Method Parameters of type primitives (including java.lang.String) will be exposed as Query Parameters &
 * Complex Types/Objects will become part of the Request body in the generated API.
 */
@ExposeToClient
public class TaskService {

    private static final Logger logger = LoggerFactory.getLogger(TaskService.class);


    @Autowired
    private NoteService noteService;


    /**
     * This is sample java operation that accepts an input from the caller and responds with "Hello".
     * <p>
     * SecurityService that is Autowired will provide access to the security context of the caller. It has methods like isAuthenticated(),
     * getUserName() and getUserId() etc which returns the information based on the caller context.
     * <p>
     * Methods in this class can declare HttpServletRequest, HttpServletResponse as input parameters to access the
     * caller's request/response objects respectively. These parameters will be injected when request is made (during API invocation).
     */
    //Review: Should be transactional??!
    @Transactional(value = "platformTransactionManager")
    public Boolean addMutipleTasks(ArrayList<Note> notes) throws WMRuntimeException {
               logger.info("Notes for loop"+notes.toString());
        try {
            for (Note note : notes) {
                logger.info("Notes for loop"+note.toString());
                 noteService.create(note);
                logger.info("Task created");
                }
        } catch (Exception e) {
            logger.info(e.getMessage());
            throw new RuntimeException(e);
        }

        return true;
    }
}