/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.qboauthservice;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import com.wavemaker.runtime.security.SecurityService;
import com.wavemaker.runtime.service.annotations.ExposeToClient;
import com.wavemaker.runtime.service.annotations.HideFromClient;

//import com.qbotest.qboauthservice.model.*;

//my imports
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.apache.commons.lang.StringUtils;

import com.portal.platform.Oauth;
import com.portal.platform.service.OauthService;
import com.portal.platform.service.PlatformQueryExecutorService;
import com.portal.platform.models.query.GetOauthDataResponse;
import com.portal.platform.models.query.UpdateOauthRequest;
import com.portal.qboauthclientservice.QBOAuthClientService;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.intuit.oauth2.config.OAuth2Config;
import com.intuit.oauth2.config.Scope;
import com.intuit.oauth2.data.BearerTokenResponse;
import com.intuit.oauth2.client.OAuth2PlatformClient;
import com.intuit.oauth2.exception.OAuthException;

@ExposeToClient
public class QBOAuthService {

    private static final Logger logger = LoggerFactory.getLogger(QBOAuthService.class);
    
    @Value("${app.environment.qbLandingPage}")
    private String landingPage;
    
    @Value("${app.environment.qbCompanyId}")
    private String qbCompanyId;
    
    @Autowired
    private QBOAuthClientService qbOAuthClientService;
    
    @Autowired
    private OauthService oauthService;

    @Autowired
    private PlatformQueryExecutorService queryService;

    //This method returns the authorization URL
    public String getAuthURL(HttpServletRequest request) {
		OAuth2Config oauth2Config = qbOAuthClientService.getOAuth2Config();
		String csrf = oauth2Config.generateCSRFToken();
		//session.setAttribute("csrfToken", csrf);
		try {
			List<Scope> scopes = new ArrayList<Scope>();
			scopes.add(Scope.Accounting);
			return  oauth2Config.prepareUrl(scopes, getCallbackURL(request), csrf);
		} catch (Exception ex) {
			logger.error("Error occurred while preparing authorization URL", ex);
			throw new RuntimeException(ex);
		}
	}

    //This method returns true when the authorization happened already for the given clientid and clientsecret values.
    public boolean isAuthenticated(HttpServletRequest request) throws Exception {
 	    GetOauthDataResponse oauthData = getOauth();
 	    if(oauthData == null || StringUtils.isBlank(oauthData.getAccessToken())) {
 	        return false;
 	    } else {
 	        return true;
 	    }
 	}
	
	//This is a callback URL where the access_token and refresh_token can be generated from the authcode.
    public void callback(HttpServletRequest request, HttpServletResponse response) {
        try {
            String authCode =  request.getParameter("code");
            String realmId = request.getParameter("realmId");
            /*logger.info("AuthCode: " + authCode);
            logger.info("state: " + request.getParameter("state"));
            logger.info("realmId: " + realmId);*/
            OAuth2PlatformClient client  = qbOAuthClientService.getOAuth2PlatformClient();
            BearerTokenResponse bearerTokenResponse = client.retrieveBearerTokens(authCode, getCallbackURL(request));
            //Update the tokens in database.
            updateOauth(bearerTokenResponse.getAccessToken(), bearerTokenResponse.getRefreshToken());
 	        response.sendRedirect(request.getContextPath() + "/#/" + landingPage);
        } catch (Exception ex) {
            logger.error("Exception occurred: ", ex);
        }
    }
    
    //This method returns OAuth table record (tokens)    
    public GetOauthDataResponse getOauth() {
        try {
            return queryService.executeGetOauthData();
        } catch (EntityNotFoundException ex) {
            //It will be thrown when there is no record found (for the quickbooks)
            //It will happen only for the first time.
            createOauth();
            return null;
        }
    }
    
    //This method is used to refresh the tokens using the latest refresh_token from database.
    public void refreshTokens() {
        try {
            OAuth2PlatformClient client  = qbOAuthClientService.getOAuth2PlatformClient();
            BearerTokenResponse bearerTokenResponse = client.refreshToken(getOauth().getRefreshToken());
            /*logger.info("Refreshed Tokens:");
            logger.info("access_token: "  + bearerTokenResponse.getAccessToken());
            logger.info("refresh_token: " + bearerTokenResponse.getRefreshToken());*/
            //Updating the refreshed tokens (both new access_token and new refresh_tokens) in database.
            updateOauth(bearerTokenResponse.getAccessToken(), bearerTokenResponse.getRefreshToken());
        } catch (OAuthException oe) {
            throw new RuntimeException("Error occurred while getting bearer token.", oe);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    //This method is used to update the tokens in database table.
    private int updateOauth(String accessToken, String refreshToken) {
        UpdateOauthRequest request = new UpdateOauthRequest();
        request.setAccessToken(accessToken);
        request.setRefreshToken(refreshToken);
        return queryService.executeUpdateOauth(request);
    }

    //This method used to create oauth table record for the first time.
    private void createOauth() {
        try {
            Oauth oauth = new Oauth();
            oauth.setProviderId(qbCompanyId);
            oauth.setProviderName("QuickBooks");
            oauthService.create(oauth);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
    
    //Preparting the callback URL
 	private String getCallbackURL(HttpServletRequest request) {
        String requestURL = request.getRequestURL().toString();
        String callbackURL = requestURL.substring(0, requestURL.lastIndexOf("/")) + "/callback";
        logger.info("Request url is {}", requestURL + " :: callbackURL :" + callbackURL);
        return callbackURL;
    }
    
}
