/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.servicesearch;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;


import com.wavemaker.runtime.security.SecurityService;
import com.wavemaker.runtime.service.annotations.ExposeToClient;
import com.wavemaker.runtime.service.annotations.HideFromClient;

import java.util.List;
import java.util.ArrayList;
import java.util.ListIterator;
import java.sql.Date;

import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.Cache;
import org.ehcache.expiry.Expirations;
import org.ehcache.expiry.Duration;
import java.util.concurrent.TimeUnit;

import com.portal.platform.service.PlatformProcedureExecutorService;

//import com.portal.servicesearch.model.*;

/**
 * This is a singleton class with all its public methods exposed as REST APIs via generated controller class.
 * To avoid exposing an API for a particular public method, annotate it with @HideFromClient.
 *
 * Method names will play a major role in defining the Http Method for the generated APIs. For example, a method name
 * that starts with delete/remove, will make the API exposed as Http Method "DELETE".
 *
 * Method Parameters of type primitives (including java.lang.String) will be exposed as Query Parameters &
 * Complex Types/Objects will become part of the Request body in the generated API.
 */
@ExposeToClient
public class ServiceSearch {

    private static final Logger logger = LoggerFactory.getLogger(ServiceSearch.class);

    @Autowired
    private PlatformProcedureExecutorService platformProcedureExecutorService;

    CacheManager cacheManager;

    public List getSearchResult(String search, String countyName, Integer taxYear, String certificateStatus, String expirationDate, String expirationDateFlag, String tag, String tagFlag, Integer fetch, Integer pageNumber, String state, Boolean isNewSearch, String uniqueSearchId, String previousUniqueSearchId) {
        logger.debug("Starting getSearchResult operation - Search " + search);
        
        // Create Cache Manager when calling Service Search for the first time
        if (cacheManager == null) {
            logger.debug("getSearchResult operation - Created new Cache Manager");
            
            cacheManager = CacheManagerBuilder.newCacheManagerBuilder().withCache("preConfigured",         CacheConfigurationBuilder.newCacheConfigurationBuilder(Long.class, List.class, ResourcePoolsBuilder.heap(10)).withExpiry(Expirations.timeToIdleExpiration(Duration.of(1800, TimeUnit.SECONDS)))).build();
        
            cacheManager.init();
            
        }
        
        //Create Cache on new search or when the Cache expires. Otherwise use existing Cache
        Cache<Long, List> resultCache = cacheManager.getCache("cache" + uniqueSearchId, Long.class, List.class);
        if (isNewSearch || resultCache == null) {
            // Convert String date to util & sql Date
            Date expirationDateSql = null;
            if (expirationDate != null) {
                try {
                java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
                java.util.Date expirationDateUtil = sdf.parse(expirationDate);
                expirationDateSql = new Date(expirationDateUtil.getTime()); 
                } catch (java.text.ParseException e) {
                    logger.debug("Error on getSearchResult operation - expirationDate conversion " + expirationDate);
                }
            }
            // Get result by calling Call Search Procedure
            List result = platformProcedureExecutorService.executeProcPortalServiceSearch(search, countyName, taxYear, certificateStatus, expirationDateSql, expirationDateFlag, tag, tagFlag, state).getContent();
            
            logger.debug("getSearchResult operation - Created new Cache Manager cache" + uniqueSearchId);
            
            resultCache = cacheManager.createCache("cache" + uniqueSearchId, 
        CacheConfigurationBuilder.newCacheConfigurationBuilder(Long.class, List.class, ResourcePoolsBuilder.heap(10)).withExpiry(Expirations.timeToIdleExpiration(Duration.of(1800, TimeUnit.SECONDS))).build());
        
            resultCache.put(1L, result);
            
            // If the user is repeating search then remove the existing cache
            if (previousUniqueSearchId != null) {
                logger.debug("getSearchResult operation - Removed existing Cache - cache" + previousUniqueSearchId);
                cacheManager.removeCache("cache" + previousUniqueSearchId); 
            }
            
        }
        
        List resultList = resultCache.get(1L); 
        List requestedPageResultList = new ArrayList();
        
        int totalSize = resultList.size();
        if (totalSize > 0) {
            int startingIndex = (pageNumber - 1) * fetch;
            int endingIndex = startingIndex + fetch;
            if (endingIndex > totalSize) {
                endingIndex = totalSize;
            }
            logger.debug("Starting = " + startingIndex + "; Ending = " + endingIndex + "; pageNumber = "+ pageNumber + "; fetch = " + fetch + "; totalSize = ", totalSize);
            requestedPageResultList = resultList.subList(startingIndex, endingIndex);
        }

        logger.debug("Returning {}", totalSize);
        return requestedPageResultList;
    }

}
