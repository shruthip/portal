/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.propertyrequests;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;


import com.wavemaker.runtime.security.SecurityService;
import com.wavemaker.runtime.service.annotations.ExposeToClient;
import com.wavemaker.runtime.service.annotations.HideFromClient;

//import com.portal.propertyrequests.model.*;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.HttpResponse;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.*;
import com.google.gson.Gson;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.IOException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;

import com.portal.platform.service.CountyService;
import com.portal.platform.service.PlatformQueryExecutorService;
import com.portal.platform.models.query.GetCountyDetailsResponse;

/**
 * This is a singleton class with all its public methods exposed as REST APIs via generated controller class.
 * To avoid exposing an API for a particular public method, annotate it with @HideFromClient.
 *
 * Method names will play a major role in defining the Http Method for the generated APIs. For example, a method name
 * that starts with delete/remove, will make the API exposed as Http Method "DELETE".
 *
 * Method Parameters of type primitives (including java.lang.String) will be exposed as Query Parameters &
 * Complex Types/Objects will become part of the Request body in the generated API.
 */
@ExposeToClient
public class PropertyRequests {

    private static final Logger logger = LoggerFactory.getLogger(PropertyRequests.class);

    @Autowired
    private SecurityService securityService;
    
    @Autowired
    private CountyService countyService;
    
    @Autowired
    private PlatformQueryExecutorService queryExecutorService;

    /**
     * Squarespace makes request for Property information and getDataRequest calls Zoho APIs and responds with Property Details.
     *
     * This project just used as a wrapper to hide Zoho's Auth Token from end user.
     */
    public JsonNode getDataRequest(String apiName) {
        logger.debug("Starting getDataRequest operation with api Name " + apiName);
        
        StringBuffer result = new StringBuffer();
        
        try {
            //Get SSLContext, which validates all the certificates.
            SSLContext sc = getSslContext();
            
            //Create a HttpClient using ClientBuilder, with the above SSLContext Created.
            HttpClient client = HttpClientBuilder.create().
                        setSslcontext(sc).
                        build();
            
            String url = "https://crm.zoho.com/crm/private/json/Leads/getRecords?authtoken=3ba0856e93a944f3f51cab3bbda350f6&scope=crmapi&fromIndex=1&toIndex=200";
            
            //Create a HttpPost variable
            HttpPost post = new HttpPost(url);
            
            //Execute the post request
            HttpResponse response = client.execute(post);
            
            //Read the response in the buffer reader
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            
            //Login to fetch the required results form the obtained response.
            String line = "";
            while ((line = rd.readLine()) != null) 
            {
                result.append(line);
            }
        
        }
         catch(KeyManagementException ex) 
         {
             logger.error("KeyManagementException " + ex.getMessage());
         }
         catch(Exception ex) 
         {
             
    			logger.error("SSL Context Errors Logged " + ex.getMessage());
    	 }
        
        String resp = "";
        if(result != null) {
            resp = result.toString();
        }
        
        JsonNode actualObj =  null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            actualObj = mapper.readTree(resp);
        } catch (JsonParseException ex) {
            logger.error("JSON Errors Logged " + ex.getMessage());
        } catch (JsonProcessingException ex) {
            logger.error("JSON Errors Logged " + ex.getMessage());
        } catch (IOException ex) {
            logger.error("JSON Errors Logged " + ex.getMessage());
        }
        
        return actualObj;
    }
    
    // Create a trust manager that does not validate certificate chains
    private static final TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return null;
        }

        public void checkClientTrusted(X509Certificate[] certs, String authType) {
        }

        public void checkServerTrusted(X509Certificate[] certs, String authType) {
        }
    }
    };
    
    private static SSLContext getSslContext() throws KeyManagementException {
        SSLContext sc = null;
        try {
            sc = SSLContext.getInstance("TLS");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        return sc;
    }
    
    public List<GetCountyDetailsResponse> addressRequest(){
        PageRequest req = new PageRequest(0, Integer.MAX_VALUE);
        Page<GetCountyDetailsResponse> countyDetailsPage =  queryExecutorService.executeGetCountyDetails(req);
        List<GetCountyDetailsResponse> countyDetailsList = countyDetailsPage.getContent();
        return countyDetailsList;
    }

}
