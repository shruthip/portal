/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.pdfjavaservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.fasterxml.jackson.databind.JsonNode;
import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;
import com.microsoft.azure.storage.*;
import com.microsoft.azure.storage.blob.*;
import com.wavemaker.runtime.file.manager.FileServiceManager;
import com.wavemaker.runtime.file.model.DownloadResponse;
import com.wavemaker.runtime.security.SecurityService;
import com.wavemaker.runtime.service.annotations.ExposeToClient;
import org.springframework.beans.factory.annotation.Value;
//import com.portal.pdfjavaservice.model.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import com.portal.platform.service.PlatformQueryExecutorService_V1;

/**
 * This is a singleton class with all its public methods exposed as REST APIs via generated controller class.
 * To avoid exposing an API for a particular public method, annotate it with @HideFromClient.
 * <p>
 * Method names will play a major role in defining the Http Method for the generated APIs. For example, a method name
 * that starts with delete/remove, will make the API exposed as Http Method "DELETE".
 * <p>
 * Method Parameters of type primitives (including java.lang.String) will be exposed as Query Parameters &
 * Complex Types/Objects will become part of the Request body in the generated API.
 */
@ExposeToClient
public class PdfJavaService {

    private static final Logger logger = LoggerFactory.getLogger(PdfJavaService.class);
    private static final String PDF = ".pdf";
    private static String PDF_DEV = "pdf-dev";
    private static final String NEWLINE_TEMPLATE = "NewLineTemplate_";
    private static final String NEWLINE_TEMPLATE_BATCH = "NewLineTemplate_batch_";
    private static final String XHTML = ".xhtml";

    @Autowired
    private SecurityService securityService;

    @Autowired
    private FileServiceManager fileServiceManager;

    @Autowired
    private PlatformQueryExecutorService_V1 queryExceutorService;
    
    @Value("${app.environment.azurefileupload}")
    private String azureEndpointsProtocol;
    
    @Value("${app.environment.azureaccountname}")
    private String azureAccountName;
    
    @Value("${app.environment.azureaccountkey}")
    private String azureAccountKey;

    public String pushPDFToAzure(String sourceFile, String azureFolder) throws Exception {
        String url = "";
        FileInputStream fileInputStream = null;
        File outputFile = null;
		PDF_DEV = azureFolder;

        try {
            CloudBlobContainer container = createCloudBlobContainer();
            outputFile = new File(sourceFile);
            CloudBlockBlob blob = container.getBlockBlobReference(sourceFile);
            fileInputStream = new FileInputStream(outputFile);
            blob.upload(fileInputStream, outputFile.length());
            url = blob.getUri().toString();
        } catch (FileNotFoundException fe) {
            throw new RuntimeException("File not found", fe);
        } catch(Exception e) {
            e.printStackTrace();
            logger.error("Error - pushPDFToAzure");
        } finally {
                    fileInputStream.close();
                    outputFile.delete();
        }
        return url;
    }
    
    public String mergePdf(String batchCode, JsonNode stringFiles) throws Exception {

        FileOutputStream pdfFos = null;
        String newFileName = NEWLINE_TEMPLATE_BATCH + batchCode + PDF;
        
        //Create a new Pdf Document
        Document document = null;

        //String[] fileNames = stringFiles.split(",");
        try {
            List<PdfReader> readers = getReaders(stringFiles);
            
            document = new Document();
            
            //Get a Writer instance
            PdfWriter writer = PdfWriter.getInstance(document, pdfFos);
            
            document.open();

            //Make a target Pdf File
            File pdf = new File(newFileName);
            pdfFos = new FileOutputStream(pdf);

            PdfContentByte cb = getPdfContentBytes(pdfFos);

            PdfImportedPage page;
            for (PdfReader pdfReader : readers) {
// Create a new page in the target for each source page.
                for (int i = 1; i <= pdfReader.getNumberOfPages(); i++) {
                    document.newPage();
                    page = writer.getImportedPage(pdfReader, i);
                    cb.addTemplate(page, 0, 0);
                }
            }
        } catch (FileNotFoundException fe) {
            throw new RuntimeException("FileNot found", fe);
        } finally {
            document.close();
            try {
                if (pdfFos != null) {
                    pdfFos.close();
                }
            } catch (Exception e) {
            }
        }
        return newFileName;
    }

    private List<PdfReader> getReaders(JsonNode stringFiles) {
        List<PdfReader> readers = new ArrayList<>();
        String[] fileNames = stringFiles.get("files").textValue().split(",");
        for (String file : fileNames) {
            try {
                readers.add(new PdfReader(file));
            } catch(IOException e) {
                e.printStackTrace();
                logger.error("Error - getReaders - IOException");
            } catch(Exception e) {
                e.printStackTrace();
                logger.error("Error - getReaders");
            }
        }
        return readers;
    }

    private PdfContentByte getPdfContentBytes(FileOutputStream pdfFos) {

        //Get a Writer instance
        PdfWriter writer = null;
        try {
            
            //Create a new Pdf Document
            Document document = new Document();
            
            writer = PdfWriter.getInstance(document, pdfFos);
            document.open();
        }  catch(Exception e) {
            e.printStackTrace();
            logger.error("Error - getPdfContentBytes");
        }
        return writer.getDirectContent();
    }


    // public String generatePDF(String xhtmlContent) {
    public String[] generatePDF(String template_id, String fileName, JsonNode xhtmlContent, boolean batch, String azureFolder) {

        String xhtmlFileName = NEWLINE_TEMPLATE + fileName + XHTML;
        String returnValue[] = new String[3];
        OutputStream os = null;
		PDF_DEV = azureFolder;
		
        try {

            String jsonxtml = xhtmlContent.get("value").textValue();
            String jsonCssStyle = xhtmlContent.get("cssStyle").textValue();


            PrintWriter writer = new PrintWriter(xhtmlFileName, "UTF-8");
            writer.println("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
            writer.println("<head>");
            writer.println("<title>Title of document</title>");
            //writer.println("<style language=\"text/css\">@page { size: 3.5in 11in; margin: 15in;}</style>");
            writer.println("<style language=\"text/css\">" + jsonCssStyle + "</style>");
            writer.println("</head>");
            writer.println("<body>");
            writer.println(jsonxtml);
            writer.println("</body>");
            writer.println("</html>");
            writer.close();
            File f = new File(xhtmlFileName);
            URL location = f.toURI().toURL();
            Date date = new Date();

            String outputFileName = NEWLINE_TEMPLATE + fileName + "_" + date.getTime() + PDF;
            File outputFile = new File(outputFileName);
            os = new FileOutputStream(outputFile);
            ITextRenderer renderer = new ITextRenderer();

            renderer.setDocument(location.toExternalForm());
            renderer.layout();
            renderer.createPDF(os);
            os.flush();
			f.delete();
            if (!batch) {
                String template_url = pushPDFToAzure(outputFileName, azureFolder);

                returnValue[0] = template_id;
                returnValue[1] = fileName;
                returnValue[2] = template_url;

            } else {
                returnValue[0] = template_id;
                returnValue[1] = fileName;
                returnValue[2] = outputFileName;
            }
        } catch (Exception e) {
            logger.debug(e.getMessage());
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (Exception e) {
                }
            }
        }
        return returnValue;

    }


    public DownloadResponse DownloadFromAzureStorage(String url) throws Exception {

        //FileInputStream fis = null;
        DownloadResponse downloadResponse;
        try {
			        String[] urlArray = url.split("/");
        String outputFile = urlArray[urlArray.length-1];
        String containerName = urlArray[urlArray.length-2];
		PDF_DEV = containerName;
            CloudBlobContainer container = createCloudBlobContainer();
            
            CloudBlockBlob blob = container.getBlockBlobReference(outputFile);
            File downloadFile = new File(outputFile);
        
            downloadFile.createNewFile();
            blob.downloadToFile(downloadFile.getAbsolutePath());
            downloadResponse = new DownloadResponse();
            FileInputStream fis = new FileInputStream(downloadFile);
            downloadResponse.setContents(fis);
            downloadResponse.setInline(false);
            downloadResponse.setFileName(outputFile);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                //if (fis != null) {
                //    fis.close();
                //}
            } catch (Exception e) {
            }
        }
        return downloadResponse;
    }


    public String DeleteBlobFromAzureStorage(String deleteFlag, String url, String docId) throws Exception {

        DownloadResponse downloadResponse;
        try {
            String[] urlArray = url.split("/");
            String outputFile = urlArray[urlArray.length-1];
            String containerName = urlArray[urlArray.length-2];
            PDF_DEV = containerName;
            CloudBlobContainer container = createCloudBlobContainer();
            
            CloudBlockBlob blob = container.getBlockBlobReference(outputFile);
            if(blob.exists()){
                blob.deleteIfExists();
                if(deleteFlag.equals("pdf")){
                    int deleteAzureBlob = queryExceutorService.executeQryDeletePdfPerId( Integer.parseInt(docId));   
                    return "Pdf deleted";
                }else {                                                 //deleteflag -> image
                    int deleteAzureBlob = queryExceutorService.executeQryDeletePhoto( Integer.parseInt(docId));  
                    return "Image deleted";
                }
            }else{
                return "blog does not exist";
            }
            
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private CloudBlobContainer createCloudBlobContainer() {
        CloudBlobContainer container = null;
        String storageConnectionString = "DefaultEndpointsProtocol=" + azureEndpointsProtocol + ";" + "AccountName=" + azureAccountName + ";" + "AccountKey=" + azureAccountKey;
        try {
            CloudStorageAccount account = CloudStorageAccount.parse(storageConnectionString);
            CloudBlobClient serviceClient = account.createCloudBlobClient();
            container = serviceClient.getContainerReference(PDF_DEV);
            container.createIfNotExists();
        } catch(URISyntaxException e) {
            e.printStackTrace();
            logger.error("Error - createCloudBlobContainer - URISyntaxException");
        } catch(InvalidKeyException e) {
            e.printStackTrace();
            logger.error("Error - createCloudBlobContainer - InvalidKeyException");
        } catch(StorageException e) {
            e.printStackTrace();
            logger.error("Error - createCloudBlobContainer - StorageException");
        }
        return container;
    }


}