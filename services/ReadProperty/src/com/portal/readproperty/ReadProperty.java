/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.readproperty;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.wavemaker.runtime.WMAppContext;
import com.wavemaker.runtime.security.SecurityService;
import com.wavemaker.runtime.service.annotations.ExposeToClient;

//import com.portal.readproperty.model.*;

/**
 * This is a singleton class with all its public methods exposed as REST APIs via generated controller class.
 * To avoid exposing an API for a particular public method, annotate it with @HideFromClient.
 * <p>
 * Method names will play a major role in defining the Http Method for the generated APIs. For example, a method name
 * that starts with delete/remove, will make the API exposed as Http Method "DELETE".
 * <p>
 * Method Parameters of type primitives (including java.lang.String) will be exposed as Query Parameters &
 * Complex Types/Objects will become part of the Request body in the generated API.
 */
@ExposeToClient
public class ReadProperty {

    private static final Logger logger = LoggerFactory.getLogger(ReadProperty.class);

    @Autowired
    private SecurityService securityService;
    
    @Value("${app.environment.mapsapikey}")
    private String googleMapsApiKey;
    
    @Value("${app.environment.pdf}")
    private String pdf;
    
    @Value("${app.environment.photos}")
    private String photos;
    
    @Value("${app.environment.azurefileupload}")
    private String azureEndpointsProtocol;
    
    @Value("${app.environment.azureaccountname}")
    private String azureAccountName;
    
    @Value("${app.environment.azureaccountkey}")
    private String azureAccountKey;
    

    /**
     * This is sample java operation that accepts an input from the caller and responds with "Hello".
     * <p>
     * SecurityService that is Autowired will provide access to the security context of the caller. It has methods like isAuthenticated(),
     * getUserName() and getUserId() etc which returns the information based on the caller context.
     * <p>
     * Methods in this class can declare HttpServletRequest, HttpServletResponse as input parameters to access the
     * caller's request/response objects respectively. These parameters will be injected when request is made (during API invocation).
     */
    public String getProperty(String key) {
        
        logger.debug("azureEndpointsProtocol ===----==-=-=-=-=--<<<<<<<<<<<<<<   ", azureEndpointsProtocol);
        logger.debug("azureAccountName ===----==-=-=-=-=-- >>>>>>>>>>>>>>>>>>>  ", azureAccountName);
        logger.debug("azureaccountkey....,,,,,...... ===----==-=-=-=-=--   ", azureAccountKey);
        //logger.debug("Starting sample operation with request url " + request.getRequestURL().toString());

        String result = null;
        //InputStream inputStream = null;
        //InputStream profileInputStream = null;
        
       // if (securityService.isAuthenticated()) {
        
        if (key.equals("pdf")) { 	
            result = pdf; 	
        }	
        else if (key.equals("photos")) {		
            result = photos;
        }
        else if (key.equals("mapsapikey")) {
            result = googleMapsApiKey;
        }
        else if (key.equals("azurefileupload")) {
            result = azureEndpointsProtocol;
        }
        else if (key.equals("azureaccountname")) {
            result = azureAccountName;
        }
        else {
            result = azureAccountKey;
        }
        //}
        //else {
          //  logger.debug("Returning {}", result);
        //}
        //try {
        //     Properties prop = new Properties();
        //     String propAbsPath = WMAppContext.getInstance().getContext().getRealPath("/resources/files/newline.properties");
        //     inputStream = new FileInputStream(propAbsPath);
        //     if (inputStream != null) {
        //         prop.load(inputStream);
        //     } else {
        //         throw new FileNotFoundException("Property file '" + propAbsPath + "' not found in the classpath");
        //     }

        //     Properties profileProp = new Properties();
        //     String profilePropAbsPath = WMAppContext.getInstance().getContext().getRealPath("/WEB-INF/classes/platform.properties");
        //     profileInputStream = new FileInputStream(profilePropAbsPath);
        //     String profileName;

        //     if (profileInputStream != null) {
        //         profileProp.load(profileInputStream);
        //         String platformUrl = profileProp.getProperty("platform.url");
        //         int idx = platformUrl.indexOf("databaseName=");
        //         profileName = platformUrl.substring(idx + 13);
        //     } else {
        //         throw new FileNotFoundException("Profile property file '" + profileInputStream + "' not found in the classpath");
        //     }

        //     get the property value and print it out
         //    if (securityService.isAuthenticated()) {
        //         // result = "{\"result\":{\"status\":\"success\", \"content\":{\"pdf.folder\": \"" + prop.getProperty("pdf.folder." + profileName) + "\", \"photo.folder\": \"" + prop.getProperty("photo.folder." + profileName) + "\", \"google.maps.api.key\": \"" + prop.getProperty("google.maps.api.key") + "\"}}}";
        //         result = "{\"result\":{\"status\":\"success\", \"content\":{\"pdf.folder\": \"" + prop.getProperty("pdf.folder." + profileName) + "\", \"photo.folder\": \"" + prop.getProperty("photo.folder." + profileName) + "\", \"google.maps.api.key\": \"" + prop.getProperty("google.maps.api.key") + "\"}}}";
        //     } else {
        //         result = "{\"result\":{\"status\":\"success\", \"content\":{\"google.oauth.secret\": \"" + prop.getProperty("google.oauth.secret") + "\", \"google.oauth.appid\": \"" + prop.getProperty("google.oauth.appid") + "\"}}}";
        //     }

        // } catch (FileNotFoundException e) {
        //     e.printStackTrace();
        //     logger.error("Error while reading property file in method getProperty " + name);
        //     result = "{\"result\": {\"status\":\"Error\", \"message\":\"Error while reading property: " + e + "\"}}";
        // } catch (IOException e) {
        //     e.printStackTrace();
        //     logger.error("Error while reading file in method getProperty " + name);
        //     result = "{\"result\": {\"status\":\"error\", \"message\":\"Error while reading file: " + e + "\"}}";
        // } finally {
        //     try {
        //         if (inputStream != null) {
        //             inputStream.close();
        //         }
        //         if (profileInputStream != null) {
        //             profileInputStream.close();
        //         }
        //     } catch (Exception e) {
        //     }
        // }
        
       // result = { pdf , photo , googleMapsApiKey };

        logger.debug("Returning {}", result);
        return result;
    }
    
    public String getBuildDate() {
            InputStream inputStream = null;
            InputStream profileInputStream = null;
            String buildDate = "";
            try{
            String propAbsPath = WMAppContext.getInstance().getContext().getRealPath("/WEB-INF/classes/version.properties");
            profileInputStream = new FileInputStream(propAbsPath);
            Properties profileProp = new Properties();
            
            
            if (profileInputStream != null) {
                profileProp.load(profileInputStream);
                buildDate = profileProp.getProperty("build.date");
            } else {
                throw new FileNotFoundException("Profile property file '" + profileInputStream + "' not found in the classpath");
            }
            } catch (FileNotFoundException e) {
                
            }  catch (IOException e) {
                
            }finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (profileInputStream != null) {
                    profileInputStream.close();
                }
            } catch (Exception e) {
            }
        }
            return buildDate;
}

}
