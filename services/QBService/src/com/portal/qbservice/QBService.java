/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.qbservice;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import com.wavemaker.runtime.security.SecurityService;
import com.wavemaker.runtime.service.annotations.ExposeToClient;
import com.wavemaker.runtime.service.annotations.HideFromClient;

//import com.qbotest.qbservice.model.*;

//my imports
import java.util.Map;
import java.util.HashMap;

import com.intuit.ipp.core.Context;
import com.intuit.ipp.util.Config;
import com.intuit.ipp.core.ServiceType;
import com.intuit.ipp.security.OAuth2Authorizer;
import com.intuit.ipp.services.DataService;
import com.intuit.ipp.services.QueryResult;
import com.intuit.ipp.util.Config;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.services.ReportService;
import com.intuit.ipp.services.ReportName;
import com.intuit.ipp.data.Report;
import com.intuit.ipp.exception.AuthenticationException;
import com.intuit.ipp.exception.ServiceException;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;
import org.apache.commons.lang.StringUtils;

import com.portal.qboauthservice.QBOAuthService;
import com.portal.platform.models.query.GetOauthDataResponse;

@ExposeToClient
public class QBService {

    private static final Logger logger = LoggerFactory.getLogger(QBService.class);
    
    private static final String QUERY_GET_COMPANYINFO = "select * from companyinfo";
    private static final String QUERY_GET_REOCUSTOMERS = "select * from Customer Where FullyQualifiedName like '%REO%'";
    private static final String BASE_URL_SUFFIX = "/v3/company";
    //private static final String QUERY_GET_REOCUSTOMERS = "select * from Customer Where FullyQualifiedName like '%REO%' and FullyQualifiedName != DisplayName";
    
    @Value("${app.environment.qbBaseUrl}")
    private String baseUrl;    
    
    @Autowired
    QBOAuthService qbOAuthService;
    
    //This method returns both balancesheet and profitandloss reports together.
    public Map<String, Report> getReports(String qbCustomerId) {
        try {
            Map<String, Report> reports = new HashMap<String, Report>();
            reports.put("balancesheet", getReport(ReportName.BALANCESHEET.toString(), qbCustomerId));
            reports.put("profitandloss", getReport(ReportName.PROFITANDLOSS.toString(), qbCustomerId));
            return reports;
        } catch(Exception ex) {
            throw new RuntimeException(ex);
        }
    }
    
    //This method returns all the REO customer of Quickbooks.
    public QueryResult getREOCustomers() {
        return getQueryResults(QUERY_GET_REOCUSTOMERS);
    }
    
    //This method is used to get the company information.
    public QueryResult getCompanyInfo() {
        return getQueryResults(QUERY_GET_COMPANYINFO);
    }
    
    //A wrapper method to generate report based on the given reportname and the Quickbooks customer.
    private Report getReport(String reportName, String qbCustomerId) {
        if(StringUtils.isBlank(reportName)) {
           throw new RuntimeException("Report name cannot be null or empty");
        }
        try {
            setBaseUrl();
            GetOauthDataResponse oauthData = qbOAuthService.getOauth();
            //provider_is is the realmId here.
            ReportService service = getReportService(oauthData.getProviderId(), oauthData.getAccessToken());
            if(!StringUtils.isBlank(qbCustomerId)) {
                service.setCustomer(qbCustomerId);
            }
            //execute to get the report.
            return service.executeReport(reportName);
        } catch(AuthenticationException ae) {
            //This exception will happen when the current access_token is expired.
            try {
                //refresh tokens here using the current refresh_token available in db.
                //following call will refresh the tokens as well store the new tokens in db also.
                qbOAuthService.refreshTokens();
                GetOauthDataResponse oauthData = qbOAuthService.getOauth();
                ReportService service = getReportService(oauthData.getProviderId(), oauthData.getAccessToken());
                if(!StringUtils.isBlank(qbCustomerId)) {
                    service.setCustomer(qbCustomerId);
                }
                return service.executeReport(reportName);
            } catch(Exception ex) {
                logger.error("Error occurred while refreshing token or getting the report using the refreshed tokens", ex);
                throw new RuntimeException(ex);
            }
        } catch (Exception ex) {
            logger.error("Error occurred: ", ex);
            throw new RuntimeException(ex);
        }
    }
    
    //A wrapper method for running queries.
    private QueryResult getQueryResults(String query) {
        try {
            setBaseUrl();
            GetOauthDataResponse oauthData = qbOAuthService.getOauth();
            DataService service = getDataService(oauthData.getProviderId(), oauthData.getAccessToken());
            return service.executeQuery(query);
        } catch(AuthenticationException ae) {
            //This exception will happen when the current access_token is expired.
            try {
                //refresh tokens here using the current refresh_token available in db.
                //following call will refresh the tokens as well store the new tokens in db also.
                qbOAuthService.refreshTokens();
                GetOauthDataResponse oauthData = qbOAuthService.getOauth();
                DataService service = getDataService(oauthData.getProviderId(), oauthData.getAccessToken());
                return service.executeQuery(query);
            } catch(Exception ex) {
                logger.error("Error occurred while refreshing token or exectuing query using the refreshed tokens", ex);
                throw new RuntimeException(ex);
            }
        } catch (Exception ex) {
            logger.error("Error occurred: ", ex);
            throw new RuntimeException(ex);
        }
    }
    
    //Method to set base url of Quickbooks APIs
    private void setBaseUrl() {
        Config.setProperty(Config.BASE_URL_QBO, baseUrl + BASE_URL_SUFFIX);
    }
    
    //Method to construct DataService object.
    private DataService getDataService(String realmId, String accessToken) throws FMSException {
		OAuth2Authorizer oauth = new OAuth2Authorizer(accessToken);
		Context context = new Context(oauth, ServiceType.QBO, realmId);
		return new DataService(context);
	}

    //Method to construct ReportService object.
    private ReportService getReportService(String realmId, String accessToken) throws FMSException {
		OAuth2Authorizer oauth = new OAuth2Authorizer(accessToken);
		Context context = new Context(oauth, ServiceType.QBO, realmId);
		return new ReportService(context);
    }

}
