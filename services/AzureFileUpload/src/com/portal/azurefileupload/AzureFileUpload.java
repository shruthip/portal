/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.azurefileupload;

import java.io.File;
import java.io.FileInputStream;
import java.util.Date;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.microsoft.azure.storage.*;
import com.microsoft.azure.storage.blob.*;
import com.wavemaker.runtime.file.manager.FileServiceManager;
import com.wavemaker.runtime.security.SecurityService;
import com.wavemaker.runtime.service.annotations.ExposeToClient;
import org.springframework.beans.factory.annotation.Value;

import java.net.URISyntaxException;
import java.io.FileNotFoundException;
import java.io.IOException;

//import com.portal.azurefileupload.model.*;

/**
 * This is a singleton class with all its public methods exposed as REST APIs via generated controller class.
 * To avoid exposing an API for a particular public method, annotate it with @HideFromClient.
 * <p>
 * Method names will play a major role in defining the Http Method for the generated APIs. For example, a method name
 * that starts with delete/remove, will make the API exposed as Http Method "DELETE".
 * <p>
 * Method Parameters of type primitives (including java.lang.String) will be exposed as Query Parameters &
 * Complex Types/Objects will become part of the Request body in the generated API.
 */
@ExposeToClient
public class AzureFileUpload {

    private static final Logger logger = LoggerFactory.getLogger(AzureFileUpload.class);

    private static final String UPLOAD_DIR = "temp/azureimages";

    private CloudBlobClient cloudBlobClient;

    @Autowired
    private SecurityService securityService;
    @Autowired
    private FileServiceManager fileServiceManager;
    
    @Value("${app.environment.azurefileupload}")
    private String azureEndpointsProtocol;
    
    @Value("${app.environment.azureaccountname}")
    private String azureAccountName;
    
    @Value("${app.environment.azureaccountkey}")
    private String azureAccountKey;

    /**
     * This is sample java operation that accepts an input from the caller and responds with "Hello".
     * <p>
     * SecurityService that is Autowired will provide access to the security context of the caller. It has methods like isAuthenticated(),
     * getUserName() and getUserId() etc which returns the information based on the caller context.
     * <p>
     * Methods in this class can declare HttpServletRequest, HttpServletResponse as input parameters to access the
     * caller's request/response objects respectively. These parameters will be injected when request is made (during API invocation).
     */
    public String uploadImageFileToAzure(String name, String blobName) throws Exception{
        String result = null;
        FileInputStream fileInputStream = null;
        File sourceFile = null;
        try {

            CloudBlobContainer container = createCloudBlobContainer(blobName);
            sourceFile = fileServiceManager.downloadFile(name, getUploadDir());
            String blockBlobReference = getBlockBlobReference(name);

            // Upload an image file.
            CloudBlockBlob blob = container.getBlockBlobReference(blockBlobReference);
            fileInputStream = new FileInputStream(sourceFile);
            blob.upload(fileInputStream, sourceFile.length());
            result = blob.getUri().toString();
                    
        } catch (IOException fe) {
            throw new RuntimeException("File not found", fe);
        } catch(Exception e) {
            e.printStackTrace();
            logger.error("Exception encountered:");
        } finally {
                    fileInputStream.close();
                    sourceFile.delete();
        }                    

        return result;
    }


    private File getUploadDir() {
        //String uploadDir = System.getProperty("user.home") + "/WaveMaker/appdata/poc_file_upload/uploads";
        String uploadDir = UPLOAD_DIR;
        File f = new File(uploadDir);
        f.mkdirs();
        return f;
    }

    private CloudBlobContainer createCloudBlobContainer(String blobName) {
        String storageConnectionString = "DefaultEndpointsProtocol=" + azureEndpointsProtocol + ";" + "AccountName=" + azureAccountName + ";" + "AccountKey=" + azureAccountKey;
        try {
            CloudStorageAccount cloudStorageAccount = CloudStorageAccount.parse(storageConnectionString);
            this.cloudBlobClient = cloudStorageAccount.createCloudBlobClient();
        } catch (URISyntaxException e) {
            e.printStackTrace();
            logger.error("Error - URISyntaxException - AzureFileUPload()");
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error - AzureFileUPload()");
        }
        
        CloudBlobContainer container = null;
        try {
            container = cloudBlobClient.getContainerReference(blobName);
            container.createIfNotExists();
        } catch (URISyntaxException e) {
            e.printStackTrace();
            logger.error("Error - URISyntaxException - createCloudBlobContainer()");
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error - createCloudBlobContainer()");
        }
        return container;
    }

    private String getBlockBlobReference(String name){
        Date date = new Date();
        String fileName = name.substring(0, name.lastIndexOf('.')).replaceAll("[%() ]", "") + '_' + date.getTime();
        String extn = name.substring(name.lastIndexOf('.') + 1, name.length());
        return fileName + '.' + extn;
    }

}