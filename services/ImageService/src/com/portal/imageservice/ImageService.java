/*Copyright (c) 2016-2017 newlinefinancial.com All Rights Reserved.
 This software is the confidential and proprietary information of newlinefinancial.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with newlinefinancial.com*/
package com.portal.imageservice;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;


import com.wavemaker.runtime.security.SecurityService;
import com.wavemaker.runtime.service.annotations.ExposeToClient;
import com.wavemaker.runtime.service.annotations.HideFromClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.HttpResponse;
import org.apache.http.HttpEntity;
import java.io.InputStream;
import java.io.ByteArrayOutputStream;
import org.apache.commons.codec.binary.Base64;
//import com.portal.imageservice.model.*;

/**
 * This is a singleton class with all its public methods exposed as REST APIs via generated controller class.
 * To avoid exposing an API for a particular public method, annotate it with @HideFromClient.
 *
 * Method names will play a major role in defining the Http Method for the generated APIs. For example, a method name
 * that starts with delete/remove, will make the API exposed as Http Method "DELETE".
 *
 * Method Parameters of type primitives (including java.lang.String) will be exposed as Query Parameters &
 * Complex Types/Objects will become part of the Request body in the generated API.
 */
@ExposeToClient
public class ImageService {

public byte[] getBytesFromInputStream(InputStream is) { 
    try  {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        byte[] buffer = new byte[0xFFFF];
        for (int len; (len = is.read(buffer)) != -1;) 
        os.write(buffer, 0, len); os.flush(); 

        return os.toByteArray();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return new byte[0];
    }


 public String downloadImage(String image_url) {
     
     String imageUrl = image_url;
     
         DefaultHttpClient client = new DefaultHttpClient();
         InputStream is = null;
         
        try{
            int urlLength =imageUrl.length(); 
            if(imageUrl.charAt(urlLength-1) == '\"'){
                imageUrl = imageUrl.substring(0,urlLength-1);
            }
            
            HttpGet request = new HttpGet(imageUrl);
            HttpResponse response = client.execute(request);
            
            HttpEntity entity = response.getEntity();
            int imageLength = (int)(entity.getContentLength());
            is = entity.getContent();
            
            byte[] imageBlob = getBytesFromInputStream(is);
            String encodedfile = new String(Base64.encodeBase64(imageBlob), "UTF-8");
            return encodedfile;
         }catch(Exception e){
             e.printStackTrace();
         }finally{
             try{
                 if(is != null){
                     is.close();
                 }
             }catch (Exception e){
                 
             }finally{
                 client.close();
             }
         }
        
    return "";
}
}